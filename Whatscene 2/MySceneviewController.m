//
//  MySceneviewController.m
//  Whatscene
//
//  Created by Alet Viegas on 09/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "MySceneviewController.h"
#import "MysceneTableViewCell.h"
#import "UITableView+DragLoad.h"
#import "ViewController.h"
#import "AsyncImageView.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)


@interface MySceneviewController ()<UITableViewDelegate,UITableViewDataSource
,FrameObservingViewDelegate>
{
    NSMutableArray * arrayofData,*arrOfEventName,*arrOfEventDateAndtime,*arrOfEventImages,*arrOfEventAddress,*arrOfCellImages,*arrOfEventDescription,*arrOfEventType,*arrOfEventLatitude,*arrOfEventLongitude,*arrOfEventId,*arrOfEventOwnerId,*arrOfEventCommentCount,*arrOfEventhouseNo,*arrOfEventStreet,*arrOfEventNotificationCount;
    UITableView *tableViewForMyscene,*strOfEventScene;
    NSInteger pageNumber;
    NSInteger pageLimit;
    BOOL rightClickEvent;
    
}

@property (strong, nonatomic) IBOutlet UIButton *buttonRightHide;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *buttonCreateNewEvent;
@property (strong, nonatomic) IBOutlet UIView *viewGetStarted;

@end
// for sending object value one view to another view
NSString *strOfEventImage,*strOfEventDateAndTime,*strOfEventDescription,*strOfEventName,*strOfEventPlace,*strOfEventType,*strOfEventId,*strOfEventOwnerId,*strOfSceneName,*strOfCommentFlag,*strOfEventInvitedStatus,*strOfEventHouseNo,*strOfEventStreet;
CGFloat eventLatitude,eventlongitude;

UIColor *colorForViewScene;
@implementation MySceneviewController

- (void)frameObservingViewFrameChanged:(FrameObservingView *)view
{
    tableViewForMyscene.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width ,self.view.frame.size.height-64);
    
    
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _activityIndicator.hidden = YES;
    [self navigationBar];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

   
    [[NSUserDefaults standardUserDefaults]setObject:@"myscene" forKey:@"StoryBoardId"];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:248.0/255.0 green:117.0/255.0 blue:47.0/255.0 alpha:1.0]];
    colorForViewScene = [UIColor colorWithRed:248.0/255.0 green:117.0/255.0 blue:47.0/255.0 alpha:1.0];
     self.view.backgroundColor = colorForViewScene;
    _buttonRightHide.hidden = YES;
    [_buttonRightHide addTarget:self action:@selector(DrawerMenuAction) forControlEvents:UIControlEventTouchUpInside];
    
    rightClickEvent  = YES;
    pageNumber = 1;
    pageLimit =10;
   
    
}

-(void) navigationBar
{
    UIButton * loginButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [loginButton setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(RightMenuDrawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *loginButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:loginButtonItem, nil];
}


-(void) buttonCreateNewEventAction
{
    strOfEventOwnerId = @"";
    strOfEventId =nil;
    strOfSceneName = @"MyScene";
    strOfEventStreet = @"";
    strOfEventHouseNo = @"";
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Newscene"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Drag delegate methods

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}
- (void)finishRefresh
{
    // _dataCount = 10;
    
    [tableViewForMyscene finishRefresh];
    [tableViewForMyscene reloadData];
}
-(void) finishLoadMore
{
    
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    if (netStatus == NotReachable)
    {
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        dispatch_async(kBgQueue, ^{
            
            [self getMyScene];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableViewForMyscene finishLoadMore];
                [tableViewForMyscene reloadData];
                
            });
            
            
            
        });
    }
   
    
}

-(void) viewWillAppear:(BOOL)animated
{
    // [tableViewForMyscene reloadData];
    [self viewInfo];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    pageNumber = 1;
    pageLimit =10;
    
    
    arrOfEventAddress = [NSMutableArray new];
    arrOfEventDateAndtime = [NSMutableArray new];
    arrOfEventName = [NSMutableArray new];
    arrOfEventImages = [NSMutableArray new];
    arrOfEventDescription = [NSMutableArray new];
    arrOfEventType = [NSMutableArray new];
    arrOfEventLatitude = [NSMutableArray new];
    arrOfEventLongitude = [NSMutableArray new];
    arrOfEventId = [NSMutableArray new];
    arrOfEventOwnerId = [NSMutableArray new];
    arrOfEventCommentCount = [NSMutableArray new];
    arrOfEventNotificationCount = [NSMutableArray new];
    arrOfEventhouseNo = [NSMutableArray new];
    arrOfEventStreet = [NSMutableArray new];
    [tableViewForMyscene reloadData];
    self.navigationController.navigationBar.hidden = NO;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        _activityIndicator.hidden = NO;
        [_activityIndicator startAnimating];
        
    });

    
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    if (netStatus == NotReachable)
    {
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        dispatch_async(kBgQueue, ^{
            
            [self getMyScene];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableViewForMyscene reloadData];
                
            });
            
            
            
        });
    }
   
    
}

-(void) viewInfo
{
    [_buttonCreateNewEvent addTarget:self action:@selector(buttonCreateNewEventAction) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    arrOfEventAddress = [NSMutableArray new];
    arrOfEventDateAndtime = [NSMutableArray new];
    arrOfEventName = [NSMutableArray new];
    arrOfEventImages = [NSMutableArray new];
    arrOfEventDescription = [NSMutableArray new];
    arrOfEventType = [NSMutableArray new];
    arrOfEventLatitude = [NSMutableArray new];
    arrOfEventLongitude = [NSMutableArray new];
    arrOfEventId = [NSMutableArray new];
    arrOfEventOwnerId = [NSMutableArray new];
    arrOfEventCommentCount = [NSMutableArray new];
    arrayofData = [[NSMutableArray alloc]initWithObjects:@"Latest Scenes",@"Nearby Scenes",@"My Scenes",@"1",@"2",@"3", nil];
    arrOfCellImages = [[NSMutableArray alloc]initWithObjects:@"1.JPG",@"2.JPG",@"3.JPG",@"2.JPG",@"3.JPG", nil];
    
    
    
    FrameObservingView *frameObservingView = [[FrameObservingView alloc] init];
    frameObservingView.delegate = self;
    [self.view addSubview:frameObservingView];
    
    
    
    tableViewForMyscene = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    // _tableMainList.frame = CGRectMake(8, 229, self.view.frame.size.width-16, self.view.frame.size.height-239);
    tableViewForMyscene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width ,self.view.frame.size.height-64);
    
    tableViewForMyscene.dataSource = self;
    tableViewForMyscene.delegate = self;
    [tableViewForMyscene setDragDelegate:self refreshDatePermanentKey:@"FriendList"];
    tableViewForMyscene.showLoadMoreView = YES;
    tableViewForMyscene.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableViewForMyscene.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableViewForMyscene];
    
    [self.view addSubview:_buttonCreateNewEvent];
    [self.view addSubview:_buttonRightHide];
    [self.view addSubview:_activityIndicator];
}

-(void) getMyScene
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/getmyevent.php?user_id=%@&page=%ld&limit=%ld",[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"],(long)pageNumber,(long)pageLimit];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err)
        {
            NSLog(@"err---- %@",err.description);
        }
        else{
            
            if([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                NSArray *arr = [[json valueForKey:@"data"] valueForKey:@"event_name"];
                if (arr.count != 0) {
                    pageNumber++;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                    _viewGetStarted.hidden = YES;
                        
                        
                    });
                    
                }
               
                for (int i = 0; i<arr.count; i++) {
                    [arrOfEventName addObject:[[[json valueForKey:@"data"] valueForKey:@"event_name"] objectAtIndex:i]];
                    
                    NSString *strOfEventDateAndTime = [NSString stringWithFormat:@"%@ | %@",[[[json valueForKey:@"data"] valueForKey:@"event_time"] objectAtIndex:i],[[[json valueForKey:@"data"] valueForKey:@"event_date"] objectAtIndex:i]];
                    [arrOfEventDateAndtime addObject:strOfEventDateAndTime];
                    [arrOfEventDescription addObject:[[[json valueForKey:@"data"] valueForKey:@"event_description"] objectAtIndex:i]];
                    [arrOfEventAddress addObject:[[[json valueForKey:@"data"] valueForKey:@"event_address"] objectAtIndex:i]];
                    //After you get image from web service add image arrOfEventImages & remove arrOfCellImageslogin
                    [arrOfEventImages addObject:[[[json valueForKey:@"data"]valueForKey:@"event_image_url"]objectAtIndex:i]];
                    [arrOfCellImages addObject:[arrOfCellImages objectAtIndex:i]];
                    [arrOfEventType addObject:[[[json valueForKey:@"data"] valueForKey:@"event_category"] objectAtIndex:i]];
                    [arrOfEventLatitude addObject:[[[json valueForKey:@"data"] valueForKey:@"event_latitude"] objectAtIndex:i]];
                    [arrOfEventLongitude addObject:[[[json valueForKey:@"data"] valueForKey:@"event_longitude"] objectAtIndex:i]];
                    [arrOfEventId addObject:[[[json valueForKey:@"data"] valueForKey:@"event_id"] objectAtIndex:i]];
                    [arrOfEventOwnerId addObject:[[[json valueForKey:@"data"] valueForKey:@"owner_id"] objectAtIndex:i]];
                    [arrOfEventCommentCount addObject:[[[json valueForKey:@"data"] valueForKey:@"event_comment_count"] objectAtIndex:i]];
                    [arrOfEventStreet addObject:[[[json valueForKey:@"data"] valueForKey:@"event_street"] objectAtIndex:i]];
                    [arrOfEventhouseNo addObject:[[[json valueForKey:@"data"] valueForKey:@"event_houseno"] objectAtIndex:i]];
                    [arrOfEventNotificationCount addObject:[[[json valueForKey:@"data"] valueForKey:@"event_notification_count"] objectAtIndex:i]];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_activityIndicator stopAnimating];
                _activityIndicator.hidden = YES;
                
                
            });
            
            NSLog(@"%@",arrOfEventName);
            
        }
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 200.0;
    }
    else
    {
        return 150.0;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrOfEventName.count;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    
    UITableViewCell *cell=[tableViewForMyscene dequeueReusableCellWithIdentifier:@"cell"];
    
    
    if (cell == nil)
        
    {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];;
        
    }
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    AsyncImageView *imageView;
    UILabel *labelTransparent;
    UILabel *labelEventName;
    UILabel *labelEventDateAndtime;
    UILabel *labelEventAdreess;
    UIButton *buttonCommentCount;
    UIImageView *imageViewComment;
    UILabel *labelEventCommentCount;
    UIButton *buttonNotificationCount;
    UIImageView *imageViewNotification;
    UILabel *labelEventNotificationCount;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 190)];
        labelTransparent = [[UILabel alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, 80)];
        labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(10, 120, self.view.frame.size.width-20, 30)];
        labelEventName.font = [UIFont boldSystemFontOfSize:25];
        labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 150, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess = [[UILabel alloc]initWithFrame:CGRectMake(10, 170, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess.font = [UIFont systemFontOfSize:15];
        buttonCommentCount = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+30, 140, 60, 40)];
        imageViewComment = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+35, 150, 20, 20)];
        labelEventCommentCount = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+60, 150,20, 20)];
        labelEventCommentCount.font = [UIFont systemFontOfSize:15];
        buttonNotificationCount = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+120, 140, 60, 40)];
        imageViewNotification = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+125, 150, 20, 20)];
        labelEventNotificationCount = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+150, 150,20, 20)];
        labelEventNotificationCount.font = [UIFont systemFontOfSize:15];
    }
    else
    {
        imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 140)];
        labelTransparent = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 60)];
        labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, self.view.frame.size.width-20, 20)];
        labelEventName.font = [UIFont boldSystemFontOfSize:17];
        labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 100, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess = [[UILabel alloc]initWithFrame:CGRectMake(10, 120, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess.font = [UIFont systemFontOfSize:12];
        buttonCommentCount = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+30, 115, 40, 20)];
        imageViewComment = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+35, 120, 10, 10)];
        labelEventCommentCount = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+50, 120,10, 10)];
        labelEventCommentCount.font = [UIFont systemFontOfSize:8];
        buttonNotificationCount = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+90, 115, 40, 20)];
        imageViewNotification = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+95, 120, 10, 10)];
        labelEventNotificationCount = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)+110, 120,10, 10)];
        labelEventNotificationCount.font = [UIFont systemFontOfSize:8];
    }
    
    
    
    if ([arrOfEventImages objectAtIndex:indexPath.row]!= [NSNull null]) {
        imageView.imageURL = [NSURL URLWithString:[arrOfEventImages objectAtIndex:indexPath.row]];
    }
    
  
    
    
    if ([[arrOfEventImages objectAtIndex:indexPath.row] isEqualToString:@""]) {
        imageView.image = [UIImage imageNamed:[arrOfCellImages objectAtIndex:indexPath.row]];
    }
    
    
    
    [cell.contentView addSubview:imageView];
    
    
    
    
    labelTransparent.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    
    [cell.contentView addSubview:labelTransparent];
    
    cell.contentView.backgroundColor = colorForViewScene;
    
    
    
    
    
    
    
    labelEventName.text = [arrOfEventName objectAtIndex:indexPath.row];
    
    
    
    labelEventName.textColor = [UIColor whiteColor];
    
    //labelEventName.numberOfLines = 2;
    
    [cell.contentView addSubview:labelEventName];
    
    
    
    //     UILabel *labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 100, (self.view.frame.size.width-20)/2, 20)];
    //
    //
    //    NSArray *arrOfDateAndTime = [[arrOfEventDateAndtime objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
    //
    //    NSLog(@"%@",arrOfDateAndTime);
    //
    //
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
    //
    //    NSDate *date = [dateFormatter dateFromString: [arrOfDateAndTime objectAtIndex:1]]; // here you can fetch date from string with define format
    //
    //    dateFormatter = [[NSDateFormatter alloc] init]; // here you can fetch date from string with define format
    //
    //
    //    [dateFormatter setDateFormat:@"dd MMM yy"];// here set format which you want...
    //    NSString *dateString = [dateFormatter stringFromDate:date];// here convert date in NSSt
    //
    //
    //    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    //    dateFormatter1.dateFormat = @"HH:mm:ss";
    //    NSDate *date1 = [dateFormatter1 dateFromString:[arrOfDateAndTime objectAtIndex:0]];
    //    dateFormatter1.dateFormat = @"hh:mm a";
    //    NSString *strOfEventTime = [dateFormatter1 stringFromDate:date1];
    //
    //    NSLog(@"Converted String : %@",strOfEventTime);
    //
    //    NSDateFormatter *dateFormatForDay = [[NSDateFormatter alloc] init];
    //
    //    [dateFormatForDay setDateFormat:@"EE"];
    //    NSString *strOfEventday = [dateFormatForDay stringFromDate:date];
    //
    //
    //    NSString *strOfDateAndTime = [NSString stringWithFormat:@"%@ %@ | %@",strOfEventday,strOfEventTime,dateString];
    //    labelEventDateAndtime.text = strOfDateAndTime;
    //    labelEventDateAndtime.numberOfLines = 1;
    //    labelEventDateAndtime.minimumFontSize = 8.;
    //    labelEventDateAndtime.adjustsFontSizeToFitWidth = YES;
    //    labelEventDateAndtime.textColor = [UIColor whiteColor];
    //
    //     [cell.contentView addSubview:labelEventDateAndtime];
    
    
    
    
    
    
    
    
    NSArray *arrOfDateAndTime = [[arrOfEventDateAndtime objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
    
    NSLog(@"%@",arrOfDateAndTime);
    
    
    //    NSString *myString = [arrOfDateAndTime objectAtIndex:1];
    //    myString = [myString stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //    dateFormatter.dateFormat = @"yyyy-dd-MM";
    //    NSDate *yourDate = [dateFormatter dateFromString:myString];
    //    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    //    NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
    

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-dd-MM"]; //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date = [dateFormatter dateFromString: [arrOfDateAndTime objectAtIndex:1]]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];// here set format which you want...
    NSString *dateString = [dateFormatter stringFromDate:date];// here convert date in NSSt
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"HH:mm:ss";
    NSDate *date1 = [dateFormatter1 dateFromString:[arrOfDateAndTime objectAtIndex:0]];
    dateFormatter1.dateFormat = @"hh:mm a";
    NSString *strOfEventTime = [dateFormatter1 stringFromDate:date1];
    
    NSLog(@"Converted String : %@",strOfEventTime);
    
    NSDateFormatter *dateFormatForDay = [[NSDateFormatter alloc] init];
    
    [dateFormatForDay setDateFormat:@"EE"];
    NSString *strOfEventday = [dateFormatForDay stringFromDate:date];
    
    
    NSString *strOfDateAndTime = [NSString stringWithFormat:@"%@ %@ | %@",strOfEventday,strOfEventTime,dateString];
    labelEventDateAndtime.text = strOfDateAndTime;
    labelEventDateAndtime.numberOfLines = 1;
    labelEventDateAndtime.minimumFontSize = 8.;
    labelEventDateAndtime.adjustsFontSizeToFitWidth = YES;
    labelEventDateAndtime.textColor = [UIColor whiteColor];
    
    [cell.contentView addSubview:labelEventDateAndtime];
    
    
    
    
    
    
    
    labelEventAdreess.text = [arrOfEventAddress objectAtIndex:indexPath.row];
    
    labelEventAdreess.textColor = [UIColor whiteColor];
    
    [cell.contentView addSubview:labelEventAdreess];
    
    
    /// for Comment Count
    
    
    
    
    [buttonCommentCount addTarget:self action:@selector(buttonCommentAction:) forControlEvents:UIControlEventTouchUpInside];
    buttonCommentCount.layer.cornerRadius = 8.0;
    buttonCommentCount.layer.masksToBounds = YES;
    buttonCommentCount.tag = indexPath.row;
    buttonCommentCount.layer.backgroundColor = colorForViewScene.CGColor;
    [cell.contentView addSubview:buttonCommentCount];
    
    
    
    
    imageViewComment.image = [UIImage imageNamed:@"comment.png"];
    [cell.contentView addSubview:imageViewComment];
    
    
    
    labelEventCommentCount.text = [arrOfEventCommentCount objectAtIndex:indexPath.row];
    
    labelEventCommentCount.textColor = [UIColor whiteColor];
    
    [cell.contentView addSubview:labelEventCommentCount];
    
    // for Notification
    
    
    [buttonNotificationCount addTarget:self action:@selector(buttonNotificationAction:) forControlEvents:UIControlEventTouchUpInside];
    buttonNotificationCount.layer.cornerRadius = 8.0;
    buttonNotificationCount.layer.masksToBounds = YES;
    buttonNotificationCount.layer.backgroundColor = colorForViewScene.CGColor;
    buttonNotificationCount.tag = indexPath.row;
    [cell.contentView addSubview:buttonNotificationCount];
    
    
    
    
    imageViewNotification.image = [UIImage imageNamed:@"Notification.png"];
    [cell.contentView addSubview:imageViewNotification];
    
    
    
    labelEventNotificationCount.text = [arrOfEventNotificationCount objectAtIndex:indexPath.row];
    
    labelEventNotificationCount.textColor = [UIColor whiteColor];
    
    [cell.contentView addSubview:labelEventNotificationCount];
    
    return cell;
    
    
}

-(void) buttonNotificationAction:(id)sender

{
    UIButton *buttonNotify =(UIButton *)sender;
    
    NSLog(@"%d",buttonNotify.tag);
    strOfEventId = [arrOfEventId objectAtIndex:buttonNotify.tag];
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void) buttonCommentAction:(id)sender
{
    UIButton *button =(UIButton *)sender;
    NSLog(@"%@",[arrOfEventId objectAtIndex:button.tag]);
    strOfCommentFlag = @"YES";
    strOfSceneName = @"MyScene";
    strOfEventImage = [arrOfEventImages objectAtIndex:button.tag];
    strOfEventDateAndTime = [arrOfEventDateAndtime objectAtIndex:button.tag];
    strOfEventDescription = [arrOfEventDescription objectAtIndex:button.tag];
    strOfEventName = [arrOfEventName objectAtIndex:button.tag];
    strOfEventPlace = [arrOfEventAddress objectAtIndex:button.tag];
    strOfEventType = [arrOfEventType objectAtIndex:button.tag];
    eventLatitude = [[arrOfEventLatitude objectAtIndex:button.tag] floatValue];
    eventlongitude = [[arrOfEventLongitude objectAtIndex:button.tag] floatValue];
    strOfEventId = [arrOfEventId objectAtIndex:button.tag];
    strOfEventOwnerId = [arrOfEventOwnerId objectAtIndex:button.tag];
    strOfEventStreet = [arrOfEventStreet objectAtIndex:button.tag];
    strOfEventHouseNo = [arrOfEventhouseNo objectAtIndex:button.tag];
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"viewEvent"];
    [self.navigationController pushViewController:viewController animated:NO];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    strOfCommentFlag = @"NO";
    strOfSceneName = @"MyScene";
    strOfEventImage = [arrOfEventImages objectAtIndex:indexPath.row];
    strOfEventDateAndTime = [arrOfEventDateAndtime objectAtIndex:indexPath.row];
    strOfEventDescription = [arrOfEventDescription objectAtIndex:indexPath.row];
    strOfEventName = [arrOfEventName objectAtIndex:indexPath.row];
    strOfEventPlace = [arrOfEventAddress objectAtIndex:indexPath.row];
    strOfEventType = [arrOfEventType objectAtIndex:indexPath.row];
    eventLatitude = [[arrOfEventLatitude objectAtIndex:indexPath.row] floatValue];
    eventlongitude = [[arrOfEventLongitude objectAtIndex:indexPath.row] floatValue];
    strOfEventId = [arrOfEventId objectAtIndex:indexPath.row];
    strOfEventOwnerId = [arrOfEventOwnerId objectAtIndex:indexPath.row];
    strOfEventInvitedStatus = @"";
    strOfEventStreet = [arrOfEventStreet objectAtIndex:indexPath.row];
    strOfEventHouseNo = [arrOfEventhouseNo objectAtIndex:indexPath.row];
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"viewEvent"];
    [self.navigationController pushViewController:viewController animated:NO];
    
}


-(void) viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
}

- (IBAction)RightMenuDrawer:(id)sender
{
    if (rightClickEvent==YES) {
        _buttonRightHide.hidden = NO;
        rightClickEvent = NO;
    }
    else
    {
        _buttonRightHide.hidden = YES;
        rightClickEvent = YES;
    }
    
    [self.ViewController rightRevealToggle:nil];
    
}

-(void) DrawerMenuAction
{
    _buttonRightHide.hidden = YES;
    rightClickEvent = YES;
    [self.ViewController rightRevealToggle:nil];
}




- (IBAction)BackToHome:(id)sender
{
    UIViewController * viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
    [self.navigationController pushViewController:viewcontroller animated:NO];
    
}



@end
