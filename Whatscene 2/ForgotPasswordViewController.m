//
//  ForgotPasswordViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 12/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()<UITextFieldDelegate,UINavigationControllerDelegate,UINavigationBarDelegate>
{

    NSString * savedValue;
    NSString *valueToSave;
}

@property (strong, nonatomic) IBOutlet UIButton *buttonForSubmitEmailorMnumber;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForEmailORMobilenumber;
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    UIView *paddingEmailORMobilenumber;
    
   
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingEmailORMobilenumber = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
     
    }
    else
    {
        paddingEmailORMobilenumber = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
       
        
    }
    
    _textFieldForEmailORMobilenumber.leftView = paddingEmailORMobilenumber;
    _textFieldForEmailORMobilenumber.leftViewMode = UITextFieldViewModeAlways;
    
    
    

    

[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
    
    _buttonForSubmitEmailorMnumber.layer.cornerRadius = 5.0f;
    _buttonForSubmitEmailorMnumber.clipsToBounds=YES;

    
    
    
    UIColor *color =[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];

    
    [_textFieldForEmailORMobilenumber setValue:color forKeyPath:@"_placeholderLabel.textColor"];

    
    
}


//Resign the responder for All TextField Of Screen

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    return YES;
    
}



/////////////Check EmailID from Server/////////
-(BOOL)validateEmail:(NSString*)emailid
{
    
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
   NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:emailid];
    
}

-(BOOL)validateMobileNumber:(NSString*)mobileNumber
{
    NSString *regex = @"[0-9]{10}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:mobileNumber];
    
    
    
    
}



- (IBAction)SubmitEmailId:(id)sender
{
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];

    if ([self validateEmail:[_textFieldForEmailORMobilenumber text]]==1 ||![_textFieldForEmailORMobilenumber.text stringByTrimmingCharactersInSet:charSet] || [self validateMobileNumber:[_textFieldForEmailORMobilenumber text]]==1)
    {
        
        
        NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/forgotpassword_sub.php?forgot_email=%@",_textFieldForEmailORMobilenumber.text];
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *err;
        if (data !=nil )
        {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
            if (err) {
                NSLog(@"err---- %@",err.description);
            }
            else{
                NSLog(@"Result = %@",json);
                NSLog(@"calltorequestviewcontroller");
                NSLog(@"Result = %@",[[json valueForKey:@"result"]valueForKey:@"response"]);
                
                /////////////////////////////////How to Use NSUSERDEFAULT//////////////////////////////////////////////////////////////
                //Receive value for NSUSERDEFAULT//
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                
                NSString* emailid = _textFieldForEmailORMobilenumber.text;
                [defaults setObject:emailid forKey:@"textField1Text"];
                [defaults synchronize];
                
                ///////Retrive Value From NSUSERDEFAULT/////////
                NSString* temp = [defaults objectForKey:@"textField1Text"];
                NSLog(@"%@",temp);
                
                
                NSLog(@"%@",[json valueForKey:@"success"]);
                
                
                if ([[json valueForKey:@"success"] isEqualToString:@"true"])
                {
                    
                    
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"code"];
                    [self.navigationController pushViewController:vc animated:NO];
                    
                    NSLog(@"Login Successfully");
                    
                    
                    
                    
                    
                    NSLog(@"%@",savedValue);
                    
                    
                }else
                {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter register email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    
                }
                
            }
            
            
            
            
            
            
        }
        
//------------------------------------------Dipak Code For Internet Check-------------------------------------------//
        else
        {
            Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
            [internetReach startNotifier];
            NetworkStatus netStatus = [internetReach currentReachabilityStatus];
            
            
            
            if(netStatus == NotReachable)
            {
                
                UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
                [networkAlert show];
                
                
            }

        
        
        }
        
        
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter valid email id / mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
   

    NSLog(@"done");

    
    
    
    
}
- (IBAction)BackButton:(id)sender
{
    UIViewController * viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:viewcontroller animated:NO];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
