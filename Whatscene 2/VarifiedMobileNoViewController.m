//
//  VarifiedMobileNoViewController.m
//  Whatscene
//
//  Created by Black Bean Engagement on 13/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "VarifiedMobileNoViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define NUMERIC                 @"1234567890"
#import "AsyncImageView.h"
@interface VarifiedMobileNoViewController ()<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    NSString *strOfFbUserId;
}
@property (strong, nonatomic) IBOutlet UITextField *textmobileNo;
@property (strong, nonatomic) IBOutlet UITextField *textFirstName;
@property (strong, nonatomic) IBOutlet UITextField *textLastName;
@property (strong, nonatomic) IBOutlet UITextField *textEmail;
@property (strong, nonatomic) IBOutlet AsyncImageView *imageViewForUserSelFie;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
extern NSString *strOfFbID;
extern NSDictionary *dictOfFBData;
@implementation VarifiedMobileNoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:57.0/255.0 alpha:1.0]];
    
    NSLog(@"%@",dictOfFBData);
    _activityIndicator.hidden = YES;

     UIColor *color =[UIColor  colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.15];
    
    [_textFirstName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
     [_textEmail setValue:color forKeyPath:@"_placeholderLabel.textColor"];
     [_textLastName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
     [_textmobileNo setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    
    

    
    
    _textFirstName.text = [dictOfFBData valueForKey:@"first_name"];

    _textLastName.text = [dictOfFBData valueForKey:@"last_name"];
    NSArray *key = [dictOfFBData allKeys];
    if ([key containsObject:@"email"]) {
        _textEmail.text = [dictOfFBData valueForKey:@"email"];
    }
    _imageViewForUserSelFie.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"id"]]];
    _imageViewForUserSelFie.layer.cornerRadius = _imageViewForUserSelFie.frame.size.height/2;
    _imageViewForUserSelFie.layer.masksToBounds = YES;

    // Do any additional setup after loading the view.
}
-(void) viewWillAppear:(BOOL)animated
{
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:57.0/255.0 alpha:1.0]];
}

-(BOOL)validateMobileNumber:(NSString*)mobileNumber
{
    NSString *regex = @"[0-9]{10}";
     NSPredicate *mobileNOPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [mobileNOPredicate evaluateWithObject:mobileNumber];
    
    
    
    
}


-(BOOL)validateEmail:(NSString*)emailid
{
    
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
  NSPredicate  *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:emailid];
    
}

- (IBAction)buttonSubmit:(id)sender {
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    
    
    if ([[_textFirstName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {

        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter firstname" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    
    }
    else     if ([[_textLastName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter firstname" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
  else   if ([self validateMobileNumber:[_textmobileNo text]]!=1)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid mobile number" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
     else  if ([self validateEmail:[_textEmail text]]!=1)
     {
         UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid email" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
         [alert show];
     }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        dispatch_async(kBgQueue, ^{
            
            [self signUpWithfacebook];
           
            
        });
    }
  
   
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    NSLog(@"%@",string);
    
    NSString * stroftextKg;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    
    if (textField == _textmobileNo)
    {
        if (_textmobileNo.text.length >=10 && range.length == 0)
        {
            return NO;
        }
        stroftextKg = [_textmobileNo.text stringByReplacingCharactersInRange:range withString:string];
        
        return [string isEqualToString:filtered];
    }
    
    
    
    
    
    
    
    
    return YES;
}




-(void) signUpWithfacebook
{
    
    //------------------------Delete cookies & keyboard notification method---------------------------//
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *each in cookieStorage.cookies) {
        [cookieStorage deleteCookie:each];
}
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebooklogin.php?firstname=%@&lastname=%@&password=&email=%@&mob=%@&id=%@",_textFirstName.text,_textLastName.text ,_textEmail.text,_textmobileNo.text,[dictOfFBData valueForKey:@"id"]];
      strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
            
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                strOfFbID =[[[jsondata valueForKey:@"data"]valueForKey:@"fbid"]objectAtIndex:0];
                strOfFbUserId =[[[jsondata valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0];
                NSLog(@"Signup Successfully");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    [_activityIndicator stopAnimating];
                    _activityIndicator.hidden = YES;
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"notificatio_status"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"LoginStatus"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"LoginFB" forKey:@"FBLoginStatus"];
//                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0] forKey:@"image"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"firstname"]objectAtIndex:0] forKey:@"firstname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"lastname"]objectAtIndex:0] forKey:@"lastname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0] forKey:@"LoginUserId"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                    NSArray *key = [[[jsondata valueForKey:@"data"] objectAtIndex:0] allKeys];
                    if ([key containsObject:@"mobno"]) {
                        [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"mobno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                    }
                    if ([key containsObject:@"dob"]) {
                        [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"dob"]objectAtIndex:0] forKey:@"LoginUserDOB"];
                    }
                    
                    
                    
                    NSLog(@"Downloading...");
                    
                    dispatch_async(kBgQueue, ^{
                        
                         [self uploadFBImage];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self uploadImage];
                            
                        });
                        
                    });
                    
      
                   
          
//                   // [self uploadContact];
//                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
//                    [self.navigationController pushViewController:vc animated:NO];
                    
                    
                    
                    
                    
//                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
//                    [self.navigationController pushViewController:vc animated:YES];
                    
                });
            }
            else if([[jsondata valueForKey:@"error"] isEqualToString:@"provided email id/mobile number already exists."])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    _activityIndicator.hidden = YES;
                    [_activityIndicator stopAnimating];
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This email / mobile number already in use" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    
                });
            }

            
            
            
        }
    }
    
}

-(void) uploadImage
{
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png",docDir];
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(_imageViewForUserSelFie.image)];
    [data1 writeToFile:pngFilePath atomically:YES];
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
    [self.navigationController pushViewController:vc animated:YES];
    
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    
}



-(void) uploadFBImage
{
    ///home/listo8mw/public_html/IMAGEDEMO/uploadImage.php
    
    
    
    NSData *imgData = UIImagePNGRepresentation(self.imageViewForUserSelFie.image);
    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/userimageupload.php?user_id=%@",strOfFbUserId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"user%@",strOfFbUserId]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imgData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    NSMutableDictionary *dict=[arr objectAtIndex:0];
    NSLog(@"Data in Dixtionary %@",dict);
}

- (IBAction)tapActionForGetImage:(UITapGestureRecognizer *)sender
{
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose from library", nil];
    [action showInView:self.view];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    

     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate=self ;
    picker.allowsEditing = YES;
    if (buttonIndex==0) // take photo From camera
    {
 
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
      
    }
    else if (buttonIndex==1) // take photo From Gallery
    {
 
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    }
      [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark UIPickerViewDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
  
    self.imageViewForUserSelFie.image = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return  YES;
}


@end
