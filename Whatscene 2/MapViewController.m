//
//  MapViewController.m
//  Whatscene
//
//  Created by Black Bean Engagement on 13/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import "Anno.h"

@interface MapViewController ()<MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *map;

@end
extern CGFloat eventLatitude,eventlongitude;
extern NSString *strOfEventPlace,*strOfEventName;
@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getMapPin];
    // Do any additional setup after loading the view.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (mapView != _map || [annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    static NSString *annotationIdentifier = @"SPGooglePlacesAutocompleteAnnotation";
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[_map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier] ;
    }
    annotationView.animatesDrop = YES;
    annotationView.canShowCallout = YES;
    
    return annotationView;
    
    
    
}


-(void) getMapPin
{
    MKCoordinateRegion Region;
    
    
        
        Region.center.latitude=eventLatitude;
        Region.center.longitude=eventlongitude;
        Region.span.latitudeDelta=0.15;
        Region.span.longitudeDelta=0.15;
        [_map setRegion:Region];
        
       Anno *ann =[[Anno alloc]init];
        ann.title =strOfEventName;
          ann.subtitle =strOfEventPlace;
        ann.coordinate = Region.center;
        
        [_map addAnnotation:ann];
        
 
    
}

- (IBAction)buttonBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
