//
//  MysceneTableViewCell.m
//  Whatscene
//
//  Created by Alet Viegas on 09/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "MysceneTableViewCell.h"

@implementation MysceneTableViewCell
@synthesize labelForTableviewcellImageTransperancy,labelForDate,labelForSceneName;
- (void)awakeFromNib {
    // Initialization code
    _buttonCommentForMyScene.layer.cornerRadius =10; // this value vary as per your desire
    _buttonCommentForMyScene.clipsToBounds = YES;
    
    _buttonNotificationForMyScene.layer.cornerRadius =10; // this value vary as per your desire
    _buttonNotificationForMyScene.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
