//
//  imageCircle.m
//  Whatscene
//
//  Created by Black Bean Engagement on 14/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "imageCircle.h"

@implementation imageCircle





- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
        self.layer.masksToBounds = YES;
}

@end
