//
//  NearBySceneTableViewCell.h
//  Whatscene
//
//  Created by Alet Viegas on 09/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearBySceneTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelForSceneNameG;
@property (weak, nonatomic) IBOutlet UILabel *labelForEventDateAndTime;

@property (weak, nonatomic) IBOutlet UILabel *labelForEventLocation;

@property (strong, nonatomic) IBOutlet UILabel *labelForTableViewTranperancy;

@end
