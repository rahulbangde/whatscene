//
//  CreateNewSceneViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 12/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "CreateNewSceneViewController.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import <MapKit/MapKit.h>
#import "AsyncImageView.h"
#import <CoreLocation/CoreLocation.h>
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface CreateNewSceneViewController ()<UITextFieldDelegate,UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, MKMapViewDelegate,UIAlertViewDelegate>
{
    
    UIView *_subview;
    
    UIButton * clickbox;
    BOOL checked;
    UIImagePickerController *imagePicker;
    
    NSArray *searchResultPlaces;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    MKPointAnnotation *selectedPlaceAnnotation;
    
    BOOL shouldBeginEditing;
    MKMapView * mapView;
    NSMutableArray * arrayOfImageData;
    NSError * err;
    UIImage *chosenImage;
    NSString *theDate;
    CLLocationCoordinate2D coordinate;
    
    UIDatePicker * datePicker;
    NSString * strEventId;
   // NSString *strOfEventType;
    UIScrollView *scrollViewForPreviewEvent;
    
    UIActivityIndicatorView * activityIndicator;
    
    
    
    
}
@property (strong, nonatomic) IBOutlet UILabel *labelPrivateBackground;
@property (strong, nonatomic) IBOutlet UILabel *labelPublicBackground;
@property (strong, nonatomic) IBOutlet UIView *ccontentView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableViewAdress;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewForEventImage;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForNameYourScene;


@property (strong, nonatomic) IBOutlet UITextView *textViewForTellUs;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForHouseAndBuildingName;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForStreetName;


@property (strong, nonatomic) IBOutlet UIDatePicker *datePickerForCreateNewScene;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewForCreateScene;


@property (strong, nonatomic) IBOutlet UITextField *textFieldForMainLocation;


//@property (strong, nonatomic) IBOutlet UIView *ViewForCreateNewSceneBaseScrollView;

@property (strong, nonatomic) IBOutlet UIButton *buttonForCheckBox;

@property (strong, nonatomic) IBOutlet UIButton *buttonForPublicImage;
@property (strong, nonatomic) IBOutlet UIButton *buttonForPrivateImage;

@property (strong, nonatomic) IBOutlet UILabel *labelForPublicTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelForPrivateTitle;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)SetDestinationCheckBoxAction:(id)sender;

@end

// For Get value from anther View
NSString *strOfEventDateAndTimeForPreview,*strOfEventDescriptionForPreview,*strOfEventNameForPreview,*strOfEventPlaceForPreview,*strOfEventTypeForPreview;
UIImage *imageForPreview;



extern NSString *strOfEventImage,*strOfEventDateAndTime,*strOfEventDescription,*strOfEventName,*strOfEventPlace,*strOfEventType,*strOfEventId,*strOfCreateNewSceneFlag,*strOfEventOwnerId,*strOfEventStreet,*
strOfEventHouseNo;
extern UIColor *colorForViewScene;
extern CGFloat eventLatitude,eventlongitude;
@implementation CreateNewSceneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    _activityIndicator.hidden = YES;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
       // Do any additional setup after loading the view.
    // set As default Location
    
//    CLLocationCoordinate2D center;
//    center =[self getLocationFromAddressString:@"uthangarai"];
//    double  latFrom=&center.latitude;
//    double  lonFrom=&center.longitude;
//    
//    NSLog(@"View Controller get Location Logitute : %f",latFrom);
//    NSLog(@"View Controller get Location Latitute : %f",lonFrom);
    
    
    UIView *paddingViewLocation;
    UIView *rightPaddingView;
    UIView *paddingHouseAndBuildingName;
    UIView *paddingViewStreetName;
         UIView *paddingViewNameYourScene;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingViewLocation = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        paddingHouseAndBuildingName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingViewStreetName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
             paddingViewNameYourScene = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    }
    else
    {
        paddingViewLocation = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
          paddingHouseAndBuildingName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingViewStreetName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
             paddingViewNameYourScene = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    }
    
    

    _textFieldForMainLocation.leftView = paddingViewLocation;
    _textFieldForMainLocation.leftViewMode = UITextFieldViewModeAlways;
    _textFieldForMainLocation.rightView = rightPaddingView;
    _textFieldForMainLocation.rightViewMode = UITextFieldViewModeAlways;
//
    

    _textFieldForHouseAndBuildingName.leftView = paddingHouseAndBuildingName;
    _textFieldForHouseAndBuildingName.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    _textFieldForStreetName.leftView = paddingViewStreetName;
    _textFieldForStreetName.leftViewMode = UITextFieldViewModeAlways;
//
    

    _textFieldForNameYourScene.leftView = paddingViewNameYourScene;
    _textFieldForNameYourScene.leftViewMode = UITextFieldViewModeAlways;
//
    _textViewForTellUs.textContainer.lineFragmentPadding = 5;
    
    
    
    if(  [[[NSUserDefaults standardUserDefaults]valueForKey:@"checkBoxStatus"] isEqualToString:@"checkBoxSelected"])
    {
       // checkBoxSelected
         [_buttonForCheckBox setImage:[UIImage imageNamed:@"CHECk1.png"] forState:UIControlStateNormal];
        _textFieldForMainLocation.text  =  [[NSUserDefaults standardUserDefaults]valueForKey:@"MainLocation"];
        _textFieldForHouseAndBuildingName.text =  [[NSUserDefaults standardUserDefaults]valueForKey:@"HouseName"];
        _textFieldForStreetName.text =  [[NSUserDefaults standardUserDefaults]valueForKey:@"StreetName"];

    }
    else
    {
        [_buttonForCheckBox setImage:[UIImage imageNamed:@"UNCHECK.png"] forState:UIControlStateNormal];
    }
   
    
    
    
    [_datePickerForCreateNewScene setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    [_datePickerForCreateNewScene setSelected:YES];
    //[_datePickerForCreateNewScene set]
    
    
    
    //Placeholder
    // _contributionTextView.text = @"Contribution";
    _textViewForTellUs.text = @"Tell Us Something about this scene";
    
    
    self.view.backgroundColor = colorForViewScene;
    _ccontentView.backgroundColor = colorForViewScene;
    
    // http://whatscene.eventbuoy.com/dev/admin/admin/addevent.php?name=hello&event_category=Public&eventdate=2015-23-04&eventday=Monday&eventtime=11:30:00&location=Pune&latitude=19.521054&longitude=75.857002&description=Test%20Event&userid=67
    
    
//    
//    [_buttonForPublicImage setTintColor:[UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0]];
//    _labelForPublicTitle.textColor = [UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0];
    

    
    if ([strOfEventOwnerId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]]) {
        
        //----------------------------- predefine value For Edit Event -----------------------------//
        
        if (strOfEventId != nil) {
            _textFieldForHouseAndBuildingName.text = strOfEventHouseNo;
             _textFieldForStreetName.text = strOfEventStreet;
            _textFieldForNameYourScene.text = strOfEventName;
            _textFieldForMainLocation.text = strOfEventPlace;
            _textViewForTellUs.text = strOfEventDescription;
            _imageViewForEventImage.imageURL = [NSURL URLWithString:strOfEventImage];
            
            
            NSArray *arrOfDateAndTime = [strOfEventDateAndTime componentsSeparatedByString:@"|"];
            
            NSLog(@"%@",arrOfDateAndTime);
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter setDateFormat:@"yyyy-dd-MM HH:mm:ss"]; //// here set format of date which is in your output date (means above str with format)
            
            
            NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",[arrOfDateAndTime objectAtIndex:1],[arrOfDateAndTime objectAtIndex:0]]]; // here you can fetch date from string with define format
            NSLog(@"%@",date);
            [_datePickerForCreateNewScene setDate:date];
            
            
            
            if (![strOfEventType isEqualToString:@"Private"]) {
//                [_buttonForPublicImage setTintColor:[UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0]];
//                _labelForPublicTitle.textColor = [UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0];
                _labelForPublicTitle.textColor = [UIColor whiteColor];
                   [_buttonForPublicImage setTintColor:[UIColor whiteColor]];
                [_buttonForPrivateImage setTintColor:[UIColor whiteColor]];
                _labelForPrivateTitle.textColor = [UIColor whiteColor];
                strOfEventTypeForPreview = @"Public";
                _labelPublicBackground.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:164.0/255.0 blue:126.0/255.0 alpha:1.0];
                       _labelPrivateBackground.backgroundColor = [UIColor clearColor];
                
            }
            else{
                [_buttonForPublicImage setTintColor:[UIColor whiteColor]];
                _labelForPublicTitle.textColor = [UIColor whiteColor];
                
                [_buttonForPrivateImage setTintColor:[UIColor whiteColor]];
                _labelForPrivateTitle.textColor = [UIColor whiteColor];
//                [_buttonForPrivateImage setTintColor:[UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0]];
//                _labelForPrivateTitle.textColor = [UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0];
                strOfEventTypeForPreview = @"Private";
                _labelPublicBackground.backgroundColor = [UIColor clearColor];
                _labelPrivateBackground.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:164.0/255.0 blue:126.0/255.0 alpha:1.0];
         
            }
            
            
            
            //--------------------------------------------------------------------------------------------//

        }
        
        

    }
    else
    {
        
        
        //_imageViewForEventImage.image = [UIImage imageNamed:@""];
        [_buttonForPublicImage setTintColor:[UIColor whiteColor]];
        _labelForPublicTitle.textColor = [UIColor whiteColor];
        [_buttonForPrivateImage setTintColor:[UIColor whiteColor]];
        _labelForPrivateTitle.textColor = [UIColor whiteColor];
        strOfEventTypeForPreview = @"Public";
        strOfEventType = @"Public";
        _labelPublicBackground.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:164.0/255.0 blue:126.0/255.0 alpha:1.0];
        _labelPrivateBackground.backgroundColor = [UIColor clearColor];
    }
    
    
   
    
    
    _tableViewAdress.hidden = YES;
    
    //////////////////////////////////////Search place From map View//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
    searchQuery.radius = 100.0;
    shouldBeginEditing = YES;
    self.searchDisplayController.searchBar.placeholder = @"Search or Address";
    
    
    
    //Change the color  of placeholder for textfield
    
    
    UIColor *color =[UIColor  colorWithRed:255.0 green:255.0 blue:255.0 alpha:1];
    
    [_textFieldForNameYourScene setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForMainLocation setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForHouseAndBuildingName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForStreetName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    
    

    
    
    //Check Box Boolean value
    checked = NO;
    

    
    //Camera Alert View
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                            delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    
    
    
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    [_textViewForTellUs resignFirstResponder];
    
}




- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = @"sample title";
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}




- (IBAction)SetDestinationCheckBoxAction:(id)sender
{
    
    if(!checked)
    {
        [_buttonForCheckBox setImage:[UIImage imageNamed:@"CHECk1.png"] forState:UIControlStateNormal];
        checked=YES ;
        [[NSUserDefaults standardUserDefaults]setObject:@"checkBoxSelected" forKey:@"checkBoxStatus"];
        [[NSUserDefaults standardUserDefaults]setObject:_textFieldForMainLocation.text forKey:@"MainLocation"];
        [[NSUserDefaults standardUserDefaults]setObject:_textFieldForHouseAndBuildingName.text forKey:@"HouseName"];
        [[NSUserDefaults standardUserDefaults]setObject:_textFieldForStreetName.text forKey:@"StreetName"];
        
        
    }
    else if(checked)
    {
        [_buttonForCheckBox setImage:[UIImage imageNamed:@"UNCHECK.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"checkBoxStatus"];
        checked=NO;
        
    }
    
    
}

- (IBAction)CameraAndEventImageButtonPopUp:(UITapGestureRecognizer *)sender
{
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose from library", nil];
    [action showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    
    picker.delegate=self ;
    if (buttonIndex==0) // take photo From camera
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
       
        
        if(IS_OS_8_OR_LATER)
            
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // Place image picker on the screen
                [self presentViewController:picker animated:YES completion:nil];
            }];
        }
        else
        {
             [self presentViewController:picker animated:YES completion:nil];
            
        }
    }
    else if (buttonIndex==1) // take photo From Gallery
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
      
        
     //   self.shouldCallViewWillAppear = NO;
        

        if(IS_OS_8_OR_LATER)
       
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // Place image picker on the screen
                [self presentViewController:picker animated:YES completion:nil];
            }];
        }
        else
        {
              [self presentViewController:picker animated:YES completion:nil];
        }
        
    }
}
#pragma mark UIPickerViewDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageViewForEventImage.image = chosenImage;
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
    
    
    
}






- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    self.navigationController.navigationBar.hidden = NO;
    
    
}


- (IBAction)BackButton:(id)sender
{
    
    
//    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"nearby"];
//    [self.navigationController pushViewController:vc animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"done");
    
    
    
}


//Google api to search places


#pragma mark -
#pragma mark UITableViewDelegate





- (void)dismissSearchControllerWhileStayingActive
{
    // Animate out the table view.
    NSTimeInterval animationDuration = 0.3;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.searchDisplayController.searchResultsTableView.alpha = 0.0;
    [UIView commitAnimations];
    
    [self.searchDisplayController.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchDisplayController.searchBar resignFirstResponder];
}



#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    searchQuery.location = self->mapView.userLocation.coordinate;
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
        } else {
            [searchResultPlaces release];
            searchResultPlaces = [places retain];
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self handleSearchForSearchString:searchString];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark -
#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (![searchBar isFirstResponder])
    {
        // User tapped the 'clear' button.
        shouldBeginEditing = NO;
        [self.searchDisplayController setActive:NO];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (shouldBeginEditing) {
        // Animate in the table view.
        NSTimeInterval animationDuration = 0.3;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        self.searchDisplayController.searchResultsTableView.alpha = 1.0;
        [UIView commitAnimations];
        
        [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:YES];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

- (IBAction)PublicEventSelection:(id)sender
{
    
    
    
}


- (IBAction)buttonChooseEventType:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button.tag == 1)
    {
//        [_buttonForPublicImage setTintColor:[UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0]];
//        _labelForPublicTitle.textColor = [UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0];
        
        
        _labelPublicBackground.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:164.0/255.0 blue:126.0/255.0 alpha:1.0];
 
  _labelForPublicTitle.textColor = [UIColor whiteColor];
        [_buttonForPublicImage setTintColor:[UIColor whiteColor]];
        
        
        _labelPrivateBackground.backgroundColor = [UIColor clearColor];
        [_buttonForPrivateImage setTintColor:[UIColor whiteColor]];
        _labelForPrivateTitle.textColor = [UIColor whiteColor];
        strOfEventType = @"Public";
         strOfEventTypeForPreview = strOfEventType;
        
    }
    else if (button.tag == 2)
    {
        _labelPublicBackground.backgroundColor = [UIColor clearColor];
        _labelPrivateBackground.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:164.0/255.0 blue:126.0/255.0 alpha:1.0];
        [_buttonForPublicImage setTintColor:[UIColor whiteColor]];
        _labelForPublicTitle.textColor = [UIColor whiteColor];
        
        [_buttonForPrivateImage setTintColor:[UIColor whiteColor]];
        _labelForPrivateTitle.textColor = [UIColor whiteColor];
//        [_buttonForPrivateImage setTintColor:[UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0]];
//        _labelForPrivateTitle.textColor = [UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:23.0/255.0 alpha:1.0];
        strOfEventType = @"Private";
        strOfEventTypeForPreview = strOfEventType;
//        if ([strOfEventOwnerId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]]) {
//            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteeUser"];
//            [self.navigationController pushViewController:viewController animated:YES];
//        }
       
        
    }
    
}







#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"%@",searchResultPlaces);
    return [searchResultPlaces count];
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return [searchResultPlaces objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SPGooglePlacesAutocompleteCell";
    UITableViewCell *cell = [_tableViewAdress dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate

//- (void)recenterMapToPlacemark:(CLPlacemark *)placemark {
//    MKCoordinateRegion region;
//    MKCoordinateSpan

//    
//    span.latitudeDelta = 0.02;
//    span.longitudeDelta = 0.02;
//    
//    region.span = span;
//    region.center = placemark.location.coordinate;
//    
//    //[self.mapView setRegion:region];
//}

//- (void)addPlacemarkAnnotationToMap:(CLPlacemark *)placemark addressString:(NSString *)address {
//    [self.mapView removeAnnotation:selectedPlaceAnnotation];
//    [selectedPlaceAnnotation release];
//
//    selectedPlaceAnnotation = [[MKPointAnnotation alloc] init];
//    selectedPlaceAnnotation.coordinate = placemark.location.coordinate;
//    selectedPlaceAnnotation.title = address;
//    [self.mapView addAnnotation:selectedPlaceAnnotation];
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
   
    NSLog(@"%@",place);
    _textFieldForMainLocation.text = [self placeAtIndexPath:indexPath].name;
    

    [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error)
     {
         if (error) {
             SPPresentAlertViewWithErrorAndTitle(error, @"Could not map selected Place");
         }
         else if (placemark)
         {

             
             eventLatitude = placemark.location.coordinate.latitude;
             eventlongitude = placemark.location.coordinate.longitude;
             //  [self addPlacemarkAnnotationToMap:placemark addressString:addressString];
            // [self recenterMapToPlacemark:placemark];
             //[self dismissSearchControllerWhileStayingActive];
            // [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
         }
     }];
    
    _tableViewAdress.hidden = YES;
    
    
    
}













//#pragma mark -
//#pragma mark TextField Delegate
//
- (BOOL) textField: (UITextField *) textField shouldChangeCharactersInRange: (NSRange) range replacementString: (NSString *) string
{
    if (textField == _textFieldForMainLocation) {
        if (![[textField.text stringByReplacingCharactersInRange:range withString:string]  isEqualToString:@""]) {
            _tableViewAdress.hidden = NO;
           
            // searchQuery.location = self.mapView.userLocation.coordinate;
            searchQuery.input =  [textField.text stringByReplacingCharactersInRange:range withString:string];;
            [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
                if (error) {
                    SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
                } else {
                    [searchResultPlaces release];
                    searchResultPlaces = [places retain];
                    [_tableViewAdress reloadData];
                }
            }];
        }
        else
        {
            _tableViewAdress.hidden = YES;
            shouldBeginEditing = NO;
            searchResultPlaces = [[NSArray alloc] init];
            [_tableViewAdress reloadData];
        }
        
    }
    

    
    return  YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"preparing to show keyboard");
    
    
    //    _scrollView.frame = CGRectMake(0,0,_scrollView.frame.size.width,_scrollView.frame.size.height-self.view.frame.size.height+64); //44:NavigationBar ; 200: Keyoard
    //  CGRect frame = CGRectMake(0, 0, _scrollView.frame.size.width, self.view.frame.size.height+self.view.frame.size.height);
    
    // [_scrollView scrollRectToVisible:frame animated:YES]; //Oddly enough, it only works with the animation...
    if (textField == _textFieldForMainLocation) {
        [_scrollView setContentOffset:CGPointMake(0, 353) animated:YES];
    }
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"prepare to hide keyboard");
    
    if (textField == _textFieldForMainLocation) {
        _scrollView.frame = CGRectMake(0,0,_scrollView.frame.size.width,_scrollView.frame.size.height); //original setup
    }
    
    
    
    
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _textFieldForMainLocation) {
        if (shouldBeginEditing) {
            // Animate in the table view.
            NSTimeInterval animationDuration = 0.3;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            // self.searchDisplayController.searchResultsTableView.alpha = 1.0;
            [UIView commitAnimations];
            
            // [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:YES];
        }
        BOOL boolToReturn = shouldBeginEditing;
        shouldBeginEditing = YES;
        return boolToReturn;
    }
    return  YES;
    
}// return NO to disallow editing.

#pragma mark -
#pragma mark MKMapView Delegate






- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    NSString *action = (NSString*)context;
    
    if([action isEqualToString:@"ANSELECTED"]){
        
        BOOL annotationAppeared = [[change valueForKey:@"new"] boolValue];
        if (annotationAppeared) {
            // clicked on an Annotation
            NSLog(@"Rahul");
        }
        else {
            // Annotation disselected
            NSLog(@"jgjgbjhbg");
        }
    }
}



- (void)annotationDetailButtonPressed:(id)sender {
    // Detail view controller application logic here.
    
}


//- (IBAction)recenterMapToUserLocation:(id)sender {
//    MKCoordinateRegion region;
//    MKCoordinateSpan span;
//
//    span.latitudeDelta = 0.02;
//    span.longitudeDelta = 0.02;
//
//    region.span = span;
//    region.center = self.mapView.userLocation.coordinate;
//
//    [self.mapView setRegion:region animated:YES];
//}

- (IBAction)CreateEventForWhatScene:(id)sender
{
    
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
   
  
    
    if ([[_textFieldForNameYourScene.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter event name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
        if ([[_textViewForTellUs.text stringByTrimmingCharactersInSet:charSet]  isEqualToString:@""] && [_textViewForTellUs.text isEqualToString:@"Tell Us Something about this scene"]) {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter event description" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }
    
        else
            if ([[_textFieldForMainLocation.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""]) {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter event description" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
    else if ([strOfEventType isEqualToString:@""])
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select event type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
    
    else
    {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        
        dispatch_async(kBgQueue, ^{
            
            [self createNewEvent];
             [self uploadImage];
            dispatch_async(dispatch_get_main_queue(), ^{
                
               
                _activityIndicator.hidden = YES;
                [_activityIndicator stopAnimating];
            });
            
        });

        
       
        
      
    }
}

-(void) createNewEvent
{
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    
    
    NSDate *myDate = _datePickerForCreateNewScene.date;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDateFormatter *dateFormatForDay = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-dd-MM"];
    [dateFormatForDay setDateFormat:@"EEEE"];
    NSString  *strOfEventDate = [dateFormat stringFromDate:myDate];
    
    NSString *strOfEventday = [dateFormatForDay stringFromDate:myDate];
    
    NSLog(@"%@",strOfEventDate);
    NSDate *myTime = _datePickerForCreateNewScene.date;
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSString  *strOfEventTime = [timeFormat stringFromDate:myTime];
    NSLog(@"%@",strOfEventTime);
    
    NSLog(@"%@",_textFieldForMainLocation.text);
    
    
    
    NSLog(@"%@",theDate);
    NSLog(@"%f",coordinate.latitude);
    NSLog(@"%f",coordinate.longitude);
   //
    
    NSString *strOfUrl;
    if (netStatus == NotReachable)
    {
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    
   else if (strOfEventId != nil ) {
        strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/editevent.php?name=%@&event_category=%@&eventdate=%@&eventday=%@&eventtime=%@&location=%@&latitude=%f&longitude=%f&description=%@&userid=%@&&eventid=%@",_textFieldForNameYourScene.text,strOfEventType,strOfEventDate,strOfEventday,strOfEventTime,_textFieldForMainLocation.text,eventLatitude,eventlongitude,_textViewForTellUs.text,[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"],strOfEventId];
    }
    else
    {
         strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/addevent.php?name=%@&event_category=%@&eventdate=%@&eventday=%@&eventtime=%@&location=%@&latitude=%f&longitude=%f&description=%@&userid=%@",_textFieldForNameYourScene.text,strOfEventType,strOfEventDate,strOfEventday,strOfEventTime,_textFieldForMainLocation.text,eventLatitude,eventlongitude,_textViewForTellUs.text,[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]];
    }
    
   
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
     NSError *error = nil;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            
            [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"eventid"] objectAtIndex:0] forKey:@"CreateEventID"];
            strEventId = [[[json valueForKey:@"data"]valueForKey:@"eventid"] objectAtIndex:0];
            NSLog(@"Result = %@",[[json valueForKey:@"data"]valueForKey:@"eventid"]);
            NSLog(@"Result = %@",[json valueForKey:@"Success"]);
            NSLog(@"%@",strEventId);
            
            // NSLog(@"Result = %@",[[json valueForKey:@"result"]valueForKey:@"response"]);
            /////////////////////////////////How to Use NSUSERDEFAULT//////////////////////////////////////////////////////////////
            //Receive value for NSUSERDEFAULT//
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            
            NSString* emailid = _textFieldForNameYourScene.text;
            [defaults setObject:emailid forKey:@"username"];
            [defaults synchronize];
            
            ///////Retrive Value From NSUSERDEFAULT/////////
            NSString* temp = [defaults objectForKey:@"username"];
            NSLog(@"%@",temp);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your event create successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag = 2;
                [alert show];
            });
           
            
            
            
            
            
        }
    }
}


-(void)uploadImage
{
    
    
    CGSize newImageSize = CGSizeMake(200, 100);
    self.imageViewForEventImage.image = [self imageWithImage:self.imageViewForEventImage.image scaledToSize:newImageSize];
    
    NSData *imgData = UIImagePNGRepresentation(self.imageViewForEventImage.image);
    
    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/eventimageupload.php?event_id=%@",strEventId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"event%@",strEventId]] dataUsingEncoding:NSUTF8StringEncoding]];    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imgData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    NSMutableDictionary *dict=[arr objectAtIndex:0];
    NSLog(@"Data in Dixtionary %@",dict);
}

- (IBAction)buttonPreview:(id)sender
{
    
    if (checked) {
        [[NSUserDefaults standardUserDefaults]setObject:_textFieldForMainLocation.text forKey:@"MainLocation"];
        [[NSUserDefaults standardUserDefaults]setObject:_textFieldForHouseAndBuildingName.text forKey:@"HouseName"];
        [[NSUserDefaults standardUserDefaults]setObject:_textFieldForStreetName.text forKey:@"StreetName"];
    }
    
    strOfEventPlaceForPreview = _textFieldForMainLocation.text;
    strOfEventNameForPreview = _textFieldForNameYourScene.text;
    strOfEventDescriptionForPreview = _textViewForTellUs.text;
    
    NSDate *myDate = _datePickerForCreateNewScene.date;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDateFormatter *dateFormatForDay = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MMMM-dd"];
    [dateFormatForDay setDateFormat:@"EEEE"];
    NSString  *strOfEventDate = [dateFormat stringFromDate:myDate];
    
    NSString *strOfEventday = [dateFormatForDay stringFromDate:myDate];
    
    NSLog(@"%@",strOfEventDate);
    NSDate *myTime = _datePickerForCreateNewScene.date;
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSString  *strOfEventTime = [timeFormat stringFromDate:myTime];
    NSLog(@"%@",strOfEventTime);
    
    strOfEventDateAndTimeForPreview = [NSString stringWithFormat:@"%@ | %@",strOfEventDate,strOfEventTime];
    imageForPreview = _imageViewForEventImage.image;
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviewEvent"];
    [self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)buttonDeleteScene:(id)sender {
    
    
    
    
    dispatch_async(kBgQueue, ^{
        if (strOfEventId != nil) {
              [self deleteEvent];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to delete this scene" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
            alert.tag = 1;
            [alert show];
        });
      
        
    });
    
}

-(void) deleteEvent
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/deleteevent.php?userid=%@&eventid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"],strOfEventId];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError   *error = nil;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (err) {
            
            
            NSLog(@"err---- %@",error.description);
        }
        else{
            if ([[json valueForKey:@"success"] isEqualToString:@"true"]) {
                
                NSLog(@"Sucessfully");


            }
        }
    }

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (alertView.tag == 1) {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myscene"];
            [self.navigationController pushViewController:vc animated:NO];
        }
        if (alertView.tag == 2) {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myscene"];
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
    
}


- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
   
    if([text isEqualToString:@"\n"]){
        if ( [textView.text isEqualToString:@""]) {
            textView.text = @"Tell Us Something about this scene";
        }
        [textView resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ( [textView.text isEqualToString:@"Tell Us Something about this scene"]) {
        textView.text = @"";
        textView.textColor = [UIColor whiteColor];
        
    }
    return  YES;
}



   

-(void) textViewDidChange:(UITextView *)textView

{
    if(textView.text.length == 0){
        textView.textColor = [UIColor whiteColor];
        textView.text = @"Tell Us Something about this scene";
        [textView resignFirstResponder];
    }
}




- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}

@end
