//
//  PrivacyandPolicyViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 3/19/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "PrivacyandPolicyViewController.h"

@interface PrivacyandPolicyViewController ()

@end

@implementation PrivacyandPolicyViewController

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

 [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    
    
    
}




- (IBAction)BackButton:(id)sender
{
    
    UIViewController * viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:viewcontroller animated:YES];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,navigationBar.frame.size.height-1,navigationBar.frame.size.width, 0.5)];
    
    // Change the frame size to suit yours //
    
    [navBorder setBackgroundColor:[UIColor colorWithWhite:200.0f/255.f alpha:0.8f]];
    [navBorder setOpaque:YES];
    [navigationBar addSubview:navBorder];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    self.navigationController.navigationBar.backgroundColor = [UIColor  colorWithRed:127.0 green:127.0 blue:127.0 alpha:1];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
