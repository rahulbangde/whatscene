//
//  NotificationViewController.m
//  Whatscene
//
//  Created by Black Bean Engagement on 17/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "NotificationViewController.h"
#import "InviteeUserTableViewCell.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface NotificationViewController ()
{
    NSMutableDictionary *json;
    
    NSMutableArray *arrOfNotificationId,*arrOfNotificationEventName,*arrOfNotificationDiscription,*arrOfNotificationSenderId;
    NSInteger  index;
}

@property (strong, nonatomic) IBOutlet UITableView *tableViewNotification;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
extern UIColor *colorForViewScene;
extern NSString *strOfEventId;
@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrOfNotificationDiscription  = [NSMutableArray new];
     arrOfNotificationEventName  = [NSMutableArray new];
     arrOfNotificationId  = [NSMutableArray new];
     arrOfNotificationSenderId  = [NSMutableArray new];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    // Do any additional setup after loading the view.
    _activityIndicator.hidden = YES;
    self.view.backgroundColor = colorForViewScene;
}

-(void) viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.hidden = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
       
        _activityIndicator.hidden = NO;
         [_activityIndicator startAnimating];
        
    });
    dispatch_async(kBgQueue, ^{
        
        [self notificationList];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_tableViewNotification reloadData];
            // [self uploadImage];
            
        });
        
    });
}


-(void)notificationList
{
    
    // [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserEmail"];
    
    //strForUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]);
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/displaynotification.php?userid=%@&eventid=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfEventId];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                for (int i = 0; i<[[json valueForKey:@"data"] count]; i++) {
                    [arrOfNotificationEventName addObject:[[[json valueForKey:@"data"] valueForKey:@"event_name"] objectAtIndex:i]];
                    [arrOfNotificationDiscription addObject:[[[json valueForKey:@"data"] valueForKey:@"message"] objectAtIndex:i]];
                    [arrOfNotificationId addObject:[[[json valueForKey:@"data"] valueForKey:@"notification_id"] objectAtIndex:i]];
                    [arrOfNotificationSenderId addObject:[[[json valueForKey:@"data"] valueForKey:@"sender_id"] objectAtIndex:i]];
                }
                NSLog(@"Successfully");

            }
            
        }
        
        
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
          [_activityIndicator stopAnimating];
        _activityIndicator.hidden = YES;
      
        
    });
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [arrOfNotificationEventName count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    InviteeUserTableViewCell *cell=(InviteeUserTableViewCell *)[_tableViewNotification dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
//    NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/images/user%@.png",[[[json valueForKey:@"data"] valueForKey:@"sender_id"] objectAtIndex:indexPath.row]]]);
    cell.imageViewInvteeUser.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/images/user%@.png",[arrOfNotificationSenderId objectAtIndex:indexPath.row]]];
    
    if(cell.imageViewInvteeUser.image == nil)
    {
        cell.imageViewInvteeUser.image = [UIImage imageNamed:@"f3.png"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.imageViewInvteeUser.layer.cornerRadius = cell.imageViewInvteeUser.frame.size.height/2;
    cell.imageViewInvteeUser.layer.masksToBounds = YES; 
    
    cell.labelInviteeName.text = [arrOfNotificationEventName objectAtIndex:indexPath.row];
    cell.labelNotificationDiscription.text = [arrOfNotificationDiscription objectAtIndex:indexPath.row];
    
    return cell;
    
}

- (IBAction)buttonBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
       index = indexPath.row;
        
        dispatch_async(kBgQueue, ^{
          [self deleteNotification];
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [arrOfNotificationId removeObjectAtIndex:indexPath.row];
                [arrOfNotificationDiscription removeObjectAtIndex:indexPath.row];
                [arrOfNotificationEventName removeObjectAtIndex:indexPath.row];
                [arrOfNotificationSenderId removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            });
        });
    
      
       
      
        
        
        // Delete the row from the data source
        
        
    }
}

-(void) deleteNotification
{
    //http://whatscene.eventbuoy.com/dev/admin/admin/deletenotification.php?userid=44&eventid=421&notificationid=107
    

        
        NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/deletenotification.php?userid=%@&eventid=%@&notificationid=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfEventId,[arrOfNotificationId objectAtIndex:index]];
        //  strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
        NSURLResponse *response;
        NSError *err;
        [request setHTTPMethod:@"GET"];
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        
        
        if (data !=nil ) {
            NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
            if (err) {
                NSLog(@"err---- %@",err.description);
            }
            else{
                NSLog(@"Result = %@",jsondata);
                
                
                if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
                {
                    NSLog(@"Delete notification");
                  
                }
                else 
                {
                   
                }
                
                
                
                
            }
        }
        
    
}

@end
