//
//  Anno.h
//  MapViewD
//
//  Created by Pradip on 29/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>
#import <UIKit/UIKit.h>
@interface Anno : NSObject <MKAnnotation>{
    CLLocationCoordinate2D coordinate;
    NSString *title,*subtitle;
    UIImageView *img;
}
@property(nonatomic, copy) NSString *title, *subtitle;
@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, retain) UIImageView *img;
@end
