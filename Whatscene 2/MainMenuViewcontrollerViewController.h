//
//  MainMenuViewcontrollerViewController.h
//  Whatscene
//
//  Created by Alet Viegas on 06/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+DragLoad.h"
@class FrameObservingView;

@protocol FrameObservingViewDelegate <NSObject>
- (void)frameObservingViewFrameChanged:(FrameObservingView *)view;
@end

@interface FrameObservingView : UIView
@property (nonatomic,assign) id<FrameObservingViewDelegate>delegate;
@end
@interface MainMenuViewcontrollerViewController : UIViewController

@end



