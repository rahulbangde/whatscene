//
//  EditProfileViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 4/2/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "EditProfileViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface EditProfileViewController ()<UIScrollViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{


    NSPredicate *emailPredicate;
    UIImage *chosenImage;
    NSString * strOfUserId;
    NSString  *strOfUserDateOfBirth;

}

@property (strong, nonatomic) IBOutlet UILabel *labelUsername;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewForEditProfile;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForEnterYourName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForMobileNumber;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForEmailAddress;


@property (strong, nonatomic) IBOutlet UIButton *buttonDoneForEditProfile;
@property (strong, nonatomic) IBOutlet UIButton *buttonHide;

@property (strong, nonatomic) IBOutlet UIDatePicker *datepickerForEditProfile;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForDate;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForLastName;
@property (strong, nonatomic) IBOutlet UIButton *buttonOpendatePicker;
@property (strong, nonatomic) IBOutlet UIView *viewPopup;

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Padding In Text View
    UIView *paddingCurrentPassword;
    
    UIView *paddingFirstName;
    UIView *paddingConfirmPassword;
    UIView *paddingEmailAddress;
    
    UIView *paddingMobile;
    UIView *paddingLastName;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingFirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        
        paddingEmailAddress = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingConfirmPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        
        paddingMobile = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingLastName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    }
    else
    {
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        paddingEmailAddress = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingConfirmPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingFirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        paddingMobile = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingLastName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
    }
    
    _textFieldForEnterYourName.leftView = paddingFirstName;
    _textFieldForEnterYourName.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForLastName.leftView = paddingLastName;
    _textFieldForLastName.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForEmailAddress.leftView = paddingConfirmPassword;
    _textFieldForEmailAddress.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForMobileNumber.leftView = paddingMobile;
    _textFieldForMobileNumber.leftViewMode = UITextFieldViewModeAlways;
    
   

    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    _activityIndicator.hidden = YES;
    
    //popView
    _buttonHide.hidden = YES;
    _viewPopup.hidden = YES;
    
    strOfUserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"];
    
    _imageViewForEditProfile.image = [self userImage];
    if (_imageViewForEditProfile.image == nil) {
        _imageViewForEditProfile.image = [UIImage imageNamed:@"f3.png"];
    }


    _labelUsername.text = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"],[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"]];
    _textFieldForEnterYourName.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"];
     _textFieldForLastName.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"];
     _textFieldForEmailAddress.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserEmail"];
     _textFieldForMobileNumber.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserMobileNo"];
    [_buttonOpendatePicker setTitle:[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserDOB"] forState:UIControlStateNormal];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserDOB"]);
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserDOB"] isEqualToString:@""])
    {
       [_buttonOpendatePicker setTitle:@"Date of birth" forState:UIControlStateNormal];
        [_buttonOpendatePicker setTitleColor:[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonOpendatePicker setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    
    
    _buttonDoneForEditProfile.layer.cornerRadius = 5.0f; // this value vary as per your desire
    //_buttonForSubmitInfo.layer.masksToBounds = YES;
    _buttonDoneForEditProfile.layer.borderColor=[[UIColor lightGrayColor]CGColor];

    
    _imageViewForEditProfile.layer.cornerRadius = _imageViewForEditProfile.frame.size.height/2; // this value vary as per your desire
    _imageViewForEditProfile.layer.masksToBounds = YES;
    
    UIColor *color =[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];
    
    
    [_textFieldForEmailAddress setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForEnterYourName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForMobileNumber setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForLastName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    

    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    
    _activityIndicator.hidden = YES;
    
}

- (UIImage*)userImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
   UIImage *image = [UIImage imageWithContentsOfFile:path];
    return image;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    _imageViewForEditProfile.layer.cornerRadius = _imageViewForEditProfile.frame.size.height/2; // this value vary as per your desire
    _imageViewForEditProfile.layer.masksToBounds = YES;
    self.navigationItem.title = @"PROFILE";
    self.navigationController.navigationBar.backgroundColor = [UIColor  colorWithRed:127.0 green:127.0 blue:127.0 alpha:1];
    _activityIndicator.hidden = YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{


    [textField resignFirstResponder];
    return YES;


}
//Validate Email Adddress

-(BOOL)validateEmail:(NSString*)emailid
{
    
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:emailid];
    
}
-(BOOL)validateMobileNumber:(NSString*)mobileNumber
{
    NSString *regex = @"[0-9]{10}";
    emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:mobileNumber];
}


- (IBAction)DoneEditProfiling:(id)sender
{
    
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    
    if ([[_textFieldForEnterYourName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        
        
        NSLog(@"Blank Text");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter firstname" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
        
    }
    else if ([[_textFieldForEmailAddress.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([[_textFieldForLastName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter lastname" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else  if ([self validateEmail:[_textFieldForEmailAddress text]]!=1)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid email" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if ([self validateMobileNumber:[_textFieldForMobileNumber text]]!=1)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid mobile number" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if([[_textFieldForMobileNumber.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter Mobile Number" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
        else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
            
            
        });
        
        dispatch_async(kBgQueue, ^{
            
            [self signUpMethod];
         
            
        });
        
        
    }

}
- (IBAction)CameraAndImageButtonPopUp:(UITapGestureRecognizer *)sender
{
    
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose from library", nil];
    [action showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if (buttonIndex==0) // take photo From camera
    {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
    }
    else if (buttonIndex==1) // take photo From Gallery
    {

        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark UIPickerViewDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageViewForEditProfile.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    NSLog(@"%@",string);
    
    NSString * stroftextKg;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    
    if (textField == _textFieldForMobileNumber)
    {
        if (_textFieldForMobileNumber.text.length >=10 && range.length == 0)
        {
            return NO;
        }
        stroftextKg = [_textFieldForMobileNumber.text stringByReplacingCharactersInRange:range withString:string];
        
        return [string isEqualToString:filtered];
    }
    
    
    
    
    
    
    
    
    return YES;
}



-(void) signUpMethod
{
    
    if (!strOfUserDateOfBirth ) {
        strOfUserDateOfBirth = @"";
    }
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/editprofile.php?first_name=%@&last_name=%@&email=%@&dob=%@&mobileno=%@&userid=%@",_textFieldForEnterYourName.text,_textFieldForLastName.text,_textFieldForEmailAddress.text,strOfUserDateOfBirth,_textFieldForMobileNumber.text,strOfUserId];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    
    NSError *err ;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",json);
           // strEventId = [[[json valueForKey:@"data"]valueForKey:@"userid"] objectAtIndex:0];
            
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                NSLog(@"Signup Successfully");
                
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"dob"]objectAtIndex:0] forKey:@"LoginUserDOB"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"first_name"]objectAtIndex:0] forKey:@"firstname"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"last_name"]objectAtIndex:0] forKey:@"lastname"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"mobileno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self uploadImage];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _activityIndicator.hidden = YES;
                        [_activityIndicator stopAnimating];
                        
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your profile updated successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
            
                        
                        
                    });
                   
                    
                });
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _activityIndicator.hidden = NO;
                [_activityIndicator stopAnimating];
               
                
                
            });
            
            
        }
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
-(void)uploadImage
{
    ///home/listo8mw/public_html/IMAGEDEMO/uploadImage.php
  
    NSData *imgData = UIImagePNGRepresentation(self.imageViewForEditProfile.image);
    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/userimageupload.php?user_id=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"user%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imgData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    NSMutableDictionary *dict=[arr objectAtIndex:0];
    NSLog(@"Data in Dixtionary %@",dict);
    if ([[dict valueForKey:@"Message"] isEqualToString:@"uploaded"]) {
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSLog(@"saving png");
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png",docDir];
        NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(_imageViewForEditProfile.image)];
        [data1 writeToFile:pngFilePath atomically:YES];
    }
    
}



- (IBAction)BackToHome:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"done");
    
}
- (IBAction)buttonDateSelectDone:(id)sender
{
    _viewPopup.hidden = YES;
    _buttonHide.hidden = YES;
    NSDate *myDate = _datepickerForEditProfile.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MMMM-yyyy"];
   strOfUserDateOfBirth = [dateFormat stringFromDate:myDate];
    
   
    
    NSLog(@"%@",strOfUserDateOfBirth);
    
    [_buttonOpendatePicker setTitle:strOfUserDateOfBirth forState:UIControlStateNormal];
    [_buttonOpendatePicker setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}

- (IBAction)buttonHidePicker:(id)sender
{
    _viewPopup.hidden = YES;
    _buttonHide.hidden = YES;
}
- (IBAction)buttonOpendatePicker:(id)sender
{
    _viewPopup.hidden = NO;
    _buttonHide.hidden = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
