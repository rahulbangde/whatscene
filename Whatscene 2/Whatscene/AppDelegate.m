//
//  AppDelegate.m
//  Whatscene
//
//  Created by Alet Viegas on 06/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//
#import "FLAnimatedImage.h"
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Flurry.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface AppDelegate ()

{
 
}
@end

@implementation AppDelegate
@synthesize gameThrive = _gameThrive;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //-----------------------------Flurry Analitics-------------------------------//
    
     [Flurry startSession:@"SNCJ5Q273CXF9TGV6DRX"];
    //[Flurry logError:@"ERROR_NAME" message:@"ERROR_MESSAGE" exception:e];
    //------------------- set iPad and Iphone screen ---------------------------//

    
    
  
    FLAnimatedImage *manWalkingImage;
    
    NSLog(@"begin splash");
    
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
            manWalkingImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"640x1136" ofType:@"gif"]]];
    } else
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    manWalkingImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"320x480" ofType:@"gif"]]];
    }
    
    
    UIViewController *initialViewController = [storyboard instantiateInitialViewController];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController  = initialViewController;
 ////_window.backgroundColor = [UIColor redColor];
    [self.window makeKeyAndVisible];
    //-------------------------------------------------------------------------------//
    

    
    FLAnimatedImageView *ImageViewAnimation = [[FLAnimatedImageView alloc]initWithFrame:CGRectMake(self.window.frame.origin.x, self.window.frame.origin.y, self.window.frame.size.width, self.window.frame.size.height)];
    

    ImageViewAnimation.animatedImage = manWalkingImage;
    
    
    
    UIView * splashView = [[UIView alloc]initWithFrame:CGRectMake(self.window.frame.origin.x, self.window.frame.origin.y, self.window.frame.size.width, self.window.frame.size.height)];
    splashView.backgroundColor = [UIColor blackColor];
    
    [splashView addSubview:ImageViewAnimation];
    [self.window addSubview:splashView];
    [self.window bringSubviewToFront:splashView];

  //  [self.view addSubview:self.ImageViewAnimation];
    
//    NSMutableArray   *arrOfImage = [[NSMutableArray alloc]initWithObjects:[ UIImage imageNamed:@"11.png"],[ UIImage imageNamed:@"22.png"],[ UIImage imageNamed:@"33.png"],[ UIImage imageNamed:@"44.png"],[ UIImage imageNamed:@"55.png"],[ UIImage imageNamed:@"66.png"], nil];
//
//    //add the image to the forefront...
//    UIImageView *splashImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.window.frame.origin.x, self.window.frame.origin.y, self.window.frame.size.width, self.window.frame.size.height)];
//    
//    [splashImage setAnimationImages:arrOfImage];
//    
//    splashImage.animationDuration =0.25;
//    splashImage.animationRepeatCount = 1000;
//    [splashImage startAnimating];
//    //  [splashImage setImage: [UIImage imageNamed:@"Default"]];
//    [self.window addSubview:splashImage];
//    [self.window bringSubviewToFront:splashImage];
//
//    
//    //set an anchor point on the image view so it opens from the left
//    splashImage.layer.anchorPoint = CGPointMake(0, 0.5);
//    
//    //reset the image view frame
//    splashImage.frame = CGRectMake(self.window.frame.origin.x, self.window.frame.origin.y, self.window.frame.size.width, self.window.frame.size.height);
//
    
    
    [UIView animateWithDuration:0
                          delay:5.0
                        options:(UIViewAnimationOptionTransitionCrossDissolve)
                     animations:^{
               
                         [splashView setAlpha:0];
                     } completion:^(BOOL finished){
                         
                         //remove that imageview from the view
                         [splashView removeFromSuperview];
                     }];
    
    
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"notificatio_status"] isEqualToString:@"on"]) {
        UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        
        if (notification) {
           // [self showAlarm:notification.alertBody];
            NSLog(@"AppDelegate didFinishLaunchingWithOptions");
            application.applicationIconBadgeNumber = 0;
            NSLog(@"Rahul-------");
        }
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        if (dictionary != nil)
        {
            // get the necessary information out of the dictionary
            // (the data you sent with your push message)
            // and load your data
            NSLog(@"Rahul-------1");
        }
        
        
        [NSTimer scheduledTimerWithTimeInterval:600.0
                                         target:self
                                       selector:@selector(getNotification)
                                       userInfo:nil
                                        repeats:YES];
        
        appDelegate = [[UIApplication sharedApplication]delegate];
    }
    
    
    
    
    
    
    
    
    
    
   //------------------ Push Notification using GameThrive ----------------------------//
    
    // Override point for customization after application launch.
    self.gameThrive = [[GameThrive alloc] initWithLaunchOptions:launchOptions handleNotification:^(NSString* message, NSDictionary* additionalData, BOOL isActive) {
        UIAlertView* alertView;
        NSLog(@"APP LOG ADDITIONALDATA: %@", additionalData);
        if (additionalData) {
            // Append AdditionalData at the end of the message
            NSString* displayMessage = [NSString stringWithFormat:@"NotificationMessage:%@", message];
            NSString* messageTitle;
            if (additionalData[@"discount"])
                messageTitle = additionalData[@"discount"];
            else if (additionalData[@"bonusCredits"])
                messageTitle = additionalData[@"bonusCredits"];
            else if (additionalData[@"actionSelected"])
                messageTitle = [NSString stringWithFormat:@"Pressed ButtonId:%@", additionalData[@"actionSelected"]];
            alertView = [[UIAlertView alloc] initWithTitle:messageTitle
                                                   message:displayMessage
                                                  delegate:self
                                         cancelButtonTitle:@"Close"
                                         otherButtonTitles:nil, nil];
        }
        // If a push notification is received when the app is being used it does not go to the notifiction center so display in your app.
        if (alertView == nil && isActive) {
            alertView = [[UIAlertView alloc] initWithTitle:@"GameThrive Message"
                                                   message:message
                                                  delegate:self
                                         cancelButtonTitle:@"Close"
                                         otherButtonTitles:nil, nil];
        }
        // Highly recommend adding game logic around this so the user is not interrupted during gameplay.
        if (alertView != nil)
            [alertView show];
    }];
    
    
    
    
    [FBLoginView class];
    return YES;
}

- (void)showAlarm:(NSString *)text {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:text delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

-(void)thisIsCalledEvery1Second
{
          NSLog(@"count---- %d",[[NSUserDefaults standardUserDefaults]integerForKey:@"notificationCount"]);
    NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/notification.php?userid=%@&count=%d",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],[[NSUserDefaults standardUserDefaults]integerForKey:@"notificationCount"]];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *error = nil;
    if (data !=nil )
    {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        NSLog(@"%@",json);
        if (error) {
            NSLog(@"err---- %@",error.description);
        }
        else{
          //   NSLog(@"count---- %d",[[NSUserDefaults standardUserDefaults]integerForKey:@"CommentCount"]);
            
            
            if ([[json valueForKey:@"success"] isEqualToString:@"true"]) {
                
                NSInteger str  = [[json valueForKey:@"count"] integerValue];
                
                NSLog(@"%d------",[[NSUserDefaults standardUserDefaults]integerForKey:@"notificationCount"]);
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"notificationCount"] < str) {
                    
                    
                    for (int i = 0; i<1; i++)
                    {
                        
                        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
                        localNotification.alertBody = [[[json valueForKey:@"data"] valueForKey:@"notification"] objectAtIndex:[[[json valueForKey:@"data"] valueForKey:@"notification"] count]-1];
                        // localNotification.alertTitle = [[[json valueForKey:@"data"] valueForKey:@"event_name"] objectAtIndex:i];
                        //                    localNotification.userInfo = json;
                        localNotification.timeZone = [NSTimeZone defaultTimeZone];
                        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                        localNotification.soundName = UILocalNotificationDefaultSoundName;
                        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                    }
                    
                    
                    [[NSUserDefaults standardUserDefaults]setInteger:[[json valueForKey:@"count"] integerValue] forKey:@"notificationCount"];
                    
                }
            }

            }
            
                 }
  
    
    
}




-(void) getNotification
{
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]) {
        dispatch_async(kBgQueue, ^{
            
            [self thisIsCalledEvery1Second];
            
            
        });
    }
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication
                        withSession:self.session];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Logs 'install' and 'app activate' App Events.
    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
        NSLog(@"applicationDidBecomeActive");
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    NSLog(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"applicationDidEnterBackground");
}

//- (void)applicationWillEnterForeground:(UIApplication *)application {
//    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//    NSLog(@"applicationWillEnterForeground");
//}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
    if ([identifier isEqualToString:@"Rahul"]) {
        
        NSLog(@"You chose action 1.");
    }
    NSLog(@"hello");
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.BlackbeanEngagement.GetPlaceDemo" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GetPlaceDemo" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GetPlaceDemo.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}
- (void)applicationWillTerminate:(UIApplication *)application
{    [self.session close];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

//    NSArray *subviews = [_window subviews];
//    for (int i = 0; i < [subviews count]; i++) {
//        [[subviews objectAtIndex:i] removeFromSuperview];
//    }
//    //[self.window addSubview:tabBarController.view];
//    [self.window makeKeyAndVisible];
//   UIViewController *viewController = [[UIViewController alloc] initWithNibName:@"EventViewViewController" bundle:nil];
//    self.window.rootViewController = viewController;
}





#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
     NSLog(@"AppDelegate didReceiveLocalNotification %@", notification.userInfo);
    
    [self showAlarm:notification.alertBody];
    application.applicationIconBadgeNumber = 0;
   // notification.
   
}
@end
