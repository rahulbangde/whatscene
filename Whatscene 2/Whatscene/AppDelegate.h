//
//  AppDelegate.h
//  Whatscene
//
//  Created by Alet Viegas on 06/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameThrive/GameThrive.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreData/CoreData.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{


  AppDelegate *appDelegate;

}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GameThrive *gameThrive;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) FBSession *session;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end


