//
//  SignUpForWhatscene.m
//  Whatscene
//
//  Created by Alet Viegas on 10/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "SignUpForWhatscene.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "OAuthConsumer.h"
#define ALPHA                   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC                 @"1234567890"
#define ALPHA_NUMERIC           ALPHA NUMERIC
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface SignUpForWhatscene ()<UIScrollViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,FBLoginViewDelegate>
{
    
    
    UIImagePickerController *imagePicker;
    BOOL strOfBoolValue;
    NSString *facebookID;
    NSString * imageString;
    AppDelegate * appdelegate;
    NSPredicate *emailPredicate;
    NSString * strEventUserId;
    UIImage *chosenImage;
    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    UIImage * image;
    NSString * Accesstoken;
    NSString * strOfName;
    NSString* strOfEmail;
    NSString*strOfGender;
    NSString*strOfLocation;
  
    NSString *strOfFbUserId;
    
}

@property (nonatomic,strong) OAToken* accessToken;


@property (strong, nonatomic) IBOutlet UIButton *buttonForFacebook;
- (IBAction)LogintoFacebook:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewForMyLatestScenes;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForFirstName;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForLastName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForEmailAddress;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForPassword;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForMobileNumber;

@property (strong, nonatomic) IBOutlet UIButton *buttonForSubmitInfo;


@property (strong, nonatomic) IBOutlet UIImageView *imageViewForUserSelFie;


@property (strong, nonatomic) IBOutlet UITextField *textFieldRepeatPasswordForSignUp;


@property (strong, nonatomic) IBOutlet UIButton *buttonForSignIn;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)SignUpForWhatscene:(id)sender;
- (IBAction)BacktoHome:(id)sender;

- (IBAction)SignInForRegisterUser:(id)sender;



@end
NSString *strOfFbID;
  NSDictionary *dictOfFBData;
@implementation SignUpForWhatscene

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // PADDING IN TEXT VIEW
   
    
    
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
    _activityIndicator.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];

//    self.navigationController.navigationBar.clipsToBounds = YES;
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,navigationBar.frame.size.height-1,navigationBar.frame.size.width, 0.5)];
    
    // Change the frame size to suit yours //
    
    [navBorder setBackgroundColor:[UIColor colorWithWhite:200.0f/255.f alpha:0.8f]];
    [navBorder setOpaque:YES];
    [navigationBar addSubview:navBorder];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self setPaddingInTextFields];
    [self setPlaceholdarColor];
    [self getInfo];
    
    
}


-(void) setPaddingInTextFields
{
    
    UIView *paddingCurrentPassword;
    
    UIView *paddingFirstName;
    UIView *paddingConfirmPassword;
    UIView *paddingEmailAddress;
    
    UIView *paddingMobile;
    UIView *paddingLastName;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingFirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        
        paddingEmailAddress = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingConfirmPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        
        paddingMobile = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingLastName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    }
    else
    {
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        paddingEmailAddress = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingConfirmPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingFirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        paddingMobile = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingLastName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
    }
    
    _textFieldForFirstName.leftView = paddingFirstName;
    _textFieldForFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForLastName.leftView = paddingLastName;
    _textFieldForLastName.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForEmailAddress.leftView = paddingEmailAddress;
    _textFieldForEmailAddress.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForMobileNumber.leftView = paddingMobile;
    _textFieldForMobileNumber.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForPassword.leftView = paddingCurrentPassword;
    _textFieldForPassword.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldRepeatPasswordForSignUp.leftView = paddingConfirmPassword;
    _textFieldRepeatPasswordForSignUp.leftViewMode = UITextFieldViewModeAlways;
    

}

-(void) setPlaceholdarColor
{
    UIColor *color =[UIColor  colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.15];
    
    
    
    [_textFieldForFirstName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForLastName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForEmailAddress setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForMobileNumber setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldRepeatPasswordForSignUp setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    

}
-(void) getInfo
{
    _imageViewForUserSelFie.layer.cornerRadius = self.imageViewForUserSelFie.frame.size.height/2;
    _imageViewForUserSelFie.layer.masksToBounds = YES;
    //    _facebooklogin.delegate = self;
    //    _facebooklogin.readPermissions = @[@"public_profile", @"email"];
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // create a fresh session object
        appDelegate.session =[[FBSession alloc] initWithPermissions:@[@"basic_info", @"email",@"user_birthday"]];
        
        // if we don't have a cached token, a call to open here would cause UX for login to
        // occur; we don't want that to happen unless the user clicks the login button, and so
        // we check here to make sure we have a token before calling open
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                // we recurse here, in order to update buttons and labels
                // [self updateView];
            }];
        }
    }
    
    
    //    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(CameraAndImageButtonPopUp:)];
    
    
    //  _imageViewForUserSelFie.image = [UIImage imageWithData:appdelegate.dataOfSql];
    
    imageString = @"dipak.jpg";
    ///////Button Corner modification//////////////////////////////////////////////////////////////////
    _buttonForSubmitInfo.layer.cornerRadius = 5.0f; // this value vary as per your desire
    //_buttonForSubmitInfo.layer.masksToBounds = YES;
    //    _buttonForSubmitInfo.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    //    _buttonForSubmitInfo.layer.borderWidth= 1.0f;
    
    _buttonForSignIn.layer.cornerRadius = 5.0f; // this value vary as per your desire
    //_buttonForSubmitInfo.layer.masksToBounds = YES;
    //    _buttonForSignIn.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    //    _buttonForSignIn.layer.borderWidth= 1.0f;
    
    
    // Do any additional setup after loading the view.
    //Camera Gallery Pop up button show and hide
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    
    NSLog(@"vfggre");
    
    
    
    [_scrollViewForMyLatestScenes setScrollEnabled:YES];
    [_scrollViewForMyLatestScenes setContentSize:CGSizeMake(320,900)];
    _scrollViewForMyLatestScenes.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_scrollViewForMyLatestScenes];
    
    
    
    
    //Camera Delegate set
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    return YES;
}


//Login with facebook data
//-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
//{
//    NSLog(@"%@", user);
//    NSLog(@"%@", user.name);
//    
//    [self _loadData];
//}


//Validate Email Adddress

-(BOOL)validateEmail:(NSString*)emailid
{
    
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:emailid];
    
}
-(BOOL)validateMobileNumber:(NSString*)mobileNumber
{
    NSString *regex = @"[0-9]{10}";
    emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:mobileNumber];
    
    
    
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    NSLog(@"%@",string);
    
    NSString * stroftextKg;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    
    if (textField == _textFieldForMobileNumber)
    {
        if (_textFieldForMobileNumber.text.length >=10 && range.length == 0)
        {
            return NO;
        }
        stroftextKg = [_textFieldForMobileNumber.text stringByReplacingCharactersInRange:range withString:string];
        
        return [string isEqualToString:filtered];
    }
    
    
    
    
    
    
    
    
    return YES;
}

- (IBAction)SignUpForWhatscene:(id)sender
{
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    
   
    if ([[_textFieldForFirstName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        
        
        NSLog(@"Blank Text");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
        
    }
    else if ([[_textFieldForLastName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""])
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
   
    
    else  if ([self validateEmail:[_textFieldForEmailAddress text]]!=1)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid email" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if ([self validateMobileNumber:[_textFieldForMobileNumber text]]!=1)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid mobile number" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if([_textFieldForPassword.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }

    
    else if(_textFieldForPassword.text.length <6)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter atleast six character" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if([_textFieldRepeatPasswordForSignUp.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter repeat password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    else if(![_textFieldForPassword.text isEqualToString:_textFieldRepeatPasswordForSignUp.text])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"alert" message:@"password and repeat password field does not match" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if(netStatus == NotReachable)
    {
        
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityIndicator.hidden = NO;
         [_activityIndicator startAnimating];
            
            
        });

        dispatch_async(kBgQueue, ^{
            
            [self signUpMethod];
       
         
        });

        
    }
  
}

-(void) signUpMethod
{
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/registration_sub.php?first_name=%@&last_name=%@&email=%@&password=%@&mobileno=%@&fbid=%@",_textFieldForFirstName.text,_textFieldForLastName.text,_textFieldForEmailAddress.text,_textFieldForPassword.text,_textFieldForMobileNumber.text,@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    
    NSError *err ;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",json);
            strEventUserId = [[[json valueForKey:@"data"]valueForKey:@"userid"] objectAtIndex:0];
         
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self uploadImage];
                    _activityIndicator.hidden = YES;
                    [_activityIndicator stopAnimating];
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
                    [self.navigationController pushViewController:vc animated:NO];
                    
                });
                NSLog(@"Signup Successfully");
              
                
            }
            else if([[json valueForKey:@"error"] isEqualToString:@"provided email id/mobile number already exists."])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                 
                    _activityIndicator.hidden = YES;
                    [_activityIndicator stopAnimating];
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This email / mobile number already in use" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
            
                    
                });
            }
            
            
        }
    }
}


- (IBAction)BacktoHome:(id)sender
{

    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"StoryBoardId"]);
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] valueForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:vc animated:NO];

    
    
    
}

- (IBAction)SignInForRegisterUser:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:vc animated:NO];
    
    
    
}




- (IBAction)CameraAndImageButtonPopUp:(UITapGestureRecognizer *)sender {
    
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose from library", nil];
    [action showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    
    picker.delegate=self ;
    if (buttonIndex==0) // take photo From camera
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    else if (buttonIndex==1) // take photo From Gallery
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
#pragma mark UIPickerViewDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageViewForUserSelFie.image = chosenImage;
    CGFloat width = chosenImage.size.width;
    CGFloat height = chosenImage.size.height;
    
    NSLog(@"%f%f",width,height);
    [picker dismissViewControllerAnimated:YES completion:NULL];
}






- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
-(void)uploadImage
{
    ///home/listo8mw/public_html/IMAGEDEMO/uploadImage.php

    NSData *imgData = UIImagePNGRepresentation(self.imageViewForUserSelFie.image);
    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/userimageupload.php?user_id=%@",strEventUserId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"user%@",strEventUserId]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imgData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    NSMutableDictionary *dict=[arr objectAtIndex:0];
    NSLog(@"Data in Dixtionary %@",dict);
}

- (IBAction)LogintoFacebook:(id)sender
{
    NSLog(@"fb");
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen) {
        // if a user logs out explicitly, we delete any cached token information, and next
        // time they run tahe applicaiton they will be presented with log in UX again; most
        // users will simply close the app or switch away, without logging out; this will
        // cause the implicit cached-token login to occur on next launch of the application
        [appDelegate.session closeAndClearTokenInformation];
        
    } else {
        if (appDelegate.session.state != FBSessionStateCreated) {
            // Create a new, logged out session.
            appDelegate.session = [[FBSession alloc] init];
        }
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState status,
                                                         NSError *error) {
            // and here we make sure to update our UX according to the new session state
            [self updateView];
        }];
        
        
    }




}
- (void)updateView
{
    // get the app delegate, so that we can reference the session property
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (appDelegate.session.isOpen)
    {
        // valid account UI is shown whenever the session is open
        //        [self.buttonLoginLogout setTitle:@"Log out" forState:UIControlStateNormal];
        //        [self.textNoteOrLink setText:[NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
        //appDelegate.session.accessTokenData.accessToken]];
        NSString *strData=[NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",
                           appDelegate.session.accessTokenData.accessToken];
        NSLog(@"%@",appDelegate.session.accessTokenData.accessToken);
        NSURL *strURl=[NSURL URLWithString:strData];
        NSError *err;
        NSData *data=[NSData dataWithContentsOfURL:strURl];
        dictOfFBData=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        NSLog(@"dictOfData>>>>>>>>>%@",dictOfFBData);
      //  NSString *strOfImage=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"id"]];
        
        
        
        
               dispatch_async(kBgQueue, ^{
            
            [self checkFbRegister];
           
            
        });

        
    }
    else
    {
        // login-needed account UI is shown whenever the session is closed
        //        [self.buttonLoginLogout setTitle:@"Log in" forState:UIControlStateNormal];
        //        [self.textNoteOrLink setText:@"Login to create a link to fetch account data"];
    }
}

-(void) checkFbRegister
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebookloginstatus.php?fbid=%@",[dictOfFBData valueForKey:@"id"]];
      strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
            
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"LoginStatus"];
                     [[NSUserDefaults standardUserDefaults] setObject:@"LoginFB" forKey:@"FBLoginStatus"];
                      [[NSUserDefaults standardUserDefaults] setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"notificatio_status"] objectAtIndex:0] forKey:@"notificatio_status"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0] forKey:@"image"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"firstname"]objectAtIndex:0] forKey:@"firstname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"lastname"]objectAtIndex:0] forKey:@"lastname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0] forKey:@"LoginUserId"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                    NSArray *key = [[[jsondata valueForKey:@"data"] objectAtIndex:0] allKeys];
                    if ([key containsObject:@"mobno"]) {
                         [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"mobno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                    }
                    if ([key containsObject:@"dob"]) {
                         [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"dob"]objectAtIndex:0] forKey:@"LoginUserDOB"];
                    }
                   
                   
                    
                    NSLog(@"Downloading...");
                    
                    image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0]]]];
                    [self uploadPhotoInDoc];
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
                    [self.navigationController pushViewController:vc animated:NO];
                    
                });
               
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VarifiedMobileNoView"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                });
            }
        }
    }
}

-(void) uploadPhotoInDoc
{
    
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png",docDir];
        NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
        [data1 writeToFile:pngFilePath atomically:YES];
//        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myscene"];
//        [self.navigationController pushViewController:vc animated:YES];
  
}



-(void) signUpWithfacebook
{
    
    //------------------------Delete cookies & keyboard notification method---------------------------//
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *each in cookieStorage.cookies) {
        [cookieStorage deleteCookie:each];
    }
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebooklogin.php?id=%@&firstname=%@&lastname=%@&email=%@",[dictOfFBData valueForKey:@"id"],[dictOfFBData valueForKey:@"first_name"],[dictOfFBData valueForKey:@"last_name"],[dictOfFBData valueForKey:@"email"]];
  //  strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];

    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
           
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                strOfFbID =[[[jsondata valueForKey:@"data"]valueForKey:@"fbid"]objectAtIndex:0];
                     strOfFbUserId =[[[jsondata valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0];
                NSLog(@"Signup Successfully");
               
              
            }
            
            
        }
    }

}


-(void) uploadFBImage
{
    ///home/listo8mw/public_html/IMAGEDEMO/uploadImage.php
    NSString *strOfImage=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"id"]];
    
    _imageViewForUserSelFie.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strOfImage]]];
    
    
    NSData *imgData = UIImagePNGRepresentation(self.imageViewForUserSelFie.image);
    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/userimageupload.php?user_id=%@",strOfFbUserId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"user%@",strOfFbUserId]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imgData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    NSMutableDictionary *dict=[arr objectAtIndex:0];
    NSLog(@"Data in Dixtionary %@",dict);
}





@end
