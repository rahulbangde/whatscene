//
//  EventViewViewController.m
//  Whatscene
//
//  Created by Black Bean Engagement on 02/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//
#import "AppDelegate.h"
#import "EventViewViewController.h"
#import "AsyncImageView.h"
#define FONT_SIZE 14.0f
#define kCharacterMaximumLimit 140
#define kCharacterWarningLimit 10
#define kOFFSET_FOR_KEYBOARD 216.0
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define CELL_CONTENT_MARGIN 60.0f
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
@interface EventViewViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
    UIView *_subview;
    NSMutableArray *items;
    UITableView *dataTableView;
    CGFloat heightTableView;
    CGRect keyboardFrame;
    NSString *strOfEvent;
    UICollectionView *collectionViewInviteFriends;
    UITextView *chatTextField;
    UIButton *buttonSendcomment;
    NSInteger CELL_CONTENT_WIDTH;
    NSString *strOftablerefreshFlag,*strOfScrollFlag;
    CGFloat keyboardHight;
    UIView *backView;
    UIScrollView *_scrollView;
    NSMutableArray *arrOfCommentUserId,*arrOfCommentedUserName,*arrOfCommentDateAndTime;
    NSMutableDictionary *dictOfInviteeUser;
 
    NSString *strOfFbUserId;
    UIImage *image;
       UIButton    *buttonjoinThisScene;
    UIButton *buttonCancelInvitation;
    UIButton *buttonAcceptInvitation;;
    
}

//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHight;
@property (strong, nonatomic) IBOutlet UIView *viewForShareToSocial;
@property (strong, nonatomic) IBOutlet UIButton *buttonHide;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end


// For Get value from anther View
NSString *strOfFbID;
extern NSString *strOfEventImage,*strOfEventDateAndTime,*strOfEventDescription,*strOfEventName,*strOfEventPlace,*strOfEventType,*strOfEventId,*strOfEventOwnerId,*strOfCommentFlag,*strOfEventInvitedStatus,*strOfSceneName,*strOfInviteeUserName;
extern UIColor *colorForViewScene;
extern CGFloat eventLatitude,eventlongitude;
extern    NSDictionary *dictOfFBData;
@implementation EventViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
    _activityIndicator.hidden = YES;
    //////// Facebook method Code //////
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // create a fresh session object
        appDelegate.session =[[FBSession alloc] initWithPermissions:@[@"basic_info", @"email",@"user_birthday"]];
        
        // if we don't have a cached token, a call to open here would cause UX for login to
        // occur; we don't want that to happen unless the user clicks the login button, and so
        // we check here to make sure we have a token before calling open
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                // we recurse here, in order to update buttons and labels
                // [self updateView];
            }];
        }
    }
    
    ////////////////////////////////////////
    
    
    
    _viewForShareToSocial.hidden = YES;
       _buttonHide.hidden = YES;
    [_buttonHide setBackgroundColor:colorForViewScene];
    _buttonHide.alpha = 0.5;
    dictOfInviteeUser = [NSMutableDictionary new];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginStatus"] isEqualToString:@"Login"])
    {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-104);
    }
    else{
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-64);
    }
    
    
    [self.view addSubview:_scrollView];
    [self.view addSubview:_buttonHide];
    [self.view addSubview:_viewForShareToSocial];
    [_buttonHide addTarget:self action:@selector(hideButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.view.backgroundColor = colorForViewScene;
    [self.navigationController.navigationBar setBarTintColor:colorForViewScene];
    CELL_CONTENT_WIDTH = self.view.frame.size.width-70;
    heightTableView = 40;
    strOfEvent = @"Public1";
    strOfScrollFlag = @"NO";
    // dataTableView.backgroundColor = [UIColor greenColor];
    items = [[NSMutableArray alloc]init];;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    
    strOftablerefreshFlag = @"YES";
    
    //[self.view addSubview:dataTableView];
    
    // [self prepareScrollView];
}



-(void) navigationBar
{
             UIButton * loginButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20
                                                                                )];
        [loginButton setImage:[UIImage imageNamed:@"Share.png"] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *loginButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:loginButtonItem, nil];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: YES];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _activityIndicator.hidden = NO;
        [_activityIndicator startAnimating];
     
    });
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginStatus"] isEqualToString:@"Login"])
    {
       
        dispatch_async(kBgQueue, ^{
            
            [self getComment];
            [self getInviteeFriendlist];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self chatTextFieldMethod];
                [self prepareScrollView];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    _activityIndicator.hidden = YES;
                    [_activityIndicator stopAnimating];
                    
                });
                if ([strOfCommentFlag isEqualToString:@"YES"]) {
                    CGPoint bottomOffset = CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height);
                    [_scrollView setContentOffset:bottomOffset animated:YES];
                }
                [collectionViewInviteFriends reloadData];
                dataTableView.scrollEnabled = NO;
            });
            
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [self prepareScrollView];
            dataTableView.scrollEnabled = NO;
            _activityIndicator.hidden = YES;
            [_activityIndicator stopAnimating];
        });
    }
    
    
    self.navigationItem.title = [NSString stringWithFormat:@"SCENE:%@",strOfEventName];
    self.navigationController.navigationBar.hidden = NO;
    
    
    
    
    
}

-(void) chatTextFieldMethod
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-40, self.view.frame.size.width, 40)];
  //  backView.backgroundColor = [UIColor colorWithRed:244.0/255.0 green:131.0/255.0 blue:17.0/255.0 alpha:1.0];
    backView.backgroundColor = colorForViewScene;
    UILabel *labelLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
    labelLine.backgroundColor = [UIColor whiteColor];
    labelLine.text = @"";
    [backView addSubview:labelLine];
    
    backView.alpha = 0.7;
    [self.view addSubview:backView];
    buttonSendcomment = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-70, self.view.frame.size.height-35, 70, 30)];
    [buttonSendcomment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buttonSendcomment setTitle:@"SEND" forState:UIControlStateNormal];
    [buttonSendcomment addTarget:self action:@selector(buttonSend:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:buttonSendcomment];
    chatTextField = [[UITextView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, self.view.frame.size.height-35, self.view.frame.size.width-70, 30)];
    chatTextField.layer.borderColor = [UIColor whiteColor].CGColor;
    chatTextField.layer.cornerRadius = 3.0;
    chatTextField.textColor = [UIColor blackColor];
    chatTextField.delegate = self;
    chatTextField.layer.backgroundColor = [UIColor whiteColor].CGColor;
    //[chatTextField setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:chatTextField];
}

-(void) getComment
{
    
    arrOfCommentUserId = [NSMutableArray new];
    arrOfCommentedUserName = [NSMutableArray new];
    arrOfCommentDateAndTime = [NSMutableArray new];
    NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/get_comments.php?eventid=%@",strOfEventId];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *error = nil;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            NSLog(@"err---- %@",error.description);
        }
        else{
            NSLog(@"%@",json);
            
            if ([[[json valueForKey:@"data"] valueForKey:@"comment"] count]==0) {
                [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"CommentCount"];
            }
            for (int i = 0; i<[[[json valueForKey:@"data"] valueForKey:@"comment"] count]; i++) {
                [[NSUserDefaults standardUserDefaults]setInteger:[[json valueForKey:@"count"] integerValue] forKey:@"CommentCount"];
                [items addObject:[[[json valueForKey:@"data"] valueForKey:@"comment"] objectAtIndex:i]];
                [arrOfCommentUserId addObject:[[[json valueForKey:@"data"] valueForKey:@"user_id"] objectAtIndex:i]];
                [arrOfCommentedUserName addObject:[[[json valueForKey:@"data"] valueForKey:@"username"] objectAtIndex:i]];
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd/MM/yyyy"]; //// here set format of date which is in your output date (means above str with format
                NSDate *date = [dateFormat dateFromString: [[[json valueForKey:@"data"] valueForKey:@"date"] objectAtIndex:i]]; // here you can fetch date from string with define format
                
                dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd MMM yyyy"];// here set format which you want...
                NSString *dateString = [dateFormat stringFromDate:date];// here convert date in NSString
                
                NSLog(@"%@",dateString);
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                dateFormatter1.dateFormat = @"HH:mm";
                NSDate *date1 = [dateFormatter1 dateFromString:[[[json valueForKey:@"data"] valueForKey:@"time"] objectAtIndex:i]];
                   NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                dateFormatter2.dateFormat = @"hh:mm a";
                NSString *strOfCommentTime = [dateFormatter2 stringFromDate:date1];
                
                
                [arrOfCommentDateAndTime  addObject:[NSString stringWithFormat:@"%@ | %@",dateString,strOfCommentTime]];
            }
            NSLog(@"%@",items);
            
        }
    }
    
    
}



- (void)textViewDidChange:(UITextView *)textView
{
    if (textView == chatTextField) {
        CGFloat fixedWidth = textView.frame.size.width;
        CGFloat yPosition ;
        
        CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = textView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        
        yPosition = self.view.frame.size.height-(newFrame.size.height+keyboardHight+5);
        newFrame.origin = CGPointMake(textView.frame.origin.x, yPosition);
        
        
        textView.frame = newFrame;
        
        backView.frame = CGRectMake(self.view.frame.origin.x, yPosition-5, self.view.frame.size.width, newFrame.size.height+10);
        CGRect scrollViewFrame = CGRectMake(0, 0, _scrollView.frame.size.width, yPosition);
        _scrollView.frame = scrollViewFrame;
//        CGPoint bottomOffset = CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height);
//        [_scrollView setContentOffset:bottomOffset animated:YES];
        
        
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    if([text length] == 0)
    {
        if([textView.text length] != 0)
            return YES;
        else
            return NO;
    }
    
    if([[textView text] length] > kCharacterMaximumLimit-1)
    {
        return NO;
    }
    
    return YES;
    
}

-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(keyboardFrame));
}

-(void) prepareScrollView
{
    [_subview removeFromSuperview];
    NSLog(@"%f",heightTableView);
    _subview = [[UIView alloc] init];
  
    
    AsyncImageView * imageView;
    
    // Add Event image on scrollView
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
     imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, 250)];
    } else
    {
         imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, 150)];
    }
    
    
    
    imageView.imageURL = [NSURL URLWithString:strOfEventImage];
    if (imageView.image == nil) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
              imageView.image = [UIImage imageNamed:@"iPadDummyScene.png"];
        }
        else
        {
              imageView.image = [UIImage imageNamed:@"iphonedummyScene.png"];
        }
        }
    [_subview addSubview:imageView];
    
  
    UIButton *buttonEditEvent;
    
    
    // Edit Event Button ...................
    
    if ([strOfEventOwnerId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]]) {
        if ([strOfSceneName isEqualToString:@"MyScene"]) {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                buttonEditEvent = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 50, 50)];
            }
            else{
                buttonEditEvent = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-30, 0, 30, 30)];
            }
            
            [buttonEditEvent addTarget:self action:@selector(buttonEditEvent) forControlEvents:UIControlEventTouchUpInside];
            [buttonEditEvent setBackgroundImage:[UIImage imageNamed:@"editScene.PNG"] forState:UIControlStateNormal];
            // [buttonEditEvent setTitle:@"Edit" forState:UIControlStateNormal];
            [buttonEditEvent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.view addSubview:buttonEditEvent];
        }
       
    }
    
    UILabel *labelImageShadow;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        labelImageShadow = [[UILabel alloc]initWithFrame:CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y+imageView.frame.size.height-70, imageView.frame.size.width, 70)];
         labelImageShadow.font = [UIFont boldSystemFontOfSize:22.0];
    }
    else
    {
        labelImageShadow = [[UILabel alloc]initWithFrame:CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y+imageView.frame.size.height-50, imageView.frame.size.width, 50)];
         labelImageShadow.font = [UIFont boldSystemFontOfSize:17.0];
    }
    
    
 
    labelImageShadow.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.7];
    labelImageShadow.text = strOfEventName;
    labelImageShadow.textColor = [UIColor whiteColor];
   
    labelImageShadow.textAlignment = NSTextAlignmentCenter;
    [_subview addSubview:labelImageShadow];
    
    if ([strOfEventInvitedStatus isEqualToString:@"Pending"]) {
        
        UILabel *labelInviteFriendname;
       
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {  labelInviteFriendname = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x, 0, imageView.frame.size.width, 30)];
             labelInviteFriendname.font = [UIFont systemFontOfSize:17];
            
            buttonAcceptInvitation = [[UIButton alloc] initWithFrame:CGRectMake(imageView.frame.origin.x+10, labelImageShadow.frame.origin.y+10, 50, 50)];
              buttonCancelInvitation = [[UIButton alloc] initWithFrame:CGRectMake(imageView.frame.size.width-50, labelImageShadow.frame.origin.y+10, 50, 50)];
        }
        else{
            labelInviteFriendname = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x, 0, imageView.frame.size.width, 20)];
             labelInviteFriendname.font = [UIFont systemFontOfSize:10];
             buttonAcceptInvitation = [[UIButton alloc] initWithFrame:CGRectMake(imageView.frame.origin.x+10, labelImageShadow.frame.origin.y+5, 40, 40)];
              buttonCancelInvitation = [[UIButton alloc] initWithFrame:CGRectMake(imageView.frame.size.width-50, labelImageShadow.frame.origin.y+5, 40, 40)];
        }
        
        labelInviteFriendname.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.7];
      
        labelInviteFriendname.text= [[NSString stringWithFormat:@"%@ has invited you",strOfInviteeUserName] capitalizedString];
        labelInviteFriendname.textAlignment = NSTextAlignmentCenter;
        labelInviteFriendname.textColor = [UIColor whiteColor];
       
        
        [_subview addSubview:labelInviteFriendname];
        
        
        
        
        buttonAcceptInvitation.showsTouchWhenHighlighted=YES;
        [buttonAcceptInvitation addTarget:self action:@selector(buttonInvitationAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonAcceptInvitation setBackgroundImage:[UIImage imageNamed:@"Greentick.png"] forState:UIControlStateNormal];
        buttonAcceptInvitation.tag = 1;
        [_subview addSubview:buttonAcceptInvitation];
        
       buttonCancelInvitation.showsTouchWhenHighlighted=YES;
        [buttonCancelInvitation addTarget:self action:@selector(buttonInvitationAction:) forControlEvents:UIControlEventTouchUpInside];
        [buttonCancelInvitation setBackgroundImage:[UIImage imageNamed:@"redCross.png"] forState:UIControlStateNormal];
        buttonCancelInvitation.tag = 2;
        [_subview addSubview:buttonCancelInvitation];

    }
    
    
    
      // For Set Date And time
     NSArray *arrOfDateAndTime = [strOfEventDateAndTime componentsSeparatedByString:@"|"];
     
     NSLog(@"%@",arrOfDateAndTime);
     
     
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
     [dateFormatter setDateFormat:@"yyyy-dd-MM"]; //// here set format of date which is in your output date (means above str with format)
     
     NSDate *date = [dateFormatter dateFromString: [arrOfDateAndTime objectAtIndex:1]]; // here you can fetch date from string with define format
     
     dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"dd MMM"];// here set format which you want...
     NSString *dateString = [dateFormatter stringFromDate:date];// here convert date in NSString
     
     NSMutableString *strOfEventdate = [NSMutableString stringWithString:dateString];
     [strOfEventdate insertString:@"th" atIndex:2];
     // [strOfEventdate insertString:@"th" atIndex:1];
     
     
     NSLog(@"Converted String : %@",strOfEventdate);
     
     
     NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
     dateFormatter1.dateFormat = @"HH:mm:ss";
     NSDate *date1 = [dateFormatter1 dateFromString:[arrOfDateAndTime objectAtIndex:0]];
     dateFormatter1.dateFormat = @"hh:mm a";
     NSString *strOfEventTime = [dateFormatter1 stringFromDate:date1];
     
     NSLog(@"Converted String : %@",strOfEventTime);
     
     
     NSString *strOfDateAndTime = [NSString stringWithFormat:@"%@ | %@",strOfEventdate,strOfEventTime];
    
    
    
    
    UILabel *labelDateAndTime;
    UIImageView *imageViewClock;
    UIButton *buttonPlace;
    UILabel *labelLineOne;
    UIImageView *imageViewPlace;
    UITextView *labelEventPlace;
    UITextView *textViewEventDescription;
  
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
         labelDateAndTime = [[UILabel alloc]initWithFrame:CGRectMake(labelImageShadow.frame.origin.x+40, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height+7, (self.view.frame.size.width/2)-30, 30)];
         imageViewClock = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageShadow.frame.origin.x+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height+9, 25, 25)];
       
        
        imageViewPlace = [[UIImageView alloc]initWithFrame:CGRectMake(labelDateAndTime.frame.origin.x+labelDateAndTime.frame.size.width+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height+9, 25, 25)];
        
        
        labelEventPlace = [[UITextView alloc]initWithFrame:CGRectMake(imageViewPlace.frame.origin.x+imageViewPlace.frame.size.width+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height+7, (self.view.frame.size.width/2)-40, 30)];
       
        
        labelEventPlace.text = strOfEventPlace;
        
        labelEventPlace.backgroundColor = [UIColor clearColor];
        labelEventPlace.textColor  = [UIColor whiteColor];
         labelEventPlace.font = [UIFont systemFontOfSize:17.0];
        labelEventPlace.scrollEnabled = NO;
        labelEventPlace.editable = NO;
        
        CGRect frame = labelEventPlace.frame;
        frame.size.height = [labelEventPlace sizeThatFits:CGSizeMake(labelEventPlace.frame.size.width, MAXFLOAT)].height;
        labelEventPlace.frame = frame;
        
         buttonPlace = [[UIButton alloc]initWithFrame:CGRectMake(labelDateAndTime.frame.origin.x+labelDateAndTime.frame.size.width+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height, labelEventPlace.frame.size.width, labelEventPlace.frame.size.height)];
         labelLineOne = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, labelEventPlace.frame.origin.y+labelEventPlace.frame.size.height+5, self.view.frame.size.width-10, 1)];
        
       textViewEventDescription = [[UITextView alloc] initWithFrame:CGRectMake(labelLineOne.frame.origin.x+10, labelLineOne.frame.origin.y+labelLineOne.frame.size.height+10, labelLineOne.frame.size.width-20, 50)];
            textViewEventDescription.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:21];
        
    }
    else
    {
         labelDateAndTime = [[UILabel alloc]initWithFrame:CGRectMake(labelImageShadow.frame.origin.x+30, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height-5, (labelImageShadow.frame.size.width/2)-30, 30)];
         imageViewClock = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageShadow.frame.origin.x+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height+5, 16, 16)];
       
        
        imageViewPlace = [[UIImageView alloc]initWithFrame:CGRectMake(labelDateAndTime.frame.origin.x+labelDateAndTime.frame.size.width+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height+5, 16, 16)];
        
            labelEventPlace = [[UITextView alloc]initWithFrame:CGRectMake(imageViewPlace.frame.origin.x+imageViewPlace.frame.size.width+5, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height-5, (self.view.frame.size.width/2)-40, 30)];
        
        labelEventPlace.text = strOfEventPlace;
        
                labelEventPlace.backgroundColor = [UIColor clearColor];
        labelEventPlace.textColor  = [UIColor whiteColor];
        
        labelEventPlace.scrollEnabled = NO;
        labelEventPlace.editable = NO;
        
        CGRect frame = labelEventPlace.frame;
        frame.size.height = [labelEventPlace sizeThatFits:CGSizeMake(labelEventPlace.frame.size.width, MAXFLOAT)].height;
        labelEventPlace.frame = frame;
        
        
        
         buttonPlace = [[UIButton alloc]initWithFrame:CGRectMake(labelDateAndTime.frame.origin.x+labelDateAndTime.frame.size.width+10, labelImageShadow.frame.origin.y+labelImageShadow.frame.size.height, labelEventPlace.frame.size.width, labelEventPlace.frame.size.height)];
        
        
         labelLineOne   = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, labelEventPlace.frame.origin.y+labelEventPlace.frame.size.height, self.view.frame.size.width-10, 1)];
        
        textViewEventDescription = [[UITextView alloc] initWithFrame:CGRectMake(labelLineOne.frame.origin.x+10, labelLineOne.frame.origin.y+labelLineOne.frame.size.height+10, labelLineOne.frame.size.width-20, 30)];
            textViewEventDescription.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:15];
    }
    
    
    
    
  
    
   
     
     labelDateAndTime.text = [NSString stringWithFormat:@"%@ onwords",strOfDateAndTime];
    
    labelDateAndTime.numberOfLines = 1;
    labelDateAndTime.minimumFontSize = 8.;
    labelDateAndTime.adjustsFontSizeToFitWidth = YES;
     labelDateAndTime.textColor = [UIColor whiteColor];
    
     labelDateAndTime.textAlignment = NSTextAlignmentLeft;
     [_subview addSubview:labelDateAndTime];
    
   
    imageViewClock.image = [UIImage imageNamed:@"clock.png"];
    [_subview addSubview:imageViewClock];
     
    
   // MapView
   
    
    

    
    
    imageViewPlace.image = [UIImage imageNamed:@"location.png"];
    [_subview addSubview:imageViewPlace];
    
//    NSString *strOfPlace;
//
//    if(strOfEventPlace.length>25)
//    {
//        strOfPlace = [strOfEventPlace substringToIndex:25];
//    }
//    else{
//        strOfPlace = [NSString stringWithFormat:@"%@..........",strOfEventPlace];
//    }
//    NSLog(@"%@",strOfPlace);
  
    labelEventPlace.textColor = [UIColor whiteColor];
    
//    labelEventPlace.minimumFontSize = 8.;
//     labelEventPlace.adjustsFontSizeToFitWidth = YES;
    labelEventPlace.textAlignment = NSTextAlignmentLeft;
    [_subview addSubview:labelEventPlace];
    
    [buttonPlace addTarget:self action:@selector(buttonPlaceAction) forControlEvents:UIControlEventTouchUpInside];
    [buttonPlace setTitle:@"" forState:UIControlStateNormal];
    [_subview addSubview:buttonPlace];
    

    labelLineOne.backgroundColor = [UIColor whiteColor];
    [_subview addSubview:labelLineOne];
    
    //    strOfEventDescription = @"A young, struggling British filmmaker Sue McKinley (Alice Patten) comes across the diary of her grandfather, Mr. McKinley (Steven Mackintosh), who served as a jailer in the Imperial Police during the Indian independence movement. Through the diary, she learns about the story of five freedom fighters who were active in the movement: Chandrasekhar Azad, Bhagat Singh, Shivaram Rajguru, Ashfaqulla Khan, and Ram Prasad Bismil. McKinley, in his diary, states that he had met two type of people in his life: the first one... who died without uttering a sound and the second kind ... who died with lots of anguish.. crying over their deaths... McKinley reveals that it was then that he met with the third kind....";
    
    
    
    // NSString *str = @"This is a test text view to check the auto increment of height of a text view. This is only a test. ";
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    
    textViewEventDescription.text = [strOfEventDescription stringByTrimmingCharactersInSet:charSet];;
    
   
    textViewEventDescription.backgroundColor = [UIColor clearColor];
    textViewEventDescription.textColor  = [UIColor whiteColor];

    textViewEventDescription.scrollEnabled = NO;
    textViewEventDescription.editable = NO;
    
    CGRect frame = textViewEventDescription.frame;
    frame.size.height = [textViewEventDescription sizeThatFits:CGSizeMake(textViewEventDescription.frame.size.width, MAXFLOAT)].height;
    textViewEventDescription.frame = frame;
    
    [_subview addSubview:textViewEventDescription];
    UILabel *labelLineTwo;
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginStatus"] isEqualToString:@"Login"]) {
        [self navigationBar];
        
        if ([strOfEventOwnerId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]]) {
            
            
            
            if ([strOfEventType isEqualToString:@"Private"])
            {
               
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    buttonjoinThisScene  = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width-250)/2, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, 250, 50)];
                }
                else{
                    buttonjoinThisScene = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width-250)/2, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, 250, 40)];
                }
                [buttonjoinThisScene setTitle:@"Invite friend to join this scene" forState:UIControlStateNormal];
                buttonjoinThisScene.tag = 6;
                
                
                
                // [buttonjoinThisScene setBackgroundColor:[UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:29.0/255.0 alpha:1.0]];
                buttonjoinThisScene.layer.backgroundColor = [UIColor colorWithRed:217.0/255.0 green:94.0/255.0 blue:28.0/255.0 alpha:10.0].CGColor;
                buttonjoinThisScene.layer.cornerRadius = 5.0;
                buttonjoinThisScene.layer.masksToBounds = YES;
                [buttonjoinThisScene setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                
                [buttonjoinThisScene addTarget:self action:@selector(buttonJoinPublicEvent:) forControlEvents:UIControlEventTouchUpInside];
                [_subview addSubview:buttonjoinThisScene];
                labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, buttonjoinThisScene.frame.origin.y+buttonjoinThisScene.frame.size.height+10, self.view.frame.size.width-10, 1)];
                labelLineTwo.backgroundColor = [UIColor whiteColor];
                [_subview addSubview:labelLineTwo];
            }
        else
        {
            labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, self.view.frame.size.width-10, 1)];
            labelLineTwo.backgroundColor = [UIColor whiteColor];
            [_subview addSubview:labelLineTwo];
        }
            
            
//            }
           
        }
        else{
            if (![strOfEventType isEqualToString:@"Private"]) {
                
                if ([strOfSceneName isEqualToString:@"MyScene"]) {
                    
                    labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, self.view.frame.size.width-10, 1)];
                    labelLineTwo.backgroundColor = [UIColor whiteColor];
                    [_subview addSubview:labelLineTwo];
                }
                else
                {
                    buttonjoinThisScene = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width-150)/2, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, 150, 40)];
                    
                    if ([strOfSceneName isEqualToString:@"latest"]) {
                        
                        buttonjoinThisScene.layer.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:165.0/255.0 blue:162.0/255.0 alpha:1.0].CGColor;
                        
                        buttonjoinThisScene.layer.cornerRadius = 5.0;
                        buttonjoinThisScene.layer.masksToBounds = YES;
                        
                        [buttonjoinThisScene setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        buttonjoinThisScene.tag = 7;
                        [buttonjoinThisScene setTitle:@"Join this Scene" forState:UIControlStateNormal];
                        [buttonjoinThisScene addTarget:self action:@selector(buttonJoinPublicEvent:) forControlEvents:UIControlEventTouchUpInside];
                        [_subview addSubview:buttonjoinThisScene];
                        
                        labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, buttonjoinThisScene.frame.origin.y+buttonjoinThisScene.frame.size.height+10, self.view.frame.size.width-10, 1)];
                        labelLineTwo.backgroundColor = [UIColor whiteColor];
                        [_subview addSubview:labelLineTwo];
                        
                    }
                    else
                    {
                        labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, self.view.frame.size.width-10, 1)];
                        labelLineTwo.backgroundColor = [UIColor whiteColor];
                        [_subview addSubview:labelLineTwo];
                        
                    }
                    
                    
                }
               
                
            }
            else
            {
                labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, self.view.frame.size.width-10, 1)];
                labelLineTwo.backgroundColor = [UIColor whiteColor];
                [_subview addSubview:labelLineTwo];
            }
            

        }
        UILabel *labelJoiningThisScene;
        UILabel *labelNoneUser;
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
          labelJoiningThisScene   = [[UILabel alloc]initWithFrame:CGRectMake(labelLineTwo.frame.origin.x+5, labelLineTwo.frame.origin.y+labelLineTwo.frame.size.height+10, labelLineTwo.frame.size.width-20, 30)];
              labelJoiningThisScene.font = [UIFont systemFontOfSize:20.0];
            collectionViewInviteFriends=[[UICollectionView alloc] initWithFrame:CGRectMake(10, labelJoiningThisScene.frame.origin.y+labelJoiningThisScene.frame.size.height+10, self.view.frame.size.width-20, 80) collectionViewLayout:layout];
           labelNoneUser = [[UILabel alloc]initWithFrame:CGRectMake(10, labelJoiningThisScene.frame.origin.y+labelJoiningThisScene.frame.size.height+10, self.view.frame.size.width-20, 80)];
        }
        else
        {
            labelJoiningThisScene = [[UILabel alloc]initWithFrame:CGRectMake(labelLineTwo.frame.origin.x+5, labelLineTwo.frame.origin.y+labelLineTwo.frame.size.height+10, labelLineTwo.frame.size.width-20, 20)];
              labelJoiningThisScene.font = [UIFont systemFontOfSize:15.0];
            collectionViewInviteFriends=[[UICollectionView alloc] initWithFrame:CGRectMake(10, labelJoiningThisScene.frame.origin.y+labelJoiningThisScene.frame.size.height+10, self.view.frame.size.width-20, 60) collectionViewLayout:layout];
            labelNoneUser = [[UILabel alloc]initWithFrame:CGRectMake(10, labelJoiningThisScene.frame.origin.y+labelJoiningThisScene.frame.size.height+10, self.view.frame.size.width-20, 60)];
        }
        
        
        
        labelJoiningThisScene.text = @"Friends joining this scene.";
        labelJoiningThisScene.textColor = [UIColor whiteColor];
      
        labelJoiningThisScene.textAlignment = NSTextAlignmentLeft;
        [_subview addSubview:labelJoiningThisScene];
        
        
      
        
       
       
        
        //    collectionViewInviteFriends=[[UICollectionView alloc]initWithFrame:CGRectMake(10, labelLineTwo.frame.origin.y+labelLineTwo.frame.size.height+10, self.view.frame.size.width-20, 100)];
        [collectionViewInviteFriends setDataSource:self];
        [collectionViewInviteFriends setDelegate:self];
        // collectionViewInviteFriends.scrollIndicatorInsets = UICollectionViewScrollDirectionHorizontal;
        [collectionViewInviteFriends registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collectionViewInviteFriends setBackgroundColor:[UIColor clearColor]];
        
        [_subview addSubview:collectionViewInviteFriends];
        if ([[[dictOfInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] count] ==0) {
           
            
            
            labelNoneUser.text = @"NONE OF YOUR FRIENDS HAVE JOINED THIS SCENE";
            labelNoneUser.textColor = [UIColor whiteColor];
            labelNoneUser.numberOfLines = 2;
            labelNoneUser.font = [UIFont systemFontOfSize:15.0];
            labelNoneUser.textAlignment = NSTextAlignmentCenter;
            [_subview addSubview:labelNoneUser];
        }
        
        UILabel *labelLineThree = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, collectionViewInviteFriends.frame.origin.y+collectionViewInviteFriends.frame.size.height+10, self.view.frame.size.width-10, 1)];
        labelLineThree.backgroundColor = [UIColor whiteColor];
        [_subview addSubview:labelLineThree];
        
        
        
        
        //    UILabel *labelEventDescription = [[UILabel alloc]initWithFrame:CGRectMake(labelLine.frame.origin.x+10, labelLine.frame.origin.y+labelLine.frame.size.height+10, labelLine.frame.size.width-20, 30)];
        //
        //    labelEventDescription.text = strOfEventDescription;
        //    labelEventDescription.textColor = [UIColor whiteColor];
        //    labelEventDescription.font = [UIFont boldSystemFontOfSize:11.0];
        //    labelEventDescription.textAlignment = NSTextAlignmentRight;
        //    [_subview addSubview:labelEventDescription];
        
        //    if (items.count != 0) {
        dataTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, labelLineThree.frame.origin.y+labelLineThree.frame.size.height+10, self.view.frame.size.width-20, heightTableView)];
        dataTableView.delegate = self;
        dataTableView.dataSource = self;
        dataTableView.scrollEnabled = NO;
        [dataTableView reloadData];
        // [dataTableView setSeparatorStyle: UITableViewCellSeparatorStyleNone];
        dataTableView.backgroundColor = [UIColor clearColor];
        
        [_subview addSubview:dataTableView];
        //    }
        
        
        
        UILabel *labelLinefour = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, collectionViewInviteFriends.frame.origin.y+collectionViewInviteFriends.frame.size.height+10, self.view.frame.size.width-10, 1)];
        labelLineThree.backgroundColor = [UIColor whiteColor];
        [_subview addSubview:labelLineThree];
        
        _subview.frame = CGRectMake(0, 0,self.view.frame.size.width ,dataTableView.frame.origin.y+dataTableView.frame.size.height);
        [_scrollView addSubview:_subview];
        
        // dataTableView.frame = CGRectMake(0, 0, 320, heightTableView);
        
        _scrollView.contentSize =  CGSizeMake(_scrollView.frame.size.width, dataTableView.frame.origin.y+dataTableView.frame.size.height);
    }
    else
    {
        UILabel *labelJoinSceneDescription;
        UILabel *labelSignUpNow;
        UIButton *buttonSigninViaFB;
        UILabel *labelSigninViaFB;
        labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, textViewEventDescription.frame.origin.y+textViewEventDescription.frame.size.height+10, self.view.frame.size.width-10, 1)];
        labelLineTwo.backgroundColor = [UIColor whiteColor];
        [_subview addSubview:labelLineTwo];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            labelJoinSceneDescription = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, labelLineTwo.frame.origin.y+labelLineTwo.frame.size.height+10, self.view.frame.size.width-10, 50)];
            labelJoinSceneDescription.font = [UIFont systemFontOfSize:20.0];
            
               labelSignUpNow = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, labelJoinSceneDescription.frame.origin.y+labelJoinSceneDescription.frame.size.height+10, self.view.frame.size.width-10, 40)];
            
            buttonSigninViaFB = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width-200)/2, labelSignUpNow.frame.origin.y+labelSignUpNow.frame.size.height+5, 200, 70)];
            
            labelSigninViaFB = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-170)/2, labelSignUpNow.frame.origin.y+labelSignUpNow.frame.size.height+10, 170, 60)];
             labelSignUpNow.font = [UIFont systemFontOfSize:25.0];
               labelSigninViaFB.font = [UIFont systemFontOfSize:20.0];
            
        }
        else
        {
             labelJoinSceneDescription = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, labelLineTwo.frame.origin.y+labelLineTwo.frame.size.height+10, self.view.frame.size.width-10, 50)];
            labelJoinSceneDescription.font = [UIFont systemFontOfSize:15.0];
               labelSignUpNow = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+5, labelJoinSceneDescription.frame.origin.y+labelJoinSceneDescription.frame.size.height+10, self.view.frame.size.width-10, 40)];
            
            buttonSigninViaFB = [[UIButton alloc]initWithFrame:CGRectMake((self.view.frame.size.width-170)/2, labelSignUpNow.frame.origin.y+labelSignUpNow.frame.size.height+5, 170, 50)];
            
           labelSigninViaFB = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-150)/2, labelSignUpNow.frame.origin.y+labelSignUpNow.frame.size.height+10, 150, 40)];
             labelSignUpNow.font = [UIFont systemFontOfSize:18.0];
               labelSigninViaFB.font = [UIFont systemFontOfSize:15.0];
        }
        
        
       
        
    
        labelJoinSceneDescription.backgroundColor = [UIColor clearColor];
        labelJoinSceneDescription.text = @"You need an account to join this scene , or to see who else to join this scene";
        labelJoinSceneDescription.numberOfLines = 3;
        
        labelJoinSceneDescription.textColor = [UIColor whiteColor];
        labelJoinSceneDescription.textAlignment = NSTextAlignmentCenter;
        [_subview addSubview:labelJoinSceneDescription];
        
     
        labelSignUpNow.backgroundColor = [UIColor clearColor];
        labelSignUpNow.text = @"Sign up now!";
       
        labelSignUpNow.textColor = [UIColor whiteColor];
        labelSignUpNow.numberOfLines = 3;
        labelSignUpNow.textAlignment = NSTextAlignmentCenter;
        [_subview addSubview:labelSignUpNow];
        
        
    
        buttonSigninViaFB.layer.backgroundColor = [UIColor colorWithRed:55.0/255.0 green:116.0/255.0 blue:191.0/255.0 alpha:1.0].CGColor;
        buttonSigninViaFB.layer.cornerRadius = 5.0;
        [buttonSigninViaFB setTitle:@"" forState:UIControlStateNormal];
        [buttonSigninViaFB addTarget:self action:@selector(buttonSigninViaFBAction) forControlEvents:UIControlEventTouchUpInside];
        [_subview addSubview:buttonSigninViaFB];
        
        
        
        
     
        labelSigninViaFB.backgroundColor = [UIColor clearColor];
        labelSigninViaFB.text = @"Register Instantly via Facebook";
     
        labelSigninViaFB.textColor = [UIColor whiteColor];
        labelSigninViaFB.numberOfLines = 2;
        labelSigninViaFB.textAlignment = NSTextAlignmentCenter;
        [_subview addSubview:labelSigninViaFB];
        
        
        _subview.frame = CGRectMake(0, 0,self.view.frame.size.width ,buttonSigninViaFB.frame.origin.y+buttonSigninViaFB.frame.size.height + 10);
        [_scrollView addSubview:_subview];
        
        // dataTableView.frame = CGRectMake(0, 0, 320, heightTableView);
        
        _scrollView.contentSize =  CGSizeMake(_scrollView.frame.size.width, buttonSigninViaFB.frame.origin.y+buttonSigninViaFB.frame.size.height+10);
    }
    
    
    
}

-(void) buttonPlaceAction
{
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MapView"];
    [self.navigationController pushViewController:viewController animated:YES];
}


-(void) getInviteeFriendlist
{
    // http://whatscene.eventbuoy.com/dev/admin/admin/getjoineventlist.php?userid=2&eventid=
    
   // http://whatscene.eventbuoy.com/dev/admin/admin/getjoineventlist.php?userid=2&eventid=400
    
    NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/getjoineventlist.php?userid=%@&eventid=%@",strOfEventOwnerId,strOfEventId];
    
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *error = nil;
    if (data !=nil ) {
        dictOfInviteeUser = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            NSLog(@"err---- %@",error.description);
        }
        else{
            NSLog(@"%@",dictOfInviteeUser);
            
        }
    }
    
}



-(void) buttonInvitationAction:(id)sender
{
    
    
    
    dispatch_async(kBgQueue, ^{
        buttonCancelInvitation.enabled = NO;
              buttonAcceptInvitation.enabled = NO;
       
        UIButton *button = (UIButton *)sender;
        
        NSString *strOfStatus;
        if (button.tag == 1) {
            strOfStatus = @"Accept";
        }
        else if(button.tag == 2)
            
        {
            strOfStatus = @"Reject";
        }
        NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/get_responsetinvite.php?eventid=%@&senderid=%@&receiverid=%@&status=%@",strOfEventId,strOfEventOwnerId,[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfStatus];
        
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *error = nil;
        if (data !=nil ) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
            if (error) {
                NSLog(@"err---- %@",error.description);
            }
            else{
                
                if ([[json valueForKey:@"success"] isEqualToString:@"true"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         buttonCancelInvitation.enabled = YES;
                          buttonAcceptInvitation.enabled = YES;
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                    
                }
               
                
            }
        }

        
    });
    
    
    
   }





-(void) buttonSigninViaFBAction
{
    NSLog(@"Sign In");
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen) {
        // if a user logs out explicitly, we delete any cached token information, and next
        // time they run the applicaiton they will be presented with log in UX again; most
        // users will simply close the app or switch away, without logging out; this will
        // cause the implicit cached-token login to occur on next launch of the application
        [appDelegate.session closeAndClearTokenInformation];
        
    } else {
        if (appDelegate.session.state != FBSessionStateCreated) {
            // Create a new, logged out session.
            appDelegate.session = [[FBSession alloc] init];
        }
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState status,
                                                         NSError *error) {
            // and here we make sure to update our UX according to the new session state
            [self updateView];
        }];
        
        
    }
    
    
    
    
}
- (void)updateView
{
    // get the app delegate, so that we can reference the session property
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (appDelegate.session.isOpen)
    {
        // valid account UI is shown whenever the session is open
        //        [self.buttonLoginLogout setTitle:@"Log out" forState:UIControlStateNormal];
        //        [self.textNoteOrLink setText:[NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
        //appDelegate.session.accessTokenData.accessToken]];
        NSString *strData=[NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",
                           appDelegate.session.accessTokenData.accessToken];
        NSLog(@"%@",appDelegate.session.accessTokenData.accessToken);
        NSURL *strURl=[NSURL URLWithString:strData];
        NSError *err;
        NSData *data=[NSData dataWithContentsOfURL:strURl];
        dictOfFBData=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        NSLog(@"dictOfData>>>>>>>>>%@",dictOfFBData);
        //  NSString *strOfImage=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"id"]];
        
        
        dispatch_async(kBgQueue, ^{
            
            [self checkFbRegister];
            
        });
        
        
    }
    else
    {
        // login-needed account UI is shown whenever the session is closed
        //        [self.buttonLoginLogout setTitle:@"Log in" forState:UIControlStateNormal];
        //        [self.textNoteOrLink setText:@"Login to create a link to fetch account data"];
    }
}


-(void) signUpWithfacebook
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebooklogin.php?id=%@&firstname=%@&lastname=%@&email=%@&mob=%@&dob=%@",[dictOfFBData valueForKey:@"id"],[dictOfFBData valueForKey:@"first_name"],[dictOfFBData valueForKey:@"last_name"],[dictOfFBData valueForKey:@"email"],@"",[dictOfFBData valueForKey:@"birthday"]];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    
    NSError *err ;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",json);
            
            
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                strOfFbID =[[[json valueForKey:@"data"]valueForKey:@"fbid"]objectAtIndex:0];
                strOfFbUserId =[[[json valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0];
     
                NSLog(@"Signup Successfully");
                
                
            }
            
            
        }
    }
    
}

-(void) checkFbRegister
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebookloginstatus.php?fbid=%@",[dictOfFBData valueForKey:@"id"]];
    strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
            
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    

                    
                    
                    
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"LoginStatus"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"LoginFB" forKey:@"FBLoginStatus"];
                    [[NSUserDefaults standardUserDefaults] setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"notificatio_status"] objectAtIndex:0] forKey:@"notificatio_status"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0] forKey:@"image"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"firstname"]objectAtIndex:0] forKey:@"firstname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"lastname"]objectAtIndex:0] forKey:@"lastname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0] forKey:@"LoginUserId"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                    NSArray *key = [[[jsondata valueForKey:@"data"] objectAtIndex:0] allKeys];
                    if ([key containsObject:@"mobno"]) {
                        [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"mobno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                    }
                    if ([key containsObject:@"dob"]) {
                        [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"dob"]objectAtIndex:0] forKey:@"LoginUserDOB"];
                    }
                    
                    
                    
                    NSLog(@"Downloading...");
                    
                    image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0]]]];
                    [self uploadPhotoInDoc];
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
                    [self.navigationController pushViewController:vc animated:NO];
                    
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VarifiedMobileNoView"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                });
            }
        }
    }
}

-(void) uploadPhotoInDoc
{
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png",docDir];
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
    [data1 writeToFile:pngFilePath atomically:YES];

    
}


-(void)uploadFBImage
{
    ///home/listo8mw/public_html/IMAGEDEMO/uploadImage.php
    NSString *strOfImage=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"id"]];
    
   UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strOfImage]]];
    
    
    NSData *imgData = UIImagePNGRepresentation(image);
    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/userimageupload.php?user_id=%@",strOfFbUserId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"user%@",strOfFbUserId]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imgData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        NSLog(@"Error:%@",error.localizedDescription);
        return;
    }
    NSMutableDictionary *dict=[arr objectAtIndex:0];
    NSLog(@"Data in Dixtionary %@",dict);
}


    
    
    
    

-(void) buttonEditEvent
{
    NSLog(@"Edit");
   // strOfEventOwnerId = @"";
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Newscene"];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)buttonJoinPublicEvent:(id)sender
{
    
    UIButton *button = (UIButton *)sender;
    
    if (button.tag== 6) {
        NSLog(@"Private");
        
        if ([strOfEventOwnerId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]]) {
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteeUser"];
            [self.navigationController pushViewController:viewController animated:YES];
        }

    }
    else if (button.tag==7) {
        NSLog(@"public");
        
        
        dispatch_async(kBgQueue, ^{
            buttonjoinThisScene.enabled = NO;
            [self joinPublicEvent];
            dispatch_async(dispatch_get_main_queue(), ^{
                 buttonjoinThisScene.enabled = YES;
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have joined this scene successfully \n Find it in My Scene" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag = 90;
                [alert show];
//
               // [self viewWillAppear:YES];
            });
            
        });

       // http://whatscene.eventbuoy.com/dev/admin/admin/get_responsetinvite.php?eventid=386&senderid=247&receiverid=208&status=Reject
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (alertView.tag == 90) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void) joinPublicEvent
{
    
    
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/get_invitepublic.php?eventid=%@&ownerid=%@&userid=%@",strOfEventId,strOfEventOwnerId,[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]];
    //  strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
            
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                
                NSLog(@"Signup Successfully");
                
                
            }
            
            
        }
    }
    
}


#pragma mark -
#pragma mark UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[dictOfInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] count];
}

//The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    AsyncImageView *imageView;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    }
    else{
        imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    }

//--------------------------------Get null data of image------------------------------------------------------------//
    
    
////Dipak code ,if image get null value
    
    NSLog(@"%@",[[dictOfInviteeUser valueForKey:@"data"] valueForKey:@"image"]);
    
    NSString * empty = [[[dictOfInviteeUser valueForKey:@"data"] valueForKey:@"image"]objectAtIndex:indexPath.item];
    
    if ((NSNull *) empty!= [NSNull null] || [@"<null>" isEqualToString:empty])
    {
          imageView.imageURL = [NSURL URLWithString:[[[dictOfInviteeUser valueForKey:@"data"] valueForKey:@"image"] objectAtIndex:indexPath.item]];
    } else
    {
        
         imageView.image = [UIImage imageNamed:@"f3.png"];
        NSLog(@"Null data");

    }
    

    
    
    
    
    
//    imageView.imageURL = [NSURL URLWithString:[[[dictOfInviteeUser valueForKey:@"data"] valueForKey:@"image"] objectAtIndex:indexPath.item]];
//------------------------------------------------------------------------------------------------------------//
    
    if (imageView.image== nil)
    {
        imageView.image = [UIImage imageNamed:@"f3.png"];
    }
// imageView.image = [UIImage imageNamed:@"f3.png"];
   // imageView.contentMode = UIViewContentModeScaleAspectFit;
  //  cell.backgroundColor = [UIColor colorWithPatternImage: imageView.image];

               [[cell contentView] addSubview:imageView];

    cell.layer.cornerRadius = cell.frame.size.height/2;
    cell.layer.masksToBounds = YES;
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    { return CGSizeMake(60, 60);
    }
    else{
         return CGSizeMake(50, 50);
    }
   
}



#pragma mark -
#pragma mark UITableView Delegaates

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = [items objectAtIndex:[indexPath row]];
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - 20, 20000.0f);
    
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    
    CGFloat height = MAX(size.height+42, 44.0f);
    
    return height+20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    UILabel *label = nil;
    UILabel *labelbackgroundBorder;
    UILabel *labelCommnetedUserName,*labelCommnetDateAndTime;
    AsyncImageView * imageData = nil;
    
    cell = [dataTableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [dataTableView setSeparatorStyle: UITableViewCellSeparatorStyleNone];
        //        cell.separatorInset = UITableViewCellSeparatorStyleNone;
        
       
        
        
        
        label = [[UILabel alloc] initWithFrame:CGRectZero];
        labelbackgroundBorder = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        labelbackgroundBorder.backgroundColor = [UIColor clearColor];
        imageData = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 20, 35, 35)];
        
        //  imageData.layer.backgroundColor = [UIColor whiteColor].CGColor;
       
        NSString *strOFCommentUserId =[arrOfCommentUserId objectAtIndex:indexPath.row];
        NSLog(@"%@",[NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/images/user%@.png",strOFCommentUserId]);
        imageData.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/images/user%@.png",strOFCommentUserId]];
      //  if (imageData.image == nil) {
       //     cell.imageView.image = [UIImage imageNamed:@"f3.png"];
    //    }
        
        
         imageData.layer.cornerRadius = imageData.frame.size.height/2;
        imageData.layer.masksToBounds = YES;
         [[cell contentView] addSubview:imageData];
        //imageData.backgroundColor = [UIColor whiteColor];
        
        label.textColor = [UIColor whiteColor];
        [label setLineBreakMode:UILineBreakModeWordWrap];
        [label setMinimumFontSize:FONT_SIZE];
        [label setNumberOfLines:0];
        [label setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [label setTag:1];
        [labelbackgroundBorder setTag:2];
        labelbackgroundBorder.layer.borderWidth = 1.0;
        labelbackgroundBorder.layer.borderColor = [UIColor whiteColor].CGColor;
        // cell.contentView.backgroundColor = [UIColor clearColor];
        
        // [[label layer] setBorderWidth:2.0f];
        
        
        labelCommnetedUserName = [[UILabel alloc] initWithFrame:CGRectZero];
        labelCommnetedUserName.backgroundColor = [UIColor clearColor];
        labelCommnetedUserName.text = [arrOfCommentedUserName objectAtIndex:indexPath.row];
        labelCommnetedUserName.textColor = [UIColor whiteColor];
        
        labelCommnetDateAndTime = [[UILabel alloc] initWithFrame:CGRectZero];
        labelCommnetDateAndTime.backgroundColor = [UIColor clearColor];
        labelCommnetDateAndTime.text = [arrOfCommentDateAndTime objectAtIndex:indexPath.row];
        labelCommnetDateAndTime.textColor = [UIColor whiteColor];
        labelCommnetDateAndTime.textAlignment = NSTextAlignmentRight;
        labelCommnetDateAndTime.font = [UIFont systemFontOfSize:12.0];
        [[cell contentView] addSubview:labelCommnetDateAndTime];
        [[cell contentView] addSubview:labelbackgroundBorder];
        [[cell contentView] addSubview:label];
       
             [[cell contentView] addSubview:labelCommnetedUserName];
    }
    cell.contentView.backgroundColor = colorForViewScene;
    NSString *text = [items objectAtIndex:[indexPath row]];
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - 20, 20000.0f);
    
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    
    if (!label)
        label = (UILabel*)[cell viewWithTag:1];
    
    [label setText:text];
    NSInteger height =  MAX(size.height+42, 44.0f);
    
    [labelCommnetedUserName setFrame:CGRectMake(CELL_CONTENT_MARGIN, 7, CELL_CONTENT_WIDTH - 20, 20)];
    
    [label setFrame:CGRectMake(CELL_CONTENT_MARGIN, 32, CELL_CONTENT_WIDTH - 25, MAX(size.height, 10.0f))];
   [labelbackgroundBorder setFrame:CGRectMake(CELL_CONTENT_MARGIN-10, 5, CELL_CONTENT_WIDTH - 10, height+10)];
    
     [labelCommnetDateAndTime setFrame:CGRectMake(CELL_CONTENT_MARGIN-5, MAX(size.height, 10.0f)+40, CELL_CONTENT_WIDTH - 20, 15)];
    
    
    
    
    if ([strOftablerefreshFlag isEqualToString:@"YES"]) {
        heightTableView = [self tableViewHeight];
        
        [self prepareScrollView];
        if ([strOfScrollFlag  isEqualToString:@"YES"]) {
            CGPoint bottomOffset = CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height);
            [_scrollView setContentOffset:bottomOffset animated:YES];
            
        }
        strOftablerefreshFlag = @"NO";
        
    }
    
    
    //cell.imageView.image = [UIImage imageNamed:[dataImage objectAtIndex:indexPath.row]];
    
    return cell;
    
}


-(CGFloat) tableViewHeight
{
    CGFloat height = dataTableView.contentSize.height;
    return height;
}


//-(void)keyboardWillShow {
//    // Animate the current view out of the way
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)keyboardWillHide {
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if ([sender isEqual:_chatTextField])
//    {
//        //move the main view, so that the keyboard does not hide it.
//        if  (self.view.frame.origin.y >= 0)
//        {
//            [self setViewMovedUp:YES];
//        }
//    }
//}
//
////method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        NSLog(@"%f",rect.origin.y);
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        NSLog(@"%f",rect.size.height);
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        
        NSLog(@"%f",rect.origin.y);
        NSLog(@"%f",rect.size.height);
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
//
//
//
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}




//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
//    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
//    [self animateTextField: textField up: YES];
//}
//
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    [self animateTextField: textField up: NO];
//}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
- (IBAction)buttonSend:(id)sender {
    
    
    backView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-10-(keyboardHight+30), backView.frame.size.width, 40);
    chatTextField.frame = CGRectMake(5, self.view.frame.size.height-(keyboardHight+35), chatTextField.frame.size.width, 30);
    buttonSendcomment.frame =CGRectMake(self.view.frame.size.width-70, self.view.frame.size.height-(keyboardHight+30), buttonSendcomment.frame.size.width, buttonSendcomment.frame.size.height);
    _scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, chatTextField.frame.origin.y-5);
    
    
    strOftablerefreshFlag = @"YES";
    strOfScrollFlag  = @"YES";
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [chatTextField.text stringByTrimmingCharactersInSet:charSet];
    if ([trimmedString isEqualToString:@""]) {
        // it's empty or contains only white spaces
    }
    else
    {
        
        
        dispatch_async(kBgQueue, ^{
        
            buttonSendcomment.enabled = NO;
            [self sendComment];
//
            dispatch_async(dispatch_get_main_queue(), ^{
               chatTextField.text = @"";
                [dataTableView reloadData];
                  [self prepareScrollView];
                [self prepareScrollView];
                [dataTableView reloadData];
                buttonSendcomment.enabled = YES;
                CGPoint bottomOffset = CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height);
                [_scrollView setContentOffset:bottomOffset animated:YES];
            });
            
        });
        
        
        
        
    }
}

-(void) sendComment
{
    NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/add_comment.php?userid=%@&eventid=%@&comment=%@&count=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfEventId,chatTextField.text,[[NSUserDefaults standardUserDefaults]valueForKey:@"CommentCount"]];
   
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *error = nil;
    if (data !=nil ) {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            NSLog(@"err---- %@",error.description);
        }
        else{
            for (int i=0; i<[[[json valueForKey:@"data"] valueForKey:@"comment"] count];i++) {
               
                
                [[NSUserDefaults standardUserDefaults]setInteger:[[json valueForKey:@"count"] integerValue] forKey:@"CommentCount"];
                [items addObject:[[[json valueForKey:@"data"] valueForKey:@"comment"] objectAtIndex:i]];
                [arrOfCommentUserId addObject:[[[json valueForKey:@"data"] valueForKey:@"userid"] objectAtIndex:i]];
                [arrOfCommentedUserName addObject:[[[json valueForKey:@"data"] valueForKey:@"username"] objectAtIndex:i]];
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd/MM/yyyy"]; //// here set format of date which is in your output date (means above str with format
                NSDate *date = [dateFormat dateFromString: [[[json valueForKey:@"data"] valueForKey:@"date"] objectAtIndex:i]]; // here you can fetch date from string with define format
                
                dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd MMM yyyy"];// here set format which you want...
                NSString *dateString = [dateFormat stringFromDate:date];// here convert date in NSString
                
                NSLog(@"%@",dateString);
                
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                dateFormatter1.dateFormat = @"HH:mm";
                NSDate *date1 = [dateFormatter1 dateFromString:[[[json valueForKey:@"data"] valueForKey:@"time"] objectAtIndex:i]];
                dateFormatter1.dateFormat = @"hh:mm a";
                NSString *strOfCommentTime = [dateFormatter1 stringFromDate:date1];
                
                
                [arrOfCommentDateAndTime  addObject:[NSString stringWithFormat:@"%@ | %@",dateString,strOfCommentTime]];
            }
            
            NSLog(@"%@",json);
            
        }
    }
}




- (void) keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    keyboardHight = kbSize.height;
    // move the view up by 30 pts
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = frame.size.height-(kbSize.height);
    // _scrollView.frame = CGRectMake(0, kbSize.height, self.view.frame.size.width, self.view.frame.size.height-kbSize.height-30);
    //[self.view addSubview:_scrollView];
    //    [UIView beginAnimations: @"anim" context: nil];
    //    [UIView setAnimationBeginsFromCurrentState: YES];
    //    [UIView setAnimationDuration: movementDuration];
    //    self.view.frame = CGRectOffset(self.view.frame, 0, frame.origin.y);
    //    [UIView commitAnimations];
    
    [UIView animateWithDuration:0.2 animations:^{
        // self.view.frame = frame;
        //  _chatTextField.frame = CGRectMake(0, frame.size.height-30, _chatTextField.frame.size.width, _chatTextField.frame.size.height);
        
        
        NSLog(@"%f",_scrollView.bounds.size.height);
        
        backView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-10-(kbSize.height+chatTextField.frame.size.height), backView.frame.size.width, backView.frame.size.height);
        chatTextField.frame = CGRectMake(5, self.view.frame.size.height-(kbSize.height+chatTextField.frame.size.height+5), chatTextField.frame.size.width, chatTextField.frame.size.height);
        buttonSendcomment.frame =CGRectMake(self.view.frame.size.width-70, self.view.frame.size.height-(kbSize.height+35), buttonSendcomment.frame.size.width, buttonSendcomment.frame.size.height);
        
        
        _scrollView.frame = CGRectMake(self.view.frame.origin.x, -1, frame.size.width, chatTextField.frame.origin.y-5);
        //        _scrollViewHight.constant = chatTextField.frame.origin.y-5;
        //        [_scrollView needsUpdateConstraints];
        NSLog(@"%f",_scrollView.frame.size.height);
    
//        CGPoint bottomOffset = CGPointMake(self.view.frame.origin.x, _scrollView.contentSize.height - _scrollView.bounds.size.height);
//        [_scrollView setContentOffset:bottomOffset animated:YES];
    }];
    
    
}


-(void)scrollViewDidScroll:(UIScrollView*)scrollView{
    NSLog(@"scrollViewDidScroll");
    //  _scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, chatTextField.frame.origin.y-5);
    //    _scrollViewHight.constant = chatTextField.frame.origin.y-5;
    //    [_scrollView needsUpdateConstraints];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    NSLog(@"scrollViewDidEndScrollingAnimation");
}


- (void) keyboardDidHide:(NSNotification *)note {
    
    
    // move the view back to the origin
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = self.view.frame.size.height-(chatTextField.frame.size.height+10);
    keyboardHight = 0;
    [UIView animateWithDuration:0.2 animations:^{
        backView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-(chatTextField.frame.size.height+10), backView.frame.size.width, backView.frame.size.height);
        //        _scrollViewHight.constant = frame.size.height;
        //        [_scrollView needsUpdateConstraints];
        _scrollView.frame = frame;
        chatTextField.frame = CGRectMake(5, self.view.frame.size.height-(chatTextField.frame.size.height+5), chatTextField.frame.size.width, chatTextField.frame.size.height);
        
        buttonSendcomment.frame = CGRectMake(self.view.frame.size.width-70, self.view.frame.size.height-35, buttonSendcomment.frame.size.width, buttonSendcomment.frame.size.height);
    }];
    //    const float movementDuration = 0.0f;
    //    [UIView beginAnimations: @"anim" context: nil];
    //    [UIView setAnimationBeginsFromCurrentState: YES];
    //    [UIView setAnimationDuration: movementDuration];
    //    self.view.frame = CGRectOffset(self.view.frame, 0, frame.origin.y);
    //    [UIView commitAnimations];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // uncomment for non-ARC:
    // [super dealloc];
}

- (IBAction)buttonBack:(id)sender {
    if ([strOfSceneName isEqualToString:@"MyScene"]) {
        UIViewController *viewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"myscene"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
         [self.navigationController popViewControllerAnimated:YES];
    }
   
}

- (IBAction)shareButtonAction:(id)sender
{
    // _viewForShareToSocial.hidden = YES;
    
    
    
    if (_viewForShareToSocial.hidden == YES)
    {
        _viewForShareToSocial.hidden = NO;
        _buttonHide.hidden = NO;
  
    }
    else
    {
        
        _viewForShareToSocial.hidden = YES;
        _buttonHide.hidden = YES;
       
        
    }
  
}

-(void) hideButtonAction
{
    _viewForShareToSocial.hidden = YES;
    _buttonHide.hidden = YES;
}

- (IBAction)shareClickAction:(id)sender
{
    
    UIButton *button = (UIButton *)sender;
    if (button.tag == 1)
    {
        [self facebookShare];
        
    }
    else if (button.tag == 2)
    {
        [self twitterShare];
        
    }
    else if (button.tag == 3)
    {
               [self showEmail];
        }
    
    
    
    
    
    
}


//Facebook Share

-(void)facebookShare
{
    //    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    //    {
    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled)
        {
            
            
            NSLog(@"CancelledFB");
            
        }
        else
        {
            UIAlertView *sucessAlert=[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Uploaded Sucessfully on Facebook" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:Nil, nil];
            [sucessAlert show];
            
            
            NSLog(@"Done");
            
        }
        
        [controller dismissViewControllerAnimated:YES completion:Nil];
        
        
        
        
        
    };
    controller.completionHandler =myBlock;
    
    //Adding the Text to the facebook post value from iOS
    // [controller setInitialText:objOfAppDelegate.strTitle2];
    
    
    
    // [controller setInitialText:[NSString stringWithFormat:@"Share from JobsInHand\n\n\n\n\n\nJob Title : %@\n\n\n\nCompany Name : %@\n\n\n\n Location : %@ \n\n\n\n Job Description : %@\n\n\n\n \n\nFor APPLY Click on below given link :\n\n\n\n ",objOfAppDelegate.strTitle2,objOfAppDelegate.strCompany2,objOfAppDelegate.strLocation2,objOfAppDelegate.strDescription2]];
    [controller setInitialText:strOfEventDescription];
    //Adding the URL to the facebook post value from iOS
    
    
    [controller addURL:[NSURL URLWithString:strOfEventImage]];
    
    //Adding the Image to the facebook post value from iOS
    
    
    
    [self presentViewController:controller animated:YES completion:Nil];
    
    
    
    
}






//Twitter Share

-(void)twitterShare
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled)
        {
            
            
            NSLog(@"Cancelled");
            
        }
        else
        {
            UIAlertView *sucessAlert=[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Uploaded Sucessfully on Twitter" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:Nil, nil];
            [sucessAlert show];
            
            
            NSLog(@"Done");
            
            
            
        }
        
        [controller dismissViewControllerAnimated:YES completion:Nil];
        
        
        
        
    };
    controller.completionHandler =myBlock;
    
    //Adding the Text to the facebook post value from iOS
    // [controller setInitialText:objOfAppDelegate.strTitle2];
    
    
    //[controller addImage:image];
    // [controller setInitialText:[NSString stringWithFormat:@"Share from JobsInHand\n\n\n\n\n\nJob Title : %@\n\n\n\nCompany Name : %@\n\n\n\n Location : %@ \n\n\n\n Job Description : %@\n\n\n\n \n\nFor APPLY Click on below given link :\n\n\n\n ",objOfAppDelegate.strTitle2,objOfAppDelegate.strCompany2,objOfAppDelegate.strLocation2,objOfAppDelegate.strDescription2]];
    NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/get_comments.php?eventid"];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    
    NSString *shareData = [NSString stringWithFormat:@"%@\n\n%@",strOfEventDescription,strOfUrl];
    if (shareData.length>90) {
        shareData = [shareData substringToIndex:90];
    }
    shareData = [shareData stringByAppendingString:@".."];
    
    
    
    
    
    [controller setInitialText:strOfEventDescription];
    [controller addURL:[NSURL URLWithString:strOfEventImage]];
    [controller addURL:urlOfStr];
    controller.completionHandler =myBlock;
    
    [controller setInitialText:shareData];
    
    
    
    //Adding the Image to the facebook post value from iOS
    
    
    
    [self presentViewController:controller animated:YES completion:Nil];
    
    
    
    
    
    
    
}

//Gmail Share

-(void)showEmail
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if (mailClass != nil)
    {
        if ([mailClass canSendMail])
        {
            [self displayMailComposerSheet];
        }
        else
        {
            UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Email Not Configured" message:@"Your EmailId not Cofigured please configure your email" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    
}
-(void)displayMailComposerSheet
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate=self;
    [picker setSubject:strOfEventDescription];
    
    [picker setMessageBody:strOfEventImage isHTML:YES];
    
    //    NSString *imagefolder=[[NSBundle mainBundle]pathForResource:@"mail123" ofType:@"png"];
    //    NSData *data =[NSData dataWithContentsOfFile:imagefolder];
    NSURL *Imageurl = [NSURL URLWithString:@""];
    NSData *data =  [NSData dataWithContentsOfURL:Imageurl];
    [picker addAttachmentData:data mimeType:@"image/png" fileName:@"ra"];
    
    [self presentViewController:picker animated:YES completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Result: Mail sending canceled") ;
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result: Mail saved")	;
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result: Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result: Mail sending failed");
            break;
        default:
            NSLog(@"Result: Mail not sent");
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
