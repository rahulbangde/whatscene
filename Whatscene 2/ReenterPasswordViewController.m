//
//  ReenterPasswordViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 14/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "ReenterPasswordViewController.h"

@interface ReenterPasswordViewController ()
{


    NSString *savedValue;
    NSString * srtingforsavevalue;

}
@property (strong, nonatomic) IBOutlet UITextField *textFieldForNewPassword;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForReenterNewPassword;
@property (weak, nonatomic) IBOutlet UIButton *buttonForSubmitPassword;

@end

@implementation ReenterPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIView *paddingNewPassword;
    
    UIView *paddingReenterNewPassword;
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingNewPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        
      
    }
    else
    {
        
        paddingReenterNewPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
    }
    
    _textFieldForNewPassword.leftView = paddingNewPassword;
    _textFieldForNewPassword.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForReenterNewPassword.leftView = paddingReenterNewPassword;
    _textFieldForReenterNewPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    // Do any additional setup after loading the view.
    
 savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"forgot_email"];
    _buttonForSubmitPassword.layer.cornerRadius = 10.0f;
    _buttonForSubmitPassword.clipsToBounds=YES;

    
    
    NSLog(@"%@",savedValue);
    UIColor *color =[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];
    
    [_textFieldForNewPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForReenterNewPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    
    
    
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    return YES;
    
}

- (IBAction)ReenterPassword:(id)sender
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString* temp = [defaults objectForKey:@"textField1Text"];
    [defaults synchronize];
    NSLog(@"%@",temp);
    

    
     if([_textFieldForNewPassword.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter new password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if(_textFieldForNewPassword.text.length<6)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter atleast six character of password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
   else if([_textFieldForReenterNewPassword.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter repeat password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if (![_textFieldForNewPassword.text isEqualToString: _textFieldForReenterNewPassword.text])
    {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your new password and reapeat password does not match" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
    }
    else
    {
        
        NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/changepassword_sub.php?password=%@&email=%@",_textFieldForNewPassword.text,temp];
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *err;
        if (data !=nil )
        {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
            if (err) {
                NSLog(@"err---- %@",err.description);
            }
            else{
                NSLog(@"Result = %@",json);
                NSLog(@"Result = %@",[[json valueForKey:@"result"]valueForKey:@"response"]);
                UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
                
                [self.navigationController pushViewController:vc animated:NO];
              //Make Small for success & make true
                if ([[json valueForKey:@"success"] isEqualToString:@"true"])
                {
                    NSLog(@"Login Successfully");
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
                    [self.navigationController pushViewController:vc animated:NO];
                }
                else{
                
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Password must differ from old password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                
                    NSLog(@"Unsucessful reenterpasswordviewcontroller");
                
                }
            }
            
            
            
            
        }
            
        }
    
    NSLog(@"done");

    
    
}
- (IBAction)BackButton:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"code"];
    [self.navigationController pushViewController:vc animated:NO];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
