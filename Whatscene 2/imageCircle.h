//
//  imageCircle.h
//  Whatscene
//
//  Created by Black Bean Engagement on 14/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE

@interface imageCircle : UIImageView

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@end
