
//
//  MainMenuViewcontrollerViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 06/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "MainMenuViewcontrollerViewController.h"
#import "SeondTableViewCell.h"
#import "ViewController.h"
#import "UITableView+DragLoad.h"
#import "AsyncImageView.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)


@implementation FrameObservingView
- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self.delegate frameObservingViewFrameChanged:self];
}
@end
NSString *strOfSceneName,*strOfInviteeUserName;
NSString *strOfEventImage,*strOfEventDateAndTime,*strOfEventDescription,*strOfEventName,*strOfEventPlace,*strOfEventId,*strOfEventOwnerId,*strOfEventType,*strOfEventInvitedStatus;
UIColor *colorForViewScene;
CGFloat eventLatitude,eventlongitude;
@interface MainMenuViewcontrollerViewController ()<UITableViewDelegate,UITableViewDataSource,UITableViewDragLoadDelegate,
FrameObservingViewDelegate>
{
    NSMutableArray * arrOfEventName,*arrOfEventDateAndtime,*arrOfEventAddress,*arrOfCellImages,*arrOfEventImages,*arrOfEventID,*arrOfEventDescription,*arrOfEventOwnerId,*arrOfEventType,*arrOfEventInviteStatus,*arrOfEventLatitude,*arrOfEventLongitude,*arrOfInviteeFriendName;
    NSMutableArray * arrayOfImages;
    
    UITableView * tableViewForMyScene;
    NSInteger pageNumber;
    NSInteger pageLimit;
    BOOL rightClickEvent;
    
    
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *buttonRightHide;

@property (strong, nonatomic) IBOutlet UIButton *buttonCreateNewScene;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonBack;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonForMenu;




@end

@implementation MainMenuViewcontrollerViewController

- (void)frameObservingViewFrameChanged:(FrameObservingView *)view
{
    tableViewForMyScene.frame =  CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width ,self.view.frame.size.height-64);
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    [self navigationBar];
    _activityIndicator.hidden = YES;
    strOfSceneName = @"latest";
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]) {
        _buttonCreateNewScene.hidden = YES;
    }
   
    _buttonRightHide.hidden = YES;
    [_buttonRightHide addTarget:self action:@selector(DrawerMenuAction) forControlEvents:UIControlEventTouchUpInside];
    colorForViewScene = [UIColor colorWithRed:73.0/255.0 green:193.0/255.0 blue:190.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setBarTintColor:colorForViewScene];
    self.view.backgroundColor = colorForViewScene;
    [_buttonCreateNewScene addTarget:self action:@selector(buttonCreateNewSceneAction) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    
    // [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"PUREMCT.PNG"] forBarMetrics:UIBarMetricsDefault];
    rightClickEvent = YES;
    arrOfEventAddress = [NSMutableArray new];
    arrOfEventDateAndtime = [NSMutableArray new];
    arrOfEventName = [NSMutableArray new];
    arrOfEventImages = [NSMutableArray new];
    arrOfEventID = [NSMutableArray new];
    arrOfEventDescription = [NSMutableArray new];
    arrOfEventOwnerId = [NSMutableArray new];
        arrOfEventType = [NSMutableArray new];
    arrOfEventInviteStatus = [NSMutableArray new];
    arrOfCellImages = [[NSMutableArray alloc]initWithObjects:@"1.JPG",@"2.JPG",@"3.JPG",@"2.JPG",@"3.JPG", nil];
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 240, 300, 30)];
    headingLabel.text = @"Scorer";
    
    
    
    
    FrameObservingView *frameObservingView = [[FrameObservingView alloc] init];
    frameObservingView.delegate = self;
    [self.view addSubview:frameObservingView];
    
    
    
    tableViewForMyScene = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    // _tableMainList.frame = CGRectMake(8, 229, self.view.frame.size.width-16, self.view.frame.size.height-239);
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        tableViewForMyScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-64);
//    } else
//    {
        tableViewForMyScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width ,self.view.frame.size.height-64);
//    }
    
    tableViewForMyScene.backgroundColor = [UIColor clearColor];
    tableViewForMyScene.dataSource = self;
    tableViewForMyScene.delegate = self;
    [tableViewForMyScene setDragDelegate:self refreshDatePermanentKey:@"FriendList"];
    tableViewForMyScene.showLoadMoreView = YES;
       tableViewForMyScene.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableViewForMyScene];
    [self.view addSubview:_buttonCreateNewScene];
    [self.view addSubview:_buttonRightHide];
    [self.view addSubview:_activityIndicator];
    
    
    
}

-(void)buttonCreateNewSceneAction
{
   

    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) navigationBar
{
    UIButton * loginButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [loginButton setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(DrawerMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *loginButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:loginButtonItem, nil];
}



-(void) viewWillAppear:(BOOL)animated
{
    arrOfEventAddress = [NSMutableArray new];
    arrOfEventDateAndtime = [NSMutableArray new];
    arrOfEventName = [NSMutableArray new];
    arrOfEventImages = [NSMutableArray new];
    arrOfEventID = [NSMutableArray new];
    arrOfEventDescription = [NSMutableArray new];
    arrOfEventOwnerId = [NSMutableArray new];
    arrOfEventType = [NSMutableArray new];
    arrOfEventInviteStatus = [NSMutableArray new];
        arrOfEventLatitude = [NSMutableArray new];
    arrOfEventLongitude = [NSMutableArray new];
    arrOfInviteeFriendName = [NSMutableArray new];
    pageLimit = 10;
    pageNumber = 1;
     [tableViewForMyScene reloadData];
    self.navigationController.navigationBar.hidden = NO;
    
    
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    if (netStatus == NotReachable)
    {
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        
        dispatch_async(kBgQueue, ^{
            
            [self getLetestSceenData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableViewForMyScene reloadData];
                
                
            });
            
            
            
        });
    }
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         self.buttonBack.frame = CGRectMake(self.buttonBack.frame.origin.x+100,
//                                                            self.buttonBack.frame.origin.y,
//                                                            self.buttonBack.frame.size.width,
//                                                            self.buttonBack.frame.size.height);
//                     }];

    
    
   
    
}

-(void) getLetestSceenData
{
    NSString *strOfUserStoredId;
    
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]) {
        strOfUserStoredId = @"";
    }
    else
    {
        strOfUserStoredId = [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"];
    }
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/getscheduleevent.php?page=%ld&limit=%ld&user_id=%@",(long)pageNumber,(long)pageLimit,strOfUserStoredId];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err)
        {
            NSLog(@"err---- %@",err.description);
        }
        else{
            
            if([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                NSArray *arr = [[json valueForKey:@"data"] valueForKey:@"event_name"];
                if (arr.count != 0) {
                    pageNumber++;
                }
                
                
                for (int i = 0; i<arr.count; i++)
                {
                    [arrOfEventName addObject:[[[json valueForKey:@"data"] valueForKey:@"event_name"] objectAtIndex:i]];
                    
                    NSString *strOfEventDateAndTime = [NSString stringWithFormat:@"%@ | %@",[[[json valueForKey:@"data"] valueForKey:@"event_time"] objectAtIndex:i],[[[json valueForKey:@"data"] valueForKey:@"event_date"] objectAtIndex:i]];
                    [arrOfEventDateAndtime addObject:strOfEventDateAndTime];
                    [arrOfEventAddress addObject:[[[json valueForKey:@"data"] valueForKey:@"event_address"] objectAtIndex:i]];
                    [arrOfEventDescription addObject:[[[json valueForKey:@"data"] valueForKey:@"event_description"] objectAtIndex:i]];
                    [arrOfEventID addObject:[[[json valueForKey:@"data"] valueForKey:@"event_id"] objectAtIndex:i]];
                    [arrOfEventOwnerId addObject:[[[json valueForKey:@"data"] valueForKey:@"owner_id"] objectAtIndex:i]];
                    //After you get image from web service add image arrOfEventImages & remove arrOfCellImageslogin
                    [arrOfEventImages addObject:[[[json valueForKey:@"data"]valueForKey:@"event_image_url"]objectAtIndex:i]];
                    [arrOfEventType addObject:[[[json valueForKey:@"data"]valueForKey:@"event_category"]objectAtIndex:i]];
                    [arrOfCellImages addObject:[arrOfCellImages objectAtIndex:i]];
                    [arrOfEventInviteStatus addObject:[[[json valueForKey:@"data"]valueForKey:@"event_invited_status"]objectAtIndex:i]];
                    [arrOfEventLatitude addObject:[[[json valueForKey:@"data"] valueForKey:@"event_latitude"] objectAtIndex:i]];
                    [arrOfEventLongitude addObject:[[[json valueForKey:@"data"] valueForKey:@"event_longitude"] objectAtIndex:i]];
                    NSArray *key = [[[json valueForKey:@"data"] objectAtIndex:0] allKeys];
                    
                    if ([key containsObject:@"owner_name"]) {
                        [arrOfInviteeFriendName addObject:[[[json valueForKey:@"data"]valueForKey:@"owner_name"]objectAtIndex:i]];
                    }
                    else{
                        [arrOfInviteeFriendName addObject:@""];
                    }
                    
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [_activityIndicator startAnimating];;
                _activityIndicator.hidden = YES;
                
                
            });
            
            NSLog(@"%@",arrOfEventName);
            
            
        }
    }
}




#pragma mark - Drag delegate methods

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}
- (void)finishRefresh
{
    // _dataCount = 10;
    [tableViewForMyScene finishRefresh];
    [tableViewForMyScene reloadData];
}






-(void) finishLoadMore

{
    //    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    //    [networkReachability startNotifier];
    //    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    //
    //    if (networkStatus == NotReachable)
    //    {
    
    // [_tableMainList finishLoadMore];
    // [_tableMainList reloadData];
    //        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
    //        [networkAlert show];
    
    
    //    }
    //    else
    //    {
    
    
    [self refreshTable];
    
    //    }
}

- (void)refreshTable {
 
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    if (netStatus == NotReachable)
    {
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        
        dispatch_async(kBgQueue, ^{
            
            [self getLetestSceenData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableViewForMyScene reloadData];
                [tableViewForMyScene finishLoadMore];
                
            });
            
            
            
        });
    }
  
    NSLog(@"Rahul");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 200.0;
    }
    else{
        return 150.0;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrOfEventName.count;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=[tableViewForMyScene dequeueReusableCellWithIdentifier:@"cell"];
    
    
    if (cell == nil)
        
    {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];;
        
    }
    
    AsyncImageView *imageView;
    UILabel *labelTransparent;
    UILabel *labelEventName;
    UILabel *labelEventDateAndtime;
    UILabel *labelEventAdreess;
    
    UIImageView *image;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        image = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 50 , 50)];
        imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 190)];
          labelTransparent = [[UILabel alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, 80)];
        labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(10, 120, self.view.frame.size.width-20, 30)];
         labelEventName.font = [UIFont boldSystemFontOfSize:25];
        labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 155, (self.view.frame.size.width-20)/2, 20)];
            labelEventAdreess = [[UILabel alloc]initWithFrame:CGRectMake(10, 175, (self.view.frame.size.width-20)/2, 20)];
         labelEventAdreess.font = [UIFont systemFontOfSize:15];

    }
    else
    {
        image = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 50 , 50)];
        imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 140)];
          labelTransparent = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 60)];
        labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, self.view.frame.size.width-20, 20)];
         labelEventName.font = [UIFont boldSystemFontOfSize:17];
 labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 100, (self.view.frame.size.width-20)/2, 20)];
            labelEventAdreess = [[UILabel alloc]initWithFrame:CGRectMake(10, 120, (self.view.frame.size.width-20)/2, 20)];
         labelEventAdreess.font = [UIFont systemFontOfSize:12];
        
    }
    
    if ([[arrOfEventType objectAtIndex:indexPath.row] isEqualToString:@"Private"]) {
         image.image = [UIImage imageNamed:@"private1.png"];
    }
    else
    {
             image.image = [UIImage imageNamed:@""];
    }

   
   
    
    if ([arrOfEventImages objectAtIndex:indexPath.row] != [NSNull null])
    {
        imageView.imageURL =[NSURL URLWithString:[arrOfEventImages objectAtIndex:indexPath.row]];
    }
    else
    {
        imageView.image = [UIImage imageNamed:@""];
    }
    
  
    
    
    
    [cell.contentView addSubview:imageView];
    
    cell.contentView.backgroundColor = [UIColor colorWithRed:73.0/255.0 green:193.0/255.0 blue:190.0/255.0 alpha:1.0];
    
    
  
    
    labelTransparent.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.7];
    
    [cell.contentView addSubview:labelTransparent];

    
    
 
    
    labelEventName.text = [arrOfEventName objectAtIndex:indexPath.row];
    
   
    
    labelEventName.textColor = [UIColor whiteColor];
    
    //labelEventName.numberOfLines = 2;
    
    [cell.contentView addSubview:labelEventName];
    
    
    
    
    
    
    
    
    NSArray *arrOfDateAndTime = [[arrOfEventDateAndtime objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
    
    NSLog(@"%@",arrOfDateAndTime);
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-dd-MM"]; //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date = [dateFormatter dateFromString: [arrOfDateAndTime objectAtIndex:1]]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];// here set format which you want...
    NSString *dateString = [dateFormatter stringFromDate:date];// here convert date in NSSt
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"HH:mm:ss";
    NSDate *date1 = [dateFormatter1 dateFromString:[arrOfDateAndTime objectAtIndex:0]];
    dateFormatter1.dateFormat = @"hh:mm a";
    NSString *strOfEventTime = [dateFormatter1 stringFromDate:date1];
    
    NSLog(@"Converted String : %@",strOfEventTime);
    
    NSDateFormatter *dateFormatForDay = [[NSDateFormatter alloc] init];

    [dateFormatForDay setDateFormat:@"EE"];
    NSString *strOfEventday = [dateFormatForDay stringFromDate:date];
  
    
    NSString *strOfDateAndTime = [NSString stringWithFormat:@"%@ %@ | %@",strOfEventday,strOfEventTime,dateString];
    labelEventDateAndtime.text = strOfDateAndTime;
    labelEventDateAndtime.numberOfLines = 1;
    labelEventDateAndtime.minimumFontSize = 8.;
    labelEventDateAndtime.adjustsFontSizeToFitWidth = YES;
    labelEventDateAndtime.textColor = [UIColor whiteColor];

    [cell.contentView addSubview:labelEventDateAndtime];
    
    
    

    
    labelEventAdreess.text = [arrOfEventAddress objectAtIndex:indexPath.row];
    
    labelEventAdreess.textColor = [UIColor whiteColor];
 
   
    [cell.contentView addSubview:labelEventAdreess];
    
     [cell.contentView addSubview:image];
    
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    strOfEventImage = [arrOfEventImages objectAtIndex:indexPath.row];
    strOfEventDateAndTime = [arrOfEventDateAndtime objectAtIndex:indexPath.row];
    strOfEventDescription = [arrOfEventDescription objectAtIndex:indexPath.row];
    strOfEventName = [arrOfEventName objectAtIndex:indexPath.row];
    strOfEventPlace = [arrOfEventAddress objectAtIndex:indexPath.row];
    strOfEventId = [arrOfEventID objectAtIndex:indexPath.row];
     strOfEventOwnerId = [arrOfEventOwnerId objectAtIndex:indexPath.row];
         strOfEventType = [arrOfEventType objectAtIndex:indexPath.row];
    strOfEventInvitedStatus = [arrOfEventInviteStatus objectAtIndex:indexPath.row];
    eventLatitude = [[arrOfEventLatitude objectAtIndex:indexPath.row] floatValue];
        eventlongitude = [[arrOfEventLongitude objectAtIndex:indexPath.row] floatValue];
    strOfInviteeUserName = [arrOfInviteeFriendName objectAtIndex:indexPath.row];
    if (strOfEventInvitedStatus == (id)[NSNull null] || strOfEventInvitedStatus.length == 0 )
    {
       strOfEventInvitedStatus = @"Something";
    }
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"viewEvent"];
    [self.navigationController pushViewController:viewController animated:NO];
    
    
    
    //    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
    //    [self.navigationController pushViewController:vc animated:NO];
    NSLog(@"done");
    
    
}





-(void) viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)DrawerMenu:(id)sender
{
    
    if (rightClickEvent==YES) {
    _buttonRightHide.hidden = NO;
    rightClickEvent = NO;
}
else
{
    _buttonRightHide.hidden = YES;
    rightClickEvent = YES;
}
    
    [self.ViewController rightRevealToggle:nil];
    

    
    
    
}

-(void) DrawerMenuAction
{
    _buttonRightHide.hidden = YES;
    rightClickEvent = YES;
    [self.ViewController rightRevealToggle:nil];
}


- (IBAction)BackToHome:(id)sender
{
    
    UIViewController * viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
    [self.navigationController pushViewController:viewcontroller animated:NO];
    
    
}




@end
