//
//  RequestCodeViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 14/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "RequestCodeViewController.h"

@interface RequestCodeViewController ()<UITextFieldDelegate>
{


    NSString *savedValue;
      NSString *valueToSave;


}
@property (strong, nonatomic) IBOutlet UITextField *textFieldForRequestedCode;

@property (strong, nonatomic) IBOutlet UIButton *buttonForSubmitCode;
@end

@implementation RequestCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Padding in Text View
    
    UIView *paddingRequestedCode;
   
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingRequestedCode = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        
     
    }
    else
    {
        paddingRequestedCode = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
     
        
    }
    
    _textFieldForRequestedCode.leftView = paddingRequestedCode;
    _textFieldForRequestedCode.leftViewMode = UITextFieldViewModeAlways;
    
   
    
    
    
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    // Do any additional setup after loading the view.

    UIColor *color =[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];
    
    _buttonForSubmitCode.layer.cornerRadius = 10.0f;
    _buttonForSubmitCode.clipsToBounds=YES;

    
    
    [_textFieldForRequestedCode setValue:color forKeyPath:@"_placeholderLabel.textColor"];


   
    

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    return YES;
    
}


- (IBAction)SubmitCode:(id)sender
{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];

    NSString* temp = [defaults objectForKey:@"textField1Text"];
    [defaults synchronize];
    NSLog(@"%@",temp);

    
    if ([_textFieldForRequestedCode.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"please enter your OTP number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }else {
        
      
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"preferenceName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/otp_sub.php?otp=%@&email=%@",_textFieldForRequestedCode.text,temp];
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *err;
        if (data!=nil )
        {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
            if (err) {
                NSLog(@"err---- %@",err.description);
            }
            else{
                NSLog(@"Result = %@",json);
                NSLog(@"Result = %@",[[json valueForKey:@"result"]valueForKey:@"response"]);
                
                
              

                
                if ([[json valueForKey:@"success"] isEqualToString:@"true"])
                {
                                        NSLog(@"Login Successfully");
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"reenter"];
                    [self.navigationController pushViewController:vc animated:NO];
                    
                }else
                {
                    
                    NSLog(@"sorry you cant not login requestcodeviewcontroller");
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"please enter correct OTP number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    
                }
            }
            
            
            
            
            
            
        }
        
        
    }
    

       NSLog(@"done");


    
    
    
}


- (IBAction)BackButton:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"forgot"];
    [self.navigationController pushViewController:vc animated:NO];

    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
