//
//  RightMenuViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 11/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "RightMenuViewController.h"
#import "SignUpForWhatscene.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface RightMenuViewController ()<UITableViewDelegate,UITableViewDataSource>
{

    NSMutableArray * arrayofmenudata;
   NSString *strofChangeFrame;
    NSString* image1;
    UIImage * image;
    NSString * ImagePathLocation,*strOfNotificationStatus;
    UIView *contentView;
    UIColor *buttonBackgroundColor,*colorBackground;
    

}

@property (strong, nonatomic) IBOutlet UILabel *labelAboutUs2;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UIView *viewForUnregisterUser;


@end
NSString *strOfSocialNavigationTitle;
@implementation RightMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    buttonBackgroundColor = [UIColor colorWithRed:83.0/255.0 green:78.0/255.0 blue:83.0/255.0 alpha:1.0];
    colorBackground = [UIColor colorWithRed:63.0/255.0 green:59.0/255.0 blue:63.0/255.0 alpha:1.0];
    
    
///////Button Corner modification//////////////////////////////////////////////////////////////////
  
   


    
    
    
    _labelAboutUs2.layer.cornerRadius = 12.0f;
    _labelAboutUs2.layer.masksToBounds = YES;
    _labelAboutUs2.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    _labelAboutUs2.layer.borderWidth= 1.0f;
    
  
    
   
//Convert url data to image
//    NSURL *imageURL = [NSURL URLWithString:@"http://example.com/demo.jpg"];
//    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//    UIImage *image = [UIImage imageWithData:imageData];

    
    
    
}


-(void) viewWillAppear:(BOOL)animated
{
    
    
    
    
//      ///////Retrive Value From NSUSERDEFAULT/////////
//      _imageViewForRegisterUser.layer.cornerRadius = _imageViewForRegisterUser.frame.size.height/2;
//    _imageViewForRegisterUser.layer.masksToBounds = YES;
//    _imageViewForRegisterUser.image = [self loadImage];
//    if (_imageViewForRegisterUser.image == nil) {
//        _imageViewForRegisterUser.image = [UIImage imageNamed:@"f3.png"];
//    }
    
    
//_imageViewForRegisterUser.image = image;

    
 
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginStatus"] isEqualToString:@"Login"]) {
 
      
         _scrollView.hidden = NO;
        _viewForUnregisterUser.hidden =YES;
        [self prepareScrollview];
       
    }
    else
    {
    
        _scrollView.hidden = YES;
        _viewForUnregisterUser.hidden =NO;
    }

   
    
}

-(void) prepareScrollview
{
    [contentView removeFromSuperview];
    contentView = [[UIView alloc] init];
    contentView.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0];
    UIImageView *imageViewEditProfile,*imageViewChangePassword,*imageViewSignout,*imageViewRateUs,*imageViewPrivacyPolicy,*imageViewAbout;
    UIImageView *imageViewUser;
    UILabel *labelUserNameBackgound;
    UILabel *labelMuteNotification;
    UILabel *labelUserName;
    UISwitch *switchForNotification;
    UIButton *buttonEditProfile;
    UIButton *buttonChangePassword;

    UIButton *buttonRateUsOnAppStore;
  
    UIButton *buttonPrivacyPolicy;
  
    UIButton *buttonSignout,*buttonAbout;
    UILabel *labelLineOne,*labelLineTwo;
    
    
    UILabel *labelBackground1,*labelSignout,*labelRateUsOnAppStore,*labelPrivacyPolicy,*labelAbout,*labelLineSeven;
    UILabel *labelEditProfile,*labelChangePassword,*labelLineThree,*labelLineFour,*labelLineFive,*labelLineSix;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
      
        
       
        
        
        
        imageViewUser = [[UIImageView alloc]initWithFrame:CGRectMake((self.scrollView.frame.size.width-110)/2, 38, 110, 110)];
        labelUserName = [[UILabel alloc]initWithFrame:CGRectMake(0, imageViewUser.frame.origin.y+imageViewUser.frame.size.height, self.scrollView.frame.size.width, 40)];
        
        
        
        labelBackground1 =  [[UILabel alloc]initWithFrame:CGRectMake(0, labelUserName.frame.size.height+labelUserName.frame.origin.y+10, self.scrollView.frame.size.width, 70)];
        labelMuteNotification = [[UILabel alloc]initWithFrame:CGRectMake(10, labelBackground1.frame.origin.y+15, self.scrollView.frame.size.width-10, 40)];
        labelUserNameBackgound = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, labelMuteNotification.frame.origin.y)];
        switchForNotification = [[UISwitch alloc]initWithFrame:CGRectMake(176, labelBackground1.frame.origin.y+19, 49, 31)];
        
        labelLineOne = [[UILabel alloc]initWithFrame:CGRectMake(0, labelBackground1.frame.origin.y+labelBackground1.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        buttonEditProfile =  [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineOne.frame.size.height+labelLineOne.frame.origin.y, self.scrollView.frame.size.width, 60)];
        
         imageViewEditProfile = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineOne.frame.size.height+labelLineOne.frame.origin.y+10, 40, 40)];
      
        
        labelEditProfile = [[UILabel alloc]initWithFrame:CGRectMake(90, labelLineOne.frame.size.height+labelLineOne.frame.origin.y+10, self.scrollView.frame.size.width-90, 40)];
        
        labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonEditProfile.frame.origin.y+buttonEditProfile.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        
        if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"FBLoginStatus"] isEqualToString:@"LoginFB"]) {
            buttonChangePassword = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y, self.scrollView.frame.size.width, 60)];
            
            imageViewChangePassword = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y+10, 40, 40)];
            
            
            labelChangePassword = [[UILabel alloc]initWithFrame:CGRectMake(90, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y+10, self.scrollView.frame.size.width-90, 40)];
            
            labelLineThree = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonChangePassword.frame.origin.y+buttonChangePassword.frame.size.height, self.scrollView.frame.size.width, 1)];
            
            buttonSignout = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineThree.frame.size.height+labelLineThree.frame.origin.y, self.scrollView.frame.size.width, 60)];
         
            
            //               labelRateUs = [[UILabel alloc]initWithFrame:CGRectMake((self.scrollView.frame.size.width-120)/2, buttonChangePassword.frame.size.height+buttonChangePassword.frame.origin.y+13, 120, 35)];
        }
        else
        {
            
            buttonSignout = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y, self.scrollView.frame.size.width, 60)];
            
            //                  labelRateUs = [[UILabel alloc]initWithFrame:CGRectMake((self.scrollView.frame.size.width-120)/2, buttonEditProfile.frame.size.height+buttonEditProfile.frame.origin.y+13, 120, 35)];
        }
        
        labelSignout = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonSignout.frame.origin.y+10, self.scrollView.frame.size.width-90, 40)];
        
        labelLineFour = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonSignout.frame.origin.y+buttonSignout.frame.size.height, self.scrollView.frame.size.width, 20)];
        
        
        imageViewSignout= [[UIImageView alloc] initWithFrame:CGRectMake(20, buttonSignout.frame.origin.y+10, 40, 40)];
        
        
        
        buttonRateUsOnAppStore = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineFour.frame.size.height+labelLineFour.frame.origin.y, self.scrollView.frame.size.width, 60)];
        
        labelRateUsOnAppStore = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonRateUsOnAppStore.frame.origin.y+10, self.scrollView.frame.size.width-90, 40)];
        imageViewRateUs = [[UIImageView alloc] initWithFrame:CGRectMake(20, buttonRateUsOnAppStore.frame.origin.y+10, 40, 40)];
        
        
        labelLineFive = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonRateUsOnAppStore.frame.origin.y+buttonRateUsOnAppStore.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        
        buttonPrivacyPolicy = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineFive.frame.size.height+labelLineFive.frame.origin.y, self.scrollView.frame.size.width, 60)];
        
        labelPrivacyPolicy = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonPrivacyPolicy.frame.origin.y+10, self.scrollView.frame.size.width-90, 40)];
        
        imageViewPrivacyPolicy = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineFive.frame.size.height+labelLineFive.frame.origin.y+10, 40, 40)];
      
        
        labelLineSix = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonPrivacyPolicy.frame.origin.y+buttonPrivacyPolicy.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        
        buttonAbout = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineSix.frame.size.height+labelLineSix.frame.origin.y, self.scrollView.frame.size.width, 60)];
          imageViewAbout = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineSix.frame.size.height+labelLineSix.frame.origin.y+10, 40, 40)];
        
        labelAbout = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonAbout.frame.origin.y+10, self.scrollView.frame.size.width-90, 40)];
        
        labelLineSeven = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonAbout.frame.origin.y+buttonAbout.frame.size.height, self.scrollView.frame.size.width, self.view.frame.size.height-(labelAbout.frame.size.height+labelAbout.frame.origin.y))];

    }
    else
    {
      
        
         imageViewUser = [[UIImageView alloc]initWithFrame:CGRectMake((self.scrollView.frame.size.width-90)/2, 30, 90, 90)];
        labelUserName = [[UILabel alloc]initWithFrame:CGRectMake(0, imageViewUser.frame.origin.y+imageViewUser.frame.size.height, self.scrollView.frame.size.width, 29)];
       
        
        
       labelBackground1 =  [[UILabel alloc]initWithFrame:CGRectMake(0, labelUserName.frame.size.height+labelUserName.frame.origin.y+10, self.scrollView.frame.size.width, 60)];
        labelMuteNotification = [[UILabel alloc]initWithFrame:CGRectMake(10, labelBackground1.frame.origin.y+15, self.scrollView.frame.size.width-10, 30)];
        labelUserNameBackgound = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, labelMuteNotification.frame.origin.y)];
        switchForNotification = [[UISwitch alloc]initWithFrame:CGRectMake(176, labelBackground1.frame.origin.y+15, 49, 31)];
        
        labelLineOne = [[UILabel alloc]initWithFrame:CGRectMake(0, labelBackground1.frame.origin.y+labelBackground1.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        buttonEditProfile =  [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineOne.frame.size.height+labelLineOne.frame.origin.y, self.scrollView.frame.size.width, 50)];
        
        labelEditProfile = [[UILabel alloc]initWithFrame:CGRectMake(90, labelLineOne.frame.size.height+labelLineOne.frame.origin.y+10, self.scrollView.frame.size.width-90, 30)];
        
        
        
        imageViewEditProfile = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineOne.frame.size.height+labelLineOne.frame.origin.y+10, 30, 30)];
        
        
        labelLineTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonEditProfile.frame.origin.y+buttonEditProfile.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        
           if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"FBLoginStatus"] isEqualToString:@"LoginFB"]) {
               buttonChangePassword = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y, self.scrollView.frame.size.width, 50)];
               
               imageViewChangePassword = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y+10, 30, 30)];
               
               labelChangePassword = [[UILabel alloc]initWithFrame:CGRectMake(90, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y+10, self.scrollView.frame.size.width-90, 30)];
               
               labelLineThree = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonChangePassword.frame.origin.y+buttonChangePassword.frame.size.height, self.scrollView.frame.size.width, 1)];
               
                   buttonSignout = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineThree.frame.size.height+labelLineThree.frame.origin.y, self.scrollView.frame.size.width, 50)];
               
//               labelRateUs = [[UILabel alloc]initWithFrame:CGRectMake((self.scrollView.frame.size.width-120)/2, buttonChangePassword.frame.size.height+buttonChangePassword.frame.origin.y+13, 120, 35)];
           }
        else
        {
            
            buttonSignout = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineTwo.frame.size.height+labelLineTwo.frame.origin.y, self.scrollView.frame.size.width, 50)];

//                  labelRateUs = [[UILabel alloc]initWithFrame:CGRectMake((self.scrollView.frame.size.width-120)/2, buttonEditProfile.frame.size.height+buttonEditProfile.frame.origin.y+13, 120, 35)];
        }
        
        labelSignout = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonSignout.frame.origin.y+10, self.scrollView.frame.size.width-90, 30)];
        
        labelLineFour = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonSignout.frame.origin.y+buttonSignout.frame.size.height, self.scrollView.frame.size.width, 20)];
        
        imageViewSignout= [[UIImageView alloc] initWithFrame:CGRectMake(20, buttonSignout.frame.origin.y+10, 30, 30)];
        
        
        
        buttonRateUsOnAppStore = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineFour.frame.size.height+labelLineFour.frame.origin.y, self.scrollView.frame.size.width, 50)];
        
        
        labelRateUsOnAppStore = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonRateUsOnAppStore.frame.origin.y+10, self.scrollView.frame.size.width-90, 30)];
        
        labelLineFive = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonRateUsOnAppStore.frame.origin.y+buttonRateUsOnAppStore.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        imageViewRateUs = [[UIImageView alloc] initWithFrame:CGRectMake(20, buttonRateUsOnAppStore.frame.origin.y+10, 30, 30)];
        
        
         buttonPrivacyPolicy = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineFive.frame.size.height+labelLineFive.frame.origin.y, self.scrollView.frame.size.width, 50)];
        
        labelPrivacyPolicy = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonPrivacyPolicy.frame.origin.y+10, self.scrollView.frame.size.width-90, 30)];
        
        imageViewPrivacyPolicy = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineFive.frame.size.height+labelLineFive.frame.origin.y+10, 30, 30)];
        
        
        labelLineSix = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonPrivacyPolicy.frame.origin.y+buttonPrivacyPolicy.frame.size.height, self.scrollView.frame.size.width, 1)];
        
        
         buttonAbout = [[UIButton alloc]initWithFrame:CGRectMake(0, labelLineSix.frame.size.height+labelLineSix.frame.origin.y, self.scrollView.frame.size.width, 50)];
        
        labelAbout = [[UILabel alloc]initWithFrame:CGRectMake(90, buttonAbout.frame.origin.y+10, self.scrollView.frame.size.width-90, 30)];
        
        imageViewAbout = [[UIImageView alloc] initWithFrame:CGRectMake(20, labelLineSix.frame.size.height+labelLineSix.frame.origin.y+10, 30, 30)];
        labelLineSeven = [[UILabel alloc]initWithFrame:CGRectMake(0, buttonAbout.frame.origin.y+buttonAbout.frame.size.height, self.scrollView.frame.size.width, self.view.frame.size.height-(labelAbout.frame.size.height+labelAbout.frame.origin.y))];
        
    
    }
    
    
    labelUserNameBackgound.backgroundColor = colorBackground;
    
    [contentView addSubview:labelUserNameBackgound];
    
  
    imageViewUser.layer.cornerRadius = imageViewUser.frame.size.height/2;
    imageViewUser.layer.masksToBounds = YES;
    imageViewUser.image = [self loadImage];
      if (imageViewUser.image == nil) {

        imageViewUser.image = [UIImage imageNamed:@"f3.png"];
    }
        [contentView addSubview:imageViewUser];
    
        labelUserName.text = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"],[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"]];
    labelUserName.backgroundColor = [UIColor clearColor];
    labelUserName.font = [UIFont systemFontOfSize:15.0];
    labelUserName.textAlignment = NSTextAlignmentCenter;
    labelUserName.textColor = [UIColor whiteColor];
    [contentView addSubview:labelUserName];
    
  
    labelBackground1.text = @"";
    labelBackground1.backgroundColor = buttonBackgroundColor;
    [contentView addSubview:labelBackground1];
    
    
    labelMuteNotification.text = @"  Mute Notification";
    labelMuteNotification.backgroundColor = [UIColor clearColor];
    labelMuteNotification.textColor = [UIColor whiteColor];
     labelMuteNotification.font = [UIFont systemFontOfSize:15.0];
    [contentView addSubview:labelMuteNotification];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"notificatio_status"]);
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"notificatio_status"] isEqualToString:@"on"]) {
        switchForNotification.on = YES;
    }
    else
    {
        switchForNotification.on = NO;
    }

    [switchForNotification addTarget:self action:@selector(switchNotificationAction:) forControlEvents:UIControlEventValueChanged];
    
    [contentView addSubview:switchForNotification];
    
    
    

    
    labelLineOne.text = @"";
    labelLineOne.backgroundColor = colorBackground;
    [contentView addSubview:labelLineOne];
    
    
    
    
    [buttonEditProfile addTarget:self action:@selector(EditProfile:) forControlEvents:UIControlEventTouchUpInside];
    [buttonEditProfile setBackgroundColor:buttonBackgroundColor];
    [contentView addSubview:buttonEditProfile];
    
    
    labelEditProfile.text = @"Edit Profile";
    labelEditProfile.backgroundColor = [UIColor clearColor];
    labelEditProfile.font = [UIFont systemFontOfSize:15.0];
    labelEditProfile.textAlignment = NSTextAlignmentLeft;
    labelEditProfile.textColor = [UIColor whiteColor];
    [contentView addSubview:labelEditProfile];
    
    labelLineTwo.text = @"";
    labelLineTwo.backgroundColor = colorBackground;
    [contentView addSubview:labelLineTwo];
    
    
     if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"FBLoginStatus"] isEqualToString:@"LoginFB"]) {

        
        
         
         
         
         
         [buttonChangePassword addTarget:self action:@selector(buttonChangePass:) forControlEvents:UIControlEventTouchUpInside];
         [buttonChangePassword setBackgroundColor:buttonBackgroundColor];
         [contentView addSubview:buttonChangePassword];
         
         
         labelChangePassword.text = @"Change password";
         labelChangePassword.backgroundColor = [UIColor clearColor];
         labelChangePassword.font = [UIFont systemFontOfSize:15.0];
         labelChangePassword.textAlignment = NSTextAlignmentLeft;
         labelChangePassword.textColor = [UIColor whiteColor];
         [contentView addSubview:labelChangePassword];
       
         labelLineThree.text = @"";
         labelLineThree.backgroundColor = colorBackground;
         [contentView addSubview:labelLineThree];
       
          [contentView addSubview:imageViewChangePassword];
          imageViewChangePassword.image = [UIImage imageNamed:@"ChangePass.png"];
//         labelRateUs.text = @"Rate Us";
//         labelRateUs.layer.cornerRadius = 12.0f; // this value vary as per your desire
//         labelRateUs.layer.masksToBounds = YES;
//         labelRateUs.textAlignment = NSTextAlignmentCenter;
//         labelRateUs.layer.borderColor=[[UIColor lightGrayColor]CGColor];
//         labelRateUs.layer.borderWidth= 1.0f;
//         labelRateUs.font = [UIFont systemFontOfSize:15.0];
//         labelRateUs.backgroundColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
//         labelRateUs.textColor = [UIColor whiteColor];
//         [contentView addSubview:labelRateUs];
     }
    else
    {
  
//        labelRateUs.text = @"Rate Us";
//        labelRateUs.layer.cornerRadius = 12.0f; // this value vary as per your desire
//        labelRateUs.layer.masksToBounds = YES;
//        labelRateUs.textAlignment = NSTextAlignmentCenter;
//        labelRateUs.layer.borderColor=[[UIColor lightGrayColor]CGColor];
//        labelRateUs.layer.borderWidth= 1.0f;
//        labelRateUs.font = [UIFont systemFontOfSize:15.0];
//        labelRateUs.backgroundColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
//        labelRateUs.textColor = [UIColor whiteColor];
//        [contentView addSubview:labelRateUs];
        
//        labelLineTwo.text = @"";
//        labelLineTwo.backgroundColor = [UIColor blackColor];
//        [contentView addSubview:labelLineTwo];
    }
    
    
    
    

    
    [buttonSignout addTarget:self action:@selector(SignOutUser:) forControlEvents:UIControlEventTouchUpInside];
    [buttonSignout setBackgroundColor:buttonBackgroundColor];
    [contentView addSubview:buttonSignout];
    
    labelSignout.text = @"Sign Out";
    labelSignout.backgroundColor = [UIColor clearColor];
    labelSignout.font = [UIFont systemFontOfSize:15.0];
    labelSignout.textAlignment = NSTextAlignmentLeft;
    labelSignout.textColor = [UIColor whiteColor];
    [contentView addSubview:labelSignout];
    
    labelLineFour.text = @"";
    labelLineFour.backgroundColor = colorBackground;
    [contentView addSubview:labelLineFour];
    
    
    
    
    
      [buttonRateUsOnAppStore addTarget:self action:@selector(OpenSocialWeb:) forControlEvents:UIControlEventTouchUpInside];
    [buttonRateUsOnAppStore setBackgroundColor:buttonBackgroundColor];
    [contentView addSubview:buttonRateUsOnAppStore];
    
    labelRateUsOnAppStore.text = @"Rate us on Appstore";
    labelRateUsOnAppStore.backgroundColor = [UIColor clearColor];
    labelRateUsOnAppStore.font = [UIFont systemFontOfSize:15.0];
    labelRateUsOnAppStore.textAlignment = NSTextAlignmentLeft;
    labelRateUsOnAppStore.textColor = [UIColor whiteColor];
    [contentView addSubview:labelRateUsOnAppStore];
    
    labelLineFive.text = @"";
    labelLineFive.backgroundColor = colorBackground;
    [contentView addSubview:labelLineFive];
    

    

    [buttonPrivacyPolicy addTarget:self action:@selector(PrivacyandPolicy:) forControlEvents:UIControlEventTouchUpInside];
    [buttonPrivacyPolicy setBackgroundColor:buttonBackgroundColor];
    [contentView addSubview:buttonPrivacyPolicy];
    
   
    labelPrivacyPolicy.text = @"Privacy Policy";
    labelPrivacyPolicy.backgroundColor = [UIColor clearColor];
    labelPrivacyPolicy.font = [UIFont systemFontOfSize:15.0];
    labelPrivacyPolicy.textAlignment = NSTextAlignmentLeft;
    labelPrivacyPolicy.textColor = [UIColor whiteColor];
    [contentView addSubview:labelPrivacyPolicy];
    
   
    
    
    
    

      [buttonAbout addTarget:self action:@selector(OpenSocialWeb:) forControlEvents:UIControlEventTouchUpInside];
    [buttonAbout setBackgroundColor:buttonBackgroundColor];
    [contentView addSubview:buttonAbout];
    
    
    
    labelLineSeven.text = @"";
    labelLineSeven.backgroundColor = buttonBackgroundColor;
    [contentView addSubview:labelLineSeven];
    
    labelAbout.text = @"About";
    labelAbout.backgroundColor = [UIColor clearColor];
    labelAbout.font = [UIFont systemFontOfSize:15.0];
    labelAbout.textAlignment = NSTextAlignmentLeft;
    labelAbout.textColor = [UIColor whiteColor];
    [contentView addSubview:labelAbout];
    labelLineSix.text = @"";
    labelLineSix.backgroundColor = colorBackground
    ;
    [contentView addSubview:labelLineSix];
    
    
    imageViewEditProfile.image = [UIImage imageNamed:@"editprofile.png"];
  
    imageViewSignout.image = [UIImage imageNamed:@"Signout"];
    imageViewRateUs.image = [UIImage imageNamed:@"rateUs.png"];
    imageViewPrivacyPolicy.image = [UIImage imageNamed:@"Privacy.png"];
    imageViewAbout.image = [UIImage imageNamed:@"About.png"];
    
     [contentView addSubview:imageViewEditProfile];
    
     [contentView addSubview:imageViewSignout];
     [contentView addSubview:imageViewRateUs];
     [contentView addSubview:imageViewPrivacyPolicy];
     [contentView addSubview:imageViewAbout];
    
   
   
    

    
    
    contentView.frame = CGRectMake(0, 0,self.view.frame.size.width ,labelLineSeven.frame.origin.y+labelLineSeven.frame.size.height);
    [_scrollView addSubview:contentView];
    
    // dataTableView.frame = CGRectMake(0, 0, 320, heightTableView);
    
    _scrollView.contentSize =  CGSizeMake(_scrollView.frame.size.width, labelLineSeven.frame.origin.y+labelLineSeven.frame.size.height);
    
    
    
    
    
}



- (UIImage*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    image = [UIImage imageWithContentsOfFile:path];
    return image;
}

//- (UIImage*)loadImage:(NSString*)imageName
//{
//        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        
//        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@test.png", imageName]];
//        
//        return [UIImage imageWithContentsOfFile:fullPath];
//        
//}




- (IBAction)buttonChangePass:(id)sender
{
    [self performSegueWithIdentifier:@"changePass" sender:self];
}


- (IBAction)SignIn:(id)sender
{
    
     [self performSegueWithIdentifier:@"login" sender:self];
     
    
}
- (IBAction)About:(id)sender
{
          [self performSegueWithIdentifier:@"about" sender:self];
    
    
}
- (IBAction)Help:(id)sender
{
    
    
    [self performSegueWithIdentifier:@"help" sender:self];

    
}

- (IBAction)PrivacyandPolicy:(id)sender
{
    [self performSegueWithIdentifier:@"privacy" sender:self];

    
}

- (IBAction)TermsofUse:(id)sender
{
    
    
    [self performSegueWithIdentifier:@"term" sender:self];

    
    
}




-(void)viewWillDisappear:(BOOL)animated
{



    self.navigationController.navigationBar.backgroundColor = [UIColor  colorWithRed:127.0 green:127.0 blue:127.0 alpha:1];

}



// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue isKindOfClass: [ViewControllerSegue class]])
    {
        ViewControllerSegue *swSegue = (ViewControllerSegue*)segue;
        
        swSegue.performBlock = ^(ViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
        {
            UINavigationController* navController = (UINavigationController*)self.ViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.ViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
}

- (IBAction)SignOutUser:(id)sender
{
    

   
    _viewForUnregisterUser.hidden =NO;
   //// checkBoxStatus
    //notificationCount
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"notificationCount"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"checkBoxStatus"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FBLoginStatus"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"MainLocation"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"HouseName"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"StreetName"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"notificatio_status"];
    //notificatio_status
    [[NSUserDefaults standardUserDefaults]setObject:@"signout" forKey:@"LoginStatus"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"LoginUserId"];
    [self performSegueWithIdentifier:@"mainmenu" sender:self];

    
  
}


- (IBAction)SignUpForTheUser:(id)sender
{


    
    [self performSegueWithIdentifier:@"sign" sender:self];
    
    
}

- (IBAction)EditProfile:(id)sender
{
    
    
    
    [self performSegueWithIdentifier:@"editprofile" sender:self];
    
    
    
}



- (IBAction)OpenSocialWeb:(id)sender
{
     strOfSocialNavigationTitle = @"Facebook";
        [self performSegueWithIdentifier:@"web" sender:self];
    
        
    
        
        
   
}
- (IBAction)switchNotificationAction:(id)sender
{
    if([sender isOn]){
        
        strOfNotificationStatus = @"on";
        
        dispatch_async(kBgQueue, ^{
            [self notificationSetting];
           
        });
        
//        NSLog(@"Switch is ON");
    } else{
//        NSLog(@"Switch is OFF");
        strOfNotificationStatus = @"off";
        dispatch_async(kBgQueue, ^{
            [self notificationSetting];
            
        });
    }

}


-(void) notificationSetting
{
    //http://whatscene.eventbuoy.com/dev/admin/admin/deletenotification.php?userid=44&eventid=421&notificationid=107
    
    
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/notificationstatus.php?userid=%@&status=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfNotificationStatus];
    //  strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
            
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                NSLog(@"Delete notification");
                [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"] valueForKey:@"status"] objectAtIndex:0]forKey:@"notificatio_status"];
                
            }
            else
            {
                
            }
            
            
            
            
        }
    }
    
    
}

@end
