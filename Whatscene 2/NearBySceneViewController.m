//
//  NearBySceneViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 09/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#import "AsyncImageView.h"
#import "NearBySceneViewController.h"
#import "Anno.h"
#import <MapKit/MapKit.h>
#import "NearBySceneTableViewCell.h"
#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface NearBySceneViewController ()<MKMapViewDelegate,MKAnnotation,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>
{
    NSMutableArray * arrayofData;
  
    Anno *ann;
    NSMutableArray *arrOfEventDateAndTime,*arrOfEventDescription,*arrayOfEventPlace,*arrayOfEventType,*arrayOfEventImage,*arrayOfEventId,*arrOfEventLatitude,*arrOfEventLongitude,*arrOfEventName,*arrayOfEventOwnerId;
    
    NSString *strofChangeFrame;

    
    UIDatePicker * datepicker;
    NSMutableArray * randomArrayOfEventName;
  
    NSMutableArray * randomArrayOfEventDateAndTime,*randomArrayOfEventDescription,*randomArrayOfEventPlace,*randomArrayOfEventType,*randomArrayOfEventImage,*randomArrayOfEventId,*randomArrayOfEventLatitude,*randomArrayOfEventLongitude,*randomArrayOfEventOwnerId;
    NSString * strForTitle;
    int value ;
    BOOL rightClickEvent;
   
        UITableView *_tableViewObjectForNearByScene;
    
}
@property (strong, nonatomic) IBOutlet UIButton *buttonRightHide;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@property(nonatomic,retain) IBOutlet  MKMapView *map;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewForNearByScene;

@property (weak, nonatomic) IBOutlet UIView *viewForNearByScene;

@property (strong, nonatomic) IBOutlet UIButton *buttonNearbyScene;
@property (strong, nonatomic) IBOutlet UIButton *buttonlogin;
@property(nonatomic, retain) CLLocationManager *locationManager;



@end


NSString *strOfEventImage,*strOfEventDateAndTime,*strOfEventDescription,*strOfEventName,*strOfEventPlace,*strOfEventType,*strOfEventId,*strOfCreateNewSceneFlag,*strOfEventOwnerId,*strOfSceneName;
CGFloat eventLatitude,eventlongitude;

UIColor *colorForViewScene;
extern CGFloat currentLongitude,currentLatitude;


NSString *strOfCreateNewSceneFlag;
@implementation NearBySceneViewController

- (void)viewDidLoad
{
    _activityIndicator.hidden = YES;
    //  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    [super viewDidLoad];
    [self navigationBar];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

   
    [[NSUserDefaults standardUserDefaults]setObject:@"nearby" forKey:@"StoryBoardId"];
   
    
    strOfSceneName = @"NearByScene";
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]) {
        _buttonlogin.hidden = YES;
    }
    _buttonRightHide.hidden = YES;
    [_buttonRightHide addTarget:self action:@selector(DrawerMenuAction) forControlEvents:UIControlEventTouchUpInside];
   
    
    
    rightClickEvent  = YES;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
          _map = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-200-64-5)];
         _tableViewObjectForNearByScene = [[UITableView alloc]initWithFrame:CGRectMake(0, _map.frame.size.height+_map.frame.origin.y+5, self.view.frame.size.width, 200)];
    }
    else
    {
         _map = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-136-64-5)];
         _tableViewObjectForNearByScene = [[UITableView alloc]initWithFrame:CGRectMake(0, _map.frame.size.height+_map.frame.origin.y+5, self.view.frame.size.width, 136)];
    }
  
    _map.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
#endif
    [self.locationManager startUpdatingLocation];
    
    _map.showsUserLocation = YES;
   // [_map setMapType:MKMapTypeStandard];
    [_map setZoomEnabled:YES];
    [_map setScrollEnabled:YES];
    _tableViewObjectForNearByScene.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableViewObjectForNearByScene.backgroundColor = [UIColor clearColor];
    _tableViewObjectForNearByScene.delegate = self;
    _tableViewObjectForNearByScene.dataSource = self;
    [_map userLocation ];
    
    [self.view addSubview:_map];
    [self.view addSubview:_tableViewObjectForNearByScene];
    [self.view addSubview:_buttonlogin];
    [self.view addSubview:_buttonRightHide];
     [self.view addSubview:_buttonNearbyScene];
    
    
    colorForViewScene = [UIColor colorWithRed:91.0/255.0 green:61.0/255.0 blue:86.0/255.0 alpha:1.0];
    self.view.backgroundColor = colorForViewScene;
     [self.navigationController.navigationBar setBarTintColor:colorForViewScene];
    randomArrayOfEventName = [[NSMutableArray alloc]init];
    randomArrayOfEventPlace = [[NSMutableArray alloc]init];
    randomArrayOfEventDateAndTime = [[NSMutableArray alloc]init];
    randomArrayOfEventDescription = [[NSMutableArray alloc]init];
    randomArrayOfEventType = [[NSMutableArray alloc]init];
    randomArrayOfEventImage = [[NSMutableArray alloc]init];
    randomArrayOfEventId = [[NSMutableArray alloc]init];
    randomArrayOfEventLatitude = [[NSMutableArray alloc]init];
    randomArrayOfEventLongitude = [[NSMutableArray alloc]init];
     randomArrayOfEventOwnerId = [[NSMutableArray alloc]init];
    
    [_buttonNearbyScene setBackgroundImage:[UIImage imageNamed:@"NearbylistView.PNG"] forState:UIControlStateNormal];
    
    strofChangeFrame = @"No";
    self.view.backgroundColor = colorForViewScene;

    
    NSLog(@"%@",arrayofData);
    NSLog(@"%@",arrOfEventName);
    
    _tableViewObjectForNearByScene.scrollEnabled = NO;
    [_map setCenterCoordinate:_map.userLocation.location.coordinate animated:YES];
    _map.showsUserLocation = YES;
    
    
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    if (netStatus == NotReachable)
    {
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        dispatch_async(kBgQueue, ^{
            
            [self getWebServiceForNearByLocation];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (arrOfEventName.count != 0) {
                    [self getMapPin];
                    [randomArrayOfEventName addObject:[arrOfEventName objectAtIndex:0]];
                    [randomArrayOfEventDateAndTime addObject:[arrOfEventDateAndTime objectAtIndex:0]];
                    [randomArrayOfEventPlace addObject:[arrayOfEventPlace objectAtIndex:0]];
                    [randomArrayOfEventDescription addObject:[arrOfEventDescription objectAtIndex:0]];
                    [randomArrayOfEventType addObject:[arrayOfEventType objectAtIndex:0]];
                    [randomArrayOfEventImage addObject:[arrayOfEventImage objectAtIndex:0]];
                    [randomArrayOfEventId addObject:[arrayOfEventId objectAtIndex:0]];
                    [randomArrayOfEventLatitude addObject:[arrOfEventLatitude objectAtIndex:0]];
                    [randomArrayOfEventLongitude addObject:[arrOfEventLongitude objectAtIndex:0]];
                    [randomArrayOfEventOwnerId addObject:[arrayOfEventOwnerId objectAtIndex:0]];
                    [_tableViewObjectForNearByScene reloadData];
                    
                }
                else
                {
                    _buttonNearbyScene.hidden = YES;
                    _tableViewObjectForNearByScene.hidden = YES;
                    _map.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                    

                }
            });
            
        });
    }
    
   
    

 
    
    
    _scrollViewForNearByScene.translatesAutoresizingMaskIntoConstraints = NO;
   
    
    
}
-(void) navigationBar
{
    UIButton * loginButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [loginButton setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(RightMenuDrawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *loginButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:loginButtonItem, nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
#endif
    [self.locationManager startUpdatingLocation];
    NSLog(@"%@", [self deviceLocation]);
    
    //View Area
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = self.locationManager.location.coordinate.latitude;
    region.center.longitude = self.locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    [_map setRegion:region animated:YES];
    
}



- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceLat {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceAlt {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.altitude];
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000);
    [_map setRegion:[_map regionThatFits:region] animated:YES];
}

-(void) getMapPin
{
    MKCoordinateRegion Region;
    
    for (int i=0; i<[arrOfEventLatitude count]; i++)
    {
        
        Region.center.latitude=[[arrOfEventLatitude objectAtIndex:i]floatValue];
        Region.center.longitude=[[arrOfEventLongitude objectAtIndex:i]floatValue];
        Region.span.latitudeDelta=0.15;
        Region.span.longitudeDelta=0.15;
        [_map setRegion:Region];
        
        ann =[[Anno alloc]init];
        ann.title =[arrOfEventLatitude objectAtIndex:i];
        //  ann.subtitle =[city objectAtIndex:i];
        ann.coordinate = Region.center;
        
        [_map addAnnotation:ann];
        
    }

}





-(void)getWebServiceForNearByLocation
{
    
    
   
    
    _map.delegate = self;
    
    arrOfEventLatitude =[[NSMutableArray alloc]init];
    arrOfEventLongitude =[[NSMutableArray alloc]init];
    arrOfEventDateAndTime = [[NSMutableArray alloc]init];
    arrOfEventName =[[NSMutableArray alloc]init];
    arrayOfEventPlace =[[NSMutableArray alloc]init];
    arrOfEventDescription =[[NSMutableArray alloc]init];
    arrayOfEventType =[[NSMutableArray alloc]init];
    arrayOfEventId = [[NSMutableArray alloc]init];
    arrayOfEventImage =[[NSMutableArray alloc]init];
    arrayOfEventOwnerId =[[NSMutableArray alloc]init];
    NSString *strOfUserStoredId;
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]) {
        strOfUserStoredId = @"";
    }
    else
    {
        strOfUserStoredId = [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"];
    }
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/getnearbyevents.php?user_id=%@&lat=%f&long=%f",strOfUserStoredId,currentLatitude,currentLongitude];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
      if([[json valueForKey:@"success"] isEqualToString:@"true"])
      {
          
        
          
          NSArray *arr = [[json valueForKey:@"data"] valueForKey:@"event_name"];
          for (int i = 0; i<arr.count; i++)
          {
              [arrOfEventLatitude addObject:[[[json valueForKey:@"data"] valueForKey:@"event_latitude"] objectAtIndex:i]];
              [arrOfEventLongitude addObject:[[[json valueForKey:@"data"] valueForKey:@"event_longitude"] objectAtIndex:i]];
              [arrOfEventName addObject:[[[json valueForKey:@"data"] valueForKey:@"event_name"] objectAtIndex:i]];
              
              NSString *strOfEventDateAndTime = [NSString stringWithFormat:@"%@ | %@",[[[json valueForKey:@"data"] valueForKey:@"event_time"] objectAtIndex:i],[[[json valueForKey:@"data"] valueForKey:@"event_date"] objectAtIndex:i]];
              [arrOfEventDateAndTime addObject:strOfEventDateAndTime];
              
              [arrOfEventDescription addObject:[[[json valueForKey:@"data"] valueForKey:@"event_description"] objectAtIndex:i]];
              [arrayOfEventType addObject:[[[json valueForKey:@"data"] valueForKey:@"event_category"] objectAtIndex:i]];
              [arrayOfEventPlace addObject:[[[json valueForKey:@"data"] valueForKey:@"event_address"] objectAtIndex:i]];
              [arrayOfEventId addObject:[[[json valueForKey:@"data"] valueForKey:@"event_id"] objectAtIndex:i]];
              [arrayOfEventImage addObject:[[[json valueForKey:@"data"] valueForKey:@"event_image_url"] objectAtIndex:i]];
              [arrayOfEventOwnerId addObject:[[[json valueForKey:@"data"] valueForKey:@"owner_id"] objectAtIndex:i]];
              
              
          }
         
      }
    
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_activityIndicator stopAnimating];
                _activityIndicator.hidden = YES;
          
                
            });
            
            NSLog(@"%@",arrOfEventLatitude);
           
            
        }
     
    }
    

}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (mapView != _map || [annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    static NSString *annotationIdentifier = @"SPGooglePlacesAutocompleteAnnotation";
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[_map dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier] ;
    }
    annotationView.animatesDrop = YES;
    annotationView.canShowCallout = NO;
    
    return annotationView;
    
    
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    MKPointAnnotation *point = view.annotation;
    
    point= [[mapView selectedAnnotations] objectAtIndex:0];
    
    
    strForTitle = [NSString stringWithFormat:@"%@",point.title];
    
    
    NSLog(@"%@",strForTitle);
    [self searchbyarray];
    
    
    
    
    
}
-(void)searchbyarray
{
   
    
    if ([arrOfEventLatitude containsObject:strForTitle]) {
         NSInteger i =[arrOfEventLatitude indexOfObject:strForTitle];
        NSLog(@"%i",i);
        
      
        randomArrayOfEventName = [[NSMutableArray alloc]init];
        randomArrayOfEventPlace = [[NSMutableArray alloc]init];
        randomArrayOfEventDateAndTime = [[NSMutableArray alloc]init];
        randomArrayOfEventDescription = [[NSMutableArray alloc]init];
        randomArrayOfEventType = [[NSMutableArray alloc]init];
        randomArrayOfEventImage = [[NSMutableArray alloc]init];
        randomArrayOfEventId = [[NSMutableArray alloc]init];
        randomArrayOfEventLatitude = [[NSMutableArray alloc]init];
        randomArrayOfEventLongitude = [[NSMutableArray alloc]init];
        randomArrayOfEventOwnerId = [[NSMutableArray alloc]init];
        
        [randomArrayOfEventName addObject:[arrOfEventName objectAtIndex:i]];
        [randomArrayOfEventDateAndTime addObject:[arrOfEventDateAndTime objectAtIndex:i]];
        [randomArrayOfEventPlace addObject:[arrayOfEventPlace objectAtIndex:i]];
        [randomArrayOfEventDescription addObject:[arrOfEventDescription objectAtIndex:i]];
        [randomArrayOfEventType addObject:[arrayOfEventType objectAtIndex:i]];
        [randomArrayOfEventImage addObject:[arrayOfEventImage objectAtIndex:i]];
        [randomArrayOfEventId addObject:[arrayOfEventId objectAtIndex:i]];
        [randomArrayOfEventLatitude addObject:[arrOfEventLatitude objectAtIndex:i]];
        [randomArrayOfEventLongitude addObject:[arrOfEventLongitude objectAtIndex:i]];
          [randomArrayOfEventOwnerId addObject:[arrayOfEventOwnerId objectAtIndex:i]];
        NSLog(@"%@",randomArrayOfEventName);
        [_tableViewObjectForNearByScene reloadData];
    }
   
    
    
    
    
    
    
    
}


- (IBAction)buttonListNearBySceen:(id)sender
{
    
    
    randomArrayOfEventName = [[NSMutableArray alloc]init];
    randomArrayOfEventPlace = [[NSMutableArray alloc]init];
    randomArrayOfEventDateAndTime = [[NSMutableArray alloc]init];
    randomArrayOfEventDescription = [[NSMutableArray alloc]init];
    randomArrayOfEventType = [[NSMutableArray alloc]init];
    randomArrayOfEventImage = [[NSMutableArray alloc]init];
    randomArrayOfEventId = [[NSMutableArray alloc]init];
    randomArrayOfEventLatitude = [[NSMutableArray alloc]init];
    randomArrayOfEventLongitude = [[NSMutableArray alloc]init];
    randomArrayOfEventOwnerId = [[NSMutableArray alloc]init];

    
    if ([strofChangeFrame isEqualToString:@"Yes"])
    {
        
        [UIView animateWithDuration:0.2 animations:^{
            
       _map.hidden = YES;
            _tableViewObjectForNearByScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-64, self.view.frame.size.width, self.view.frame.size.height);
            _tableViewObjectForNearByScene.scrollEnabled = YES;
            strofChangeFrame = @"No";
            
            
        }];
       
        [UIView commitAnimations];
    }
    else if([strofChangeFrame isEqualToString:@"No"])
    {
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
   
            [_tableViewObjectForNearByScene needsUpdateConstraints];
            [UIView animateWithDuration:0.2 animations:^{
                //            _viewForNearByScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-136, self.view.frame.size.width, 136);
                
       
                _map.hidden = NO;
                _tableViewObjectForNearByScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-200, self.view.frame.size.width, 200);
                _tableViewObjectForNearByScene.scrollEnabled = NO;
                strofChangeFrame = @"Yes";
                
            }];
            
            [UIView commitAnimations];
        }
        else{
         
         
            [_tableViewObjectForNearByScene needsUpdateConstraints];
            [UIView animateWithDuration:0.2 animations:^{
                //            _viewForNearByScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-136, self.view.frame.size.width, 136);
                
         _map.hidden = NO;
                _tableViewObjectForNearByScene.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height-136, self.view.frame.size.width, 136);
                _tableViewObjectForNearByScene.scrollEnabled = NO;
                strofChangeFrame = @"Yes";
                
            }];
            
            [UIView commitAnimations];
        }
        
    }
    
    NSInteger tableHeight = _tableViewObjectForNearByScene.frame.size.height;
    
    if (tableHeight == 136 || tableHeight == 200) {
        
        [randomArrayOfEventName addObject:[arrOfEventName objectAtIndex:0]];
        [randomArrayOfEventDateAndTime addObject:[arrOfEventDateAndTime objectAtIndex:0]];
        [randomArrayOfEventPlace addObject:[arrayOfEventPlace objectAtIndex:0]];
        [randomArrayOfEventDescription addObject:[arrOfEventDescription objectAtIndex:0]];
        [randomArrayOfEventType addObject:[arrayOfEventType objectAtIndex:0]];
        [randomArrayOfEventImage addObject:[arrayOfEventImage objectAtIndex:0]];
        [randomArrayOfEventId addObject:[arrayOfEventId objectAtIndex:0]];
        [randomArrayOfEventLatitude addObject:[arrOfEventLatitude objectAtIndex:0]];
        [randomArrayOfEventLongitude addObject:[arrOfEventLongitude objectAtIndex:0]];
        [randomArrayOfEventOwnerId addObject:[arrayOfEventOwnerId objectAtIndex:0]];
        
        [_buttonNearbyScene setBackgroundImage:[UIImage imageNamed:@"NearbylistView.PNG"] forState:UIControlStateNormal];
       
    }
    else
        
    {
        
        randomArrayOfEventName= [arrOfEventName mutableCopy];
        randomArrayOfEventPlace = [arrayOfEventPlace mutableCopy];
        randomArrayOfEventDateAndTime = [arrOfEventDateAndTime mutableCopy];
        randomArrayOfEventDescription= [arrOfEventDescription mutableCopy];
        randomArrayOfEventType = [arrayOfEventType mutableCopy];
        randomArrayOfEventImage = [arrayOfEventImage mutableCopy];
        randomArrayOfEventId= [arrayOfEventId mutableCopy];
        randomArrayOfEventLatitude = [arrOfEventLatitude mutableCopy];
        randomArrayOfEventLongitude = [arrOfEventLongitude mutableCopy];
        randomArrayOfEventOwnerId = [arrayOfEventOwnerId mutableCopy];
         [_buttonNearbyScene setBackgroundImage:[UIImage imageNamed:@"nearbyLocation.PNG"] forState:UIControlStateNormal];
    }
    
    
    
   
    
    [_tableViewObjectForNearByScene reloadInputViews];
    [_tableViewObjectForNearByScene reloadData];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 200.0;
    }
    else
    {
        return 136.0;
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [randomArrayOfEventName count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    UITableViewCell *cell=(UITableViewCell *)[_tableViewObjectForNearByScene dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    
    if (cell == nil)
        
    {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];;
        
    }
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
   // cell.contentView.backgroundColor = [UIColor clearColor];
    UILabel *labelTransparent;
    UILabel *labelEventName;
    UILabel *labelEventDateAndtime;
    UILabel *labelEventAdreess;
 
    AsyncImageView *imageViewEvent;
    UIImageView *image;
    
    
    

    cell.contentView.backgroundColor = colorForViewScene;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        image = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 50 , 50)];
        imageViewEvent = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 195)];
        labelTransparent = [[UILabel alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, 80)];
        labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(10, 120, self.view.frame.size.width-20, 30)];
        labelEventName.font = [UIFont boldSystemFontOfSize:25];
        labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 150, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess = [[UILabel alloc]initWithFrame:CGRectMake(10, 170, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess.font = [UIFont systemFontOfSize:15];

    }
    else
    {
        image = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 50 , 50)];
        labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, (self.view.frame.size.width-20)/2, 20)];
        labelEventDateAndtime = [[UILabel alloc]initWithFrame:CGRectMake(10, 96, (self.view.frame.size.width-20)/2, 20)];
        labelEventAdreess = [[UILabel alloc]initWithFrame:CGRectMake(10, 114, (self.view.frame.size.width-20)/2, 20)];
        labelTransparent = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 60)];
        imageViewEvent = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 131)];
           labelEventAdreess.font = [UIFont systemFontOfSize:12];
        

    }
    
    
    if ([[randomArrayOfEventType objectAtIndex:indexPath.row] isEqualToString:@"Private"]) {
        image.image = [UIImage imageNamed:@"private1.png"];
    }
    else
    {
        image.image = [UIImage imageNamed:@""];
    }
    
    
    NSLog(@"%@",[randomArrayOfEventImage objectAtIndex:indexPath.row]);
    imageViewEvent.imageURL = [NSURL URLWithString:[randomArrayOfEventImage objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:imageViewEvent];
    labelTransparent.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [cell.contentView addSubview:labelTransparent];
    labelEventName.textColor = [UIColor whiteColor];
    labelEventDateAndtime.textColor = [UIColor whiteColor];
    labelEventAdreess.textColor = [UIColor whiteColor];

   labelEventName.text = [randomArrayOfEventName objectAtIndex:indexPath.row];
    [cell.contentView addSubview:labelEventName];
    
    NSArray *arrOfDateAndTime = [[randomArrayOfEventDateAndTime objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
    
    NSLog(@"%@",arrOfDateAndTime);
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-dd-MM"]; //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date = [dateFormatter dateFromString: [arrOfDateAndTime objectAtIndex:1]]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];// here set format which you want...
    NSString *dateString = [dateFormatter stringFromDate:date];// here convert date in NSSt
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"HH:mm:ss";
    NSDate *date1 = [dateFormatter1 dateFromString:[arrOfDateAndTime objectAtIndex:0]];
    dateFormatter1.dateFormat = @"hh:mm a";
    NSString *strOfEventTime = [dateFormatter1 stringFromDate:date1];
    
    NSLog(@"Converted String : %@",strOfEventTime);
    
    NSDateFormatter *dateFormatForDay = [[NSDateFormatter alloc] init];
    
    [dateFormatForDay setDateFormat:@"EE"];
    NSString *strOfEventday = [dateFormatForDay stringFromDate:date];
    
    
    NSString *strOfDateAndTime = [NSString stringWithFormat:@"%@ %@ | %@",strOfEventday,strOfEventTime,dateString];
  
   
    labelEventDateAndtime.text = strOfDateAndTime;
     labelEventDateAndtime.numberOfLines = 1;
     labelEventDateAndtime.minimumFontSize = 8.;
     labelEventDateAndtime.adjustsFontSizeToFitWidth = YES;
     [cell.contentView addSubview:labelEventDateAndtime];
 
    labelEventAdreess.text = [randomArrayOfEventPlace objectAtIndex:indexPath.row];
         [cell.contentView addSubview:labelEventAdreess];
    [cell.contentView addSubview:image];
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    strOfCreateNewSceneFlag = @"YES";
    strOfEventImage = [randomArrayOfEventImage objectAtIndex:indexPath.row];
    strOfEventDateAndTime = [randomArrayOfEventDateAndTime objectAtIndex:indexPath.row];
    strOfEventDescription = [randomArrayOfEventDescription objectAtIndex:indexPath.row];
    strOfEventName = [randomArrayOfEventName objectAtIndex:indexPath.row];
    strOfEventPlace = [randomArrayOfEventPlace objectAtIndex:indexPath.row];
    strOfEventType = [randomArrayOfEventType objectAtIndex:indexPath.row];
    eventLatitude = [[randomArrayOfEventLatitude objectAtIndex:indexPath.row] floatValue];
    eventlongitude = [[randomArrayOfEventLongitude objectAtIndex:indexPath.row] floatValue];
    strOfEventId = [randomArrayOfEventId objectAtIndex:indexPath.row];
    strOfEventOwnerId = [randomArrayOfEventOwnerId objectAtIndex:indexPath.row];
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"viewEvent"];
    [self.navigationController pushViewController:viewController animated:NO];
}

-(void) viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.hidden = NO;
    
    
}



-(void) viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;

    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"PUREMCT.PNG"] forBarMetrics:UIBarMetricsDefault];
}

- (IBAction)backToHomeViewController:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
    [self.navigationController pushViewController:vc animated:NO];
    NSLog(@"done");
    
}


- (IBAction)CreateMyScene:(id)sender
{
    
    
 //  strOfSceneName = @"NearByScene";
//     strOfEventOwnerId = @"";
//    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Newscene"];
//    [self.navigationController pushViewController:vc animated:YES];
//    NSLog(@"done");
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:vc animated:YES];
    
}



- (IBAction)RightMenuDrawer:(id)sender
{
    if (rightClickEvent==YES) {
        _buttonRightHide.hidden = NO;
        rightClickEvent = NO;
    }
    else
    {
        _buttonRightHide.hidden = YES;
        rightClickEvent = YES;
    }
    
    [self.ViewController rightRevealToggle:nil];
    
}

-(void) DrawerMenuAction
{
    _buttonRightHide.hidden = YES;
    rightClickEvent=YES;
    [self.ViewController rightRevealToggle:nil];
}








@end
