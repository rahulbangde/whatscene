//
//  ButtonCorner.m
//  FBdemo
//
//  Created by Black Bean Engagement on 17/03/15.
//  Copyright (c) 2015 Black Bean. All rights reserved.
//

#import "ButtonCorner.h"

@implementation ButtonCorner


- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
}

@end
