//
//  InviteeUserViewController.m
//  Whatscene
//
//  Created by Black Bean Engagement on 10/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "InviteeUserViewController.h"
#import "InviteeUserTableViewCell.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface InviteeUserViewController ()<UIAlertViewDelegate>

{
    NSMutableArray *arrOfInviteeUser,*arrOfCheckBox;
    NSString *strOfReceiverId,*strOfCancelReceiverId;
    NSMutableDictionary *dictOfPreviousInviteeUser;
    NSString  *selectAll;
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *buttonDone;
@end
extern UIColor *colorForViewScene;
extern NSString *strOfEventId;
@implementation InviteeUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectAll = @"YES";
    arrOfCheckBox = [NSMutableArray new];
    dictOfPreviousInviteeUser = [NSMutableDictionary new];
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    // Do any additional setup after loading the view.
    self.view.backgroundColor = colorForViewScene;
    
    _activityIndicator.hidden = YES;
    
    dispatch_async(kBgQueue, ^{
        
        [self getInviteeFriend];
        dispatch_async(
                       dispatch_get_main_queue(), ^{
                           
                           arrOfInviteeUser =   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UploadedContacts"] mutableCopy];
                           NSLog(@"%@",arrOfInviteeUser);
                           for (int i=0; i<arrOfInviteeUser.count; i++) {
                               
                              
                                   [arrOfCheckBox addObject:@"UNCHECK.png"];
                               
                             
                               
                           }
                           [_tableViewInviteeUser reloadData];
                           
                           NSMutableSet* set1 = [NSMutableSet setWithArray:[arrOfInviteeUser valueForKey:@"userid"]];
                           NSMutableSet* set2 = [NSMutableSet setWithArray:[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"]];
                           [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
                           
                           NSArray* result = [set1 allObjects];
//                           if (result.count != 0) {
//                               arrOfCheckBox = [NSMutableArray new];
//                           }
                           for (int i =0; i<result.count; i++) {

                               
                            
                               
                              NSUInteger index = [[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] indexOfObject:[result objectAtIndex:i]];
                               if ([[[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"status"] objectAtIndex:index] isEqualToString:@"Cancel"]) {
                                   
                                   
                                   if ([[arrOfInviteeUser valueForKey:@"userid"] containsObject:[[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] objectAtIndex:index]]) {
                                       NSUInteger indexofObject = [[arrOfInviteeUser valueForKey:@"userid"] indexOfObject:[[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] objectAtIndex:index]];
                                       [arrOfCheckBox replaceObjectAtIndex:indexofObject withObject:@"UNCHECK.png"];
                                   }
                                  
                               }
                               else
                               {
                                   if ([[arrOfInviteeUser valueForKey:@"userid"] containsObject:[[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] objectAtIndex:index]]) {
                                        NSUInteger indexofObject = [[arrOfInviteeUser valueForKey:@"userid"] indexOfObject:[[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] objectAtIndex:index]];
                                           [arrOfCheckBox replaceObjectAtIndex:indexofObject withObject:@"chackbutton.png"];
                                   }
                               
                               }
                             
                               
                           }
                          
                         //  NSUInteger *index = [[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"];
                        
                           
                           [_tableViewInviteeUser reloadData];
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [_activityIndicator stopAnimating];
                               _activityIndicator.hidden = YES;
                               
                               
                           });
//
//                             NSLog(@"%@",result);
                           
                       });
        
    });
    
    

    
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)getInviteeFriend
{
    dispatch_async(dispatch_get_main_queue(), ^{
                            _activityIndicator.hidden = NO;
        [_activityIndicator startAnimating];
  
        
        
    });
    
        NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/getinvitedlist.php?eventid=%@&senderid=%@",strOfEventId,[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]];
        
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *error = nil;
        if (data !=nil ) {
             dictOfPreviousInviteeUser = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
            if (error) {
                NSLog(@"err---- %@",error.description);
            }
            else{
                NSLog(@"%@",dictOfPreviousInviteeUser);
               
            }
        }
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [arrOfInviteeUser count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    InviteeUserTableViewCell *cell=(InviteeUserTableViewCell *)[_tableViewInviteeUser dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.imageViewInvteeUser.imageURL = [NSURL URLWithString:[[arrOfInviteeUser objectAtIndex:indexPath.row] valueForKey:@"image"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.imageViewInvteeUser.image = [UIImage imageNamed:[[arrOfInviteeUser objectAtIndex:indexPath.row] valueForKey:@"image"]];
    if (cell.imageViewInvteeUser.image == nil) {
        cell.imageViewInvteeUser.image = [UIImage imageNamed:@"f3.png"];
    }
    cell.imageViewInvteeUser.layer.cornerRadius =  cell.imageViewInvteeUser.frame.size.height/2;
    cell.imageViewInvteeUser.layer.masksToBounds = YES;
    cell.labelInviteeName.text = [[arrOfInviteeUser objectAtIndex:indexPath.row] valueForKey:@"username"];
    [cell.buttonCheckInviteeStatus setBackgroundImage:[UIImage imageNamed:[arrOfCheckBox objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    cell.buttonCheckInviteeStatus.tag = indexPath.row;
    [cell.buttonCheckInviteeStatus addTarget:self action:@selector(checkBoxAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}

-(void) checkBoxAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    NSLog(@"%d",button.tag);
    if ([[arrOfCheckBox objectAtIndex:button.tag] isEqualToString:@"UNCHECK.png"]) {
        [arrOfCheckBox replaceObjectAtIndex:button.tag withObject:@"chackbutton.png"];
    }
    else if ([[arrOfCheckBox objectAtIndex:button.tag] isEqualToString:@"chackbutton.png"]) {
        [arrOfCheckBox replaceObjectAtIndex:button.tag withObject:@"UNCHECK.png"];
    }
    
    
    [_tableViewInviteeUser reloadData];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSLog(@"%d",indexPath.row);
    if ([[arrOfCheckBox objectAtIndex:indexPath.row] isEqualToString:@"UNCHECK.png"]) {
        [arrOfCheckBox replaceObjectAtIndex:indexPath.row withObject:@"chackbutton.png"];
    }
    else if ([[arrOfCheckBox objectAtIndex:indexPath.row] isEqualToString:@"chackbutton.png"]) {
        [arrOfCheckBox replaceObjectAtIndex:indexPath.row withObject:@"UNCHECK.png"];
    }
     [_tableViewInviteeUser reloadData];
}
- (IBAction)buttonSendInvitation:(id)sender
{
    
    NSMutableArray *arrOfInviteeReceiverId = [NSMutableArray new];
        NSMutableArray *arrOfCancelInviteeReceiverId = [NSMutableArray new];
    
    if ([arrOfCheckBox containsObject:@"chackbutton.png"]) {
        for (int i = 0; i<arrOfCheckBox.count; i++) {
            if ([[arrOfCheckBox objectAtIndex:i] isEqualToString:@"chackbutton.png"]) {
               
             [arrOfInviteeReceiverId addObject:   [[arrOfInviteeUser objectAtIndex:i] valueForKey:@"userid"]];
            }
        }
    }
    
    if ([arrOfCheckBox containsObject:@"UNCHECK.png"]) {
        for (int i = 0; i<arrOfCheckBox.count; i++) {
            if ([[arrOfCheckBox objectAtIndex:i] isEqualToString:@"UNCHECK.png"]) {
                
                
                if ([[[dictOfPreviousInviteeUser valueForKey:@"data"] valueForKey:@"receiverid"] containsObject:[[arrOfInviteeUser objectAtIndex:i] valueForKey:@"userid"]]) {
                     [arrOfCancelInviteeReceiverId addObject:   [[arrOfInviteeUser objectAtIndex:i] valueForKey:@"userid"]];
                }
               
                
                
            }
        }
    }
    
    
     strOfReceiverId = [arrOfInviteeReceiverId componentsJoinedByString:@","];
     strOfCancelReceiverId = [arrOfCancelInviteeReceiverId componentsJoinedByString:@","];
    _activityIndicator.hidden = NO;
    [_activityIndicator startAnimating];
    
    
    
    dispatch_async(kBgQueue, ^{
        _buttonDone.enabled= NO;
        [self sendInvitation];
             [self rejectInvitation];
    
        dispatch_async(
                       dispatch_get_main_queue(), ^{
                            _buttonDone.enabled= YES;
                           _activityIndicator.hidden = YES;
                           [_activityIndicator stopAnimating];
                           UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invitation send successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                           [alert show];
                    
                       });
        
    });
    
    
    
    
    
    
}


-(void) sendInvitation
{
   
    // http://whatscene.eventbuoy.com/dev/admin/admin/get_invite.php?eventid=386&senderid=247&receiverid=207,208,209,210,211,212
    
    if (![strOfReceiverId isEqualToString:@""]) {
        NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/get_invite.php?eventid=%@&senderid=%@&receiverid=%@",strOfEventId,[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfReceiverId];
        
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *error = nil;
        if (data !=nil ) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
            if (error) {
                NSLog(@"err---- %@",error.description);
            }
            else{
                NSLog(@"%@",json);
            }
        }
    }
    
    //http://whatscene.eventbuoy.com/dev/admin/admin/get_responsetinvite.php?eventid=386&senderid=247&receiverid=208&status=reject

}

-(void) rejectInvitation
{
    
    http://whatscene.eventbuoy.com/dev/admin/admin/get_rejectinvite.php?eventid=413&senderid=32&receiverid=6&status=Reject
    if (![strOfCancelReceiverId isEqualToString:@""] && [[dictOfPreviousInviteeUser valueForKey:@"data"] count]!=0) {
        NSString *  strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/get_rejectinvite.php?eventid=%@&senderid=%@&receiverid=%@&status=Reject",strOfEventId,[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],strOfCancelReceiverId];
        
        strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
        NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
        NSError *error = nil;
        if (data !=nil ) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
            if (error) {
                NSLog(@"err---- %@",error.description);
            }
            else{
                NSLog(@"%@",json);
            }
        }
    }
}
- (IBAction)buttonSelectAll:(id)sender
{
     arrOfCheckBox = [NSMutableArray new];
    if ([selectAll isEqualToString:@"YES" ]) {
       
        for (int i = 0; i<arrOfInviteeUser.count; i++) {
            [arrOfCheckBox addObject:@"chackbutton.png"];
        }
        selectAll = @"NO";
        [_tableViewInviteeUser reloadData];
    }
    else
    {
        for (int i = 0; i<arrOfInviteeUser.count; i++) {
            [arrOfCheckBox addObject:@"UNCHECK.png"];
        }
       selectAll = @"YES";
        [_tableViewInviteeUser reloadData];
    }
   
    
   
    
}

- (IBAction)buttonBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
