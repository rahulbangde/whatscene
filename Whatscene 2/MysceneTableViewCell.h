//
//  MysceneTableViewCell.h
//  Whatscene
//
//  Created by Alet Viegas on 09/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MysceneTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOfMyScene;
@property (strong, nonatomic) IBOutlet UILabel *labelForSceneName;
@property (strong, nonatomic) IBOutlet UILabel *labelForDate;
@property (strong, nonatomic) IBOutlet UIButton *buttonCommentForMyScene;
@property (strong, nonatomic) IBOutlet UIButton *buttonNotificationForMyScene;

@property (strong, nonatomic) IBOutlet UILabel *labelForTableviewcellImageTransperancy;

@property (strong, nonatomic) IBOutlet UILabel *labelForMySceneEventLocation;

@end
