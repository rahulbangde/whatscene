 //
//  LatestSceneViewcontroller.m
//  Whatscene
//
//  Created by Alet Viegas on 06/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#import "FLAnimatedImage.h"
#import "LatestSceneViewcontroller.h"
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "ViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface LatestSceneViewcontroller ()<ABPeoplePickerNavigationControllerDelegate,CLLocationManagerDelegate>
{
    
    NSMutableArray * arrayofData;
    NSMutableArray * arrofdataforwhatscenelabel;
    NSMutableArray* contactList;
    NSMutableDictionary *dOfPerson;
    NSMutableDictionary * mobilenumber;
    NSArray * keys ;
    NSArray * values;
    NSMutableArray * phonenumber;
    NSMutableArray * name;
    CLLocationManager *locationManager;
    
    NSMutableArray *arrOfContactList;
    
}




@property (strong, nonatomic) IBOutlet FLAnimatedImageView *ImageViewAnimation;

@property (strong, nonatomic) IBOutlet UILabel *labelForWhatScenetitle;

@property (strong, nonatomic) IBOutlet UIButton *barbutton;

@end
CGFloat currentLongitude,currentLatitude;
@implementation LatestSceneViewcontroller
- (IBAction)buttonDemo:(id)sender
{
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginStatus"] isEqualToString:@"Login"]) {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myscene"];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"addFriend" forKey:@"StoryBoardId"];
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"signup"];
        [self.navigationController pushViewController:vc animated:NO];
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    for (NSString *fontFamilyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:fontFamilyName]) {
            NSLog(@"Family: %@    Font: %@", fontFamilyName, fontName);
        }
    }
    
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Futura-MediumItalic" size:17],
      NSFontAttributeName, nil]];
   
    [CLLocationManager locationServicesEnabled];
    
   FLAnimatedImage *manWalkingImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"rock" ofType:@"gif"]]];
    self.ImageViewAnimation.animatedImage = manWalkingImage;
    
    
    [self.view addSubview:self.ImageViewAnimation];
    
    
    NSMutableArray *firstArray = [NSMutableArray arrayWithObjects:@"Bill", @"Ben", @"Chris", @"Melissa", nil];
    NSMutableArray *secondArray = [NSMutableArray arrayWithObjects:@"Bill", @"Paul", nil];
    
    NSMutableSet* set1 = [NSMutableSet setWithArray:firstArray];
    NSMutableSet* set2 = [NSMutableSet setWithArray:secondArray];
    [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
    
    NSArray* result = [set1 allObjects];
  
    NSLog(@"%@",result);

    locationManager = [[CLLocationManager alloc] init];
    arrOfContactList = [NSMutableArray new];
    
   self.navigationItem.hidesBackButton = YES;
   // _labelForWhatScenetitle.hidden = YES;
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    // Do any additional setup after loading the view, typically from a nib.
    // Do any additional setup after loading the view, typically from a nib.
    arrayofData = [[NSMutableArray alloc]initWithObjects:@"Latest Scenes",@"Nearby Scenes",@"My Scenes", nil];
    arrofdataforwhatscenelabel =[[NSMutableArray alloc]initWithObjects:@"WHATSCENE",@"",@"", nil];
    

     // [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"PUREMCT.PNG"] forBarMetrics:UIBarMetricsDefault];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"MainMenuView" forKey:@"StoryBoardId"];
    
    
    
}



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    NSLog(@"%@", [self deviceLocation]);
}

- (NSString *)deviceLocation {
    
    currentLatitude = locationManager.location.coordinate.latitude;
    currentLongitude = locationManager.location.coordinate.longitude;
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}

- (void)getCurrentLocation
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if(IS_OS_8_OR_LATER) {
      locationManager.delegate=self;
        [locationManager requestAlwaysAuthorization];
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
    }
    [locationManager startUpdatingLocation];
    
    
   locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        currentLongitude =  currentLocation.coordinate.longitude;
        currentLatitude =  currentLocation.coordinate.latitude;
        //        NSLog(@"%@%@",currentLongitude,currentLatitude);
        
        
        
    }
    [locationManager stopUpdatingLocation];
}



-(void) viewWillAppear:(BOOL)animated
{
    
    
    dispatch_async(kBgQueue, ^{
        
        [self getAddress];
       [self getCurrentLocation];
        
        
    });
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
//self.navigationItem.hidesBackButton = YES;
    
//    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor clearColor]];
//    [self.navigationItem.leftBarButtonItem setEnabled:NO];

self.navigationController.navigationBar.hidden = YES;
}


-(void)viewWillDisappear:(BOOL)animated
{
    
   self.navigationItem.hidesBackButton = NO;
    
}





//////Address Book  mobile number & name//////////////////////////////////////////////




-(void) getAddress
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        //dispatch_release(semaphore);
    }
    
    else { // We are on iOS 5 or Older
        accessGranted = YES;
        [self getContactsWithAddressBook:addressBook];
    }
    
    if (accessGranted) {
        [self getContactsWithAddressBook:addressBook];
    }
    
}


- (void)getContactsWithAddressBook:(ABAddressBookRef )addressBook
{
       contactList = [[NSMutableArray alloc] init];
    
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i=0;i < nPeople;i++)
    {
        mobilenumber=[NSMutableDictionary dictionary];
        
        dOfPerson=[NSMutableDictionary dictionary];
        
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
        
        //For username and surname
        ABMultiValueRef phones =(__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonPhoneProperty));
        
        
        CFStringRef firstName, lastName;
        
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
        
        
        [dOfPerson setObject:[NSString stringWithFormat:@"%@ ", firstName] forKey:@"name"];
    //NSLog(@"%@",dOfPerson);
        [mobilenumber setObject:[NSString stringWithFormat:@"%@ ", lastName] forKey:@"lastName"];
      // NSLog(@"%@",mobilenumber);
        
        
        
        
        
        
        
        //For Email ids
        ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
        
        if(ABMultiValueGetCount(eMail) > 0) {
            ///[dOfPerson setObject:(__bridge NSString *)ABMultiValueCopyValueAtIndex(eMail, 0) forKey:@"email"];
            
        }
        
        //For Phone number
        NSString* mobileLabel;
        for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
            mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
            if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneHomeFAXLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneWorkFAXLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneMainLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhonePagerLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneOtherFAXLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonHomePageLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
            

        }
        
        if ([dOfPerson objectForKey:@"Phone"] != nil)
        {
           [contactList addObject:dOfPerson];
        }
        else
        {
        
            NSLog(@"enjoy");
        
        
        }
      //  NSLog(@"%@",contactList);
        
}
    [self printnumber];
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]) {
        
        
        
        Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
        [internetReach startNotifier];
        NetworkStatus netStatus = [internetReach currentReachabilityStatus];
        
        
        
        if(netStatus == NotReachable)
        {
            
            UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
            [networkAlert show];
            
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{ // 2
                [self uploadContact];
                
            });
        }
      
            }
   
    
   // NSLog(@"Contacts = %@",contactList);
    
}

- (void)printnumber
{

    
    
    for (int i =0; i<contactList.count; i++) {
        if ([[contactList objectAtIndex:i] valueForKey:@"Phone"]) {
            
            
           
            NSString *strOfNumber = [[[[[[contactList valueForKey:@"Phone"] objectAtIndex:i] stringByReplacingOccurrencesOfString:@"+91" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
           
            
            
            if ([strOfNumber hasPrefix:@"0"] && [strOfNumber length] > 1) {
                strOfNumber = [strOfNumber substringFromIndex:1];
            }
            
          NSString *strOfContactNumber = [[strOfNumber componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
            
//                   NSString  *strOfContactNumber = [strOfNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
            
 
            
            NSMutableDictionary *dictOfContactList = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[contactList valueForKey:@"name"] objectAtIndex:i],@"name",strOfContactNumber,@"Phone", nil];
            
            [arrOfContactList addObject:dictOfContactList];
           
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:arrOfContactList forKey:@"ContactList"];
   
    
}



-(void) uploadContact
{
    NSArray *arrContact = [[[NSUserDefaults standardUserDefaults]valueForKey:@"ContactList"]valueForKey:@"Phone"];
    
    NSString *strOfContactNumber = [arrContact componentsJoinedByString:@","];
    NSLog(@"%@",arrContact);
    NSLog(@"%@",strOfContactNumber);
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/uploadcontact.php?mobileno=%@&userid=%@",strOfContactNumber,[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
      NSMutableDictionary  *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                [[NSUserDefaults standardUserDefaults]setObject:[json valueForKey:@"data"] forKey:@"UploadedContacts"];
                
            }
        }
    }
}



- (IBAction)Rightmenu:(id)sender
{
    
    [self.ViewController rightRevealToggle:nil];
}


- (IBAction)actionSceenMenu:(id)sender
{
    
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
     NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    
    
    
    if(netStatus == NotReachable)
    {
        
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }
    else
    {
        UIButton *button = (UIButton *)sender;
        if (button.tag == 1)
        {
            
            
            //         [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:73.0/255.0 green:193.0/255.0 blue:190.0/255.0 alpha:1.0]];
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainMenuView"];
            [self.navigationController pushViewController:vc animated:NO];
            
        }
        else if (button.tag == 2)
        {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"nearby"];
            [self.navigationController pushViewController:vc animated:NO];
        }
        else if (button.tag == 3)
        {
            
            
            
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginStatus"] isEqualToString:@"Login"]) {
                UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myscene"];
                [self.navigationController pushViewController:vc animated:NO];
                
            }
            else
            {
                    [[NSUserDefaults standardUserDefaults]setObject:@"addFriend" forKey:@"StoryBoardId"];
                UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"signup"];
                [self.navigationController pushViewController:vc animated:NO];
            }
            
            
            
            
           
            
            
            
        }
 
    }
    
}

//http://whatscene.eventbuoy.com/dev/admin/admin/facebooklogin.php?id=2345678910&firstname=Vinayak&lastname=Mestri

@end
