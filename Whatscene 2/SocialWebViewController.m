//
//  SocialWebViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 4/2/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "SocialWebViewController.h"

@interface SocialWebViewController ()
{


    NSString * strOfSocialURL;

}
@property (strong, nonatomic) IBOutlet UIWebView *webViewForSocialWeb;


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorForSocialWeb;
@end
extern NSString *strOfSocialNavigationTitle;

@implementation SocialWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
        if ([strOfSocialNavigationTitle isEqualToString:@"Facebook"])
        {
            strOfSocialURL = @"https://m.facebook.com/SankalpForum";
    
            NSURL *urlOfStr = [NSURL URLWithString:strOfSocialURL];
            NSURLRequest *requestUrl = [NSURLRequest requestWithURL:urlOfStr];
            [_webViewForSocialWeb loadRequest:requestUrl];
            [_webViewForSocialWeb scalesPageToFit];
        }
        else if ([strOfSocialNavigationTitle isEqualToString:@"Twitter"])
        {
    
    
            strOfSocialURL = @"https://support.google.com/a/answer/4213662?hl=en";
            NSURL *urlOfStr = [NSURL URLWithString:strOfSocialURL];
            NSURLRequest *requestUrl = [NSURLRequest requestWithURL:urlOfStr];
            [_webViewForSocialWeb loadRequest:requestUrl];
            [_webViewForSocialWeb scalesPageToFit];
    
        }
        else if ([strOfSocialNavigationTitle isEqualToString:@"Google"])
        {
    
    
            strOfSocialURL = @"https://www.google.co.in/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=completion";
            NSURL *urlOfStr = [NSURL URLWithString:strOfSocialURL];
            NSURLRequest *requestUrl = [NSURLRequest requestWithURL:urlOfStr];
            [_webViewForSocialWeb loadRequest:requestUrl];
            [_webViewForSocialWeb scalesPageToFit];
            
        }


}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    _activityIndicatorForSocialWeb.hidden = NO;
    [_activityIndicatorForSocialWeb startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [_activityIndicatorForSocialWeb stopAnimating];
    _activityIndicatorForSocialWeb.hidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:200.0/255.0 green:21.0/255.0 blue:34.0/255.0 alpha:1.0]];
}

- (IBAction)BackToHome:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:vc animated:NO];
    NSLog(@"done");
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
