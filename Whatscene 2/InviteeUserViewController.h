//
//  InviteeUserViewController.h
//  Whatscene
//
//  Created by Black Bean Engagement on 10/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteeUserViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableViewInviteeUser;
@end
