//
//  ButtonCorner.h
//  FBdemo
//
//  Created by Black Bean Engagement on 17/03/15.
//  Copyright (c) 2015 Black Bean. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface ButtonCorner : UIButton

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;


@end
