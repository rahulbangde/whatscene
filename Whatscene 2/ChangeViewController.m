//
//  ChangeViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 4/16/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "ChangeViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface ChangeViewController ()<UITextFieldDelegate>
{

    NSPredicate *emailPredicate;

  NSMutableDictionary *json;
    NSString * strForUserId;

}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForCurrentPassword;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForNewPassword;

@property (strong, nonatomic) IBOutlet UITextField *textFieldForConfirmPassword;
@property (strong, nonatomic) IBOutlet UIButton *buttonForSubmit;

@end

@implementation ChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    
    UIView *paddingCurrentPassword;
  
    UIView *paddingNewPassword;
    UIView *paddingConfirmPassword;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
       
        paddingNewPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        paddingConfirmPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    }
    else
    {
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
 
        paddingNewPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        paddingConfirmPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
 
    }
    
    _textFieldForCurrentPassword.leftView = paddingCurrentPassword;
    _textFieldForCurrentPassword.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForNewPassword.leftView = paddingNewPassword;
    _textFieldForNewPassword.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForConfirmPassword.leftView = paddingConfirmPassword;
    _textFieldForConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
    

    _activityIndicator.hidden = YES;
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
    _buttonForSubmit.layer.cornerRadius = 5.0; // this value vary as per your desire
    //_buttonForSubmitInfo.layer.masksToBounds = YES;
   // _buttonForSubmit.layer.borderColor=[[UIColor lightGrayColor]CGColor];
   // _buttonForSubmit.layer.borderWidth= 1.0f;
    
    UIColor *color =[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];

    
    
    [_textFieldForCurrentPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForNewPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForConfirmPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    
     _textFieldForCurrentPassword.tintColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    _textFieldForNewPassword.tintColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    _textFieldForConfirmPassword.tintColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];

    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
self.navigationController.navigationBar.backgroundColor = [UIColor  colorWithRed:127.0 green:127.0 blue:127.0 alpha:1];
}
-(BOOL)validateMobileNumber:(NSString*)mobileNumber
{
    NSString *regex = @"{10}";
    emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    
    return [emailPredicate evaluateWithObject:mobileNumber];
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return  YES;
//    if (range.location >= 6)
//        return YES; // return NO to not change text
//    return NO;
    // Prevent crashing undo bug – see note below.
//  
//    
//    if(range.length + range.location > textField.text.length)
//    {
//        return NO;
//    }
//    
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    return (newLength <6 || newLength <6) ? NO : YES;
}

- (IBAction)SubmitNewPassword:(id)sender
{
    
    
    NSLog(@"Its Ok");
     
    if([_textFieldForConfirmPassword.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter your current password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if([_textFieldForNewPassword.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter your new password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if(_textFieldForNewPassword.text.length<6)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter atleast six character of password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    else if(![_textFieldForNewPassword.text isEqualToString:_textFieldForConfirmPassword.text])
    {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"alert" message:@"password and confirm password field does not match" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        
        dispatch_async(kBgQueue, ^{
            
            [self NewPasswordConfirm];
          
            
        });
        
        
    }

    
    
    
}



//Change password link :http://whatscene.eventbuoy.com/dev/admin/admin/editprofilepassword.php?userid=6&password=arun&newpassword=arun123

-(void)NewPasswordConfirm
{

   // [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserEmail"];
    
    //strForUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"]);
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/editprofilepassword.php?userid=%@&password=%@&newpassword=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LoginUserId"],_textFieldForCurrentPassword.text,_textFieldForNewPassword.text];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                NSLog(@"Login Successfully");
                dispatch_async(dispatch_get_main_queue(), ^{
                           [_activityIndicator stopAnimating];
                    _activityIndicator.hidden = YES;
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"StoryBoardId"]];
                    [self.navigationController pushViewController:vc animated:YES];
             
                    
                });
               
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_activityIndicator stopAnimating];
                    _activityIndicator.hidden = YES;
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter correct old password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show ];
                    
                    
                });
                
            }
            
        }
        
        
    }






}




























- (IBAction)BackButton:(id)sender
{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
