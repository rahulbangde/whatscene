//
//  PreviewEventViewController.m
//  Whatscene
//
//  Created by Black Bean Engagement on 07/04/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "PreviewEventViewController.h"
#import "AsyncImageView.h"
@interface PreviewEventViewController ()
{
    UIView *contentView;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
extern NSString *strOfEventDateAndTimeForPreview,*strOfEventDescriptionForPreview,*strOfEventNameForPreview,*strOfEventPlaceForPreview,*strOfEventTypeForPreview,*strOfSceneName;
extern UIImage *imageForPreview;;
extern UIColor *colorForViewScene;
@implementation PreviewEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonTapAction)];
[self.view addGestureRecognizer:tapGesture];
    _scrollView.layer.cornerRadius  = 1.0;
    _scrollView.layer.borderColor = [UIColor blackColor].CGColor;
    _scrollView.layer.borderWidth = 1.0;
    _scrollView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.view.backgroundColor = colorForViewScene;
    [self prepareScrollView];
  
    // Do any additional setup after loading the view.
}


-(void)buttonTapAction
{
  [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)buttonCross:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}


-(void) prepareScrollView
{
    [contentView removeFromSuperview];
    
    contentView = [[UIView alloc] init];
    [contentView setBackgroundColor: [UIColor whiteColor]];
    //------------------------ Add Event name-----------------------------//
    
    UILabel *labelEventName = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, 5,100, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventName.text = @"Event Name :";
    labelEventName.textColor = [UIColor blackColor];
    labelEventName.font = [UIFont boldSystemFontOfSize:15.0];
    labelEventName.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventName];
    
    
    UILabel *labelEventNameText = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, 40,_scrollView.frame.size.width-10, 10)];
    NSString *text = strOfEventNameForPreview;
    labelEventNameText.text = text;
    labelEventNameText.textAlignment = NSTextAlignmentLeft;
    labelEventNameText.lineBreakMode = NSLineBreakByWordWrapping;
    [labelEventNameText setTextColor:[UIColor blackColor]];
    
    CGSize expectedLabelSize = [text sizeWithFont:labelEventNameText.font
                                constrainedToSize:labelEventNameText.frame.size
                                    lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect newFrame = labelEventNameText.frame;
    newFrame.size.height = expectedLabelSize.height;
    labelEventNameText.frame = newFrame;
    labelEventNameText.numberOfLines = 0;
    [labelEventNameText sizeToFit];
    [contentView addSubview:labelEventNameText];
    
    //----------------------------------------------------------------------//
 
    //---------------------------Add Event Description-----------------------//
    
    
    UILabel *labelEventDescription = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventNameText.frame.size.height+labelEventNameText.frame.origin.y+10,200, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventDescription.text = @"Event Description :";
    labelEventDescription.textColor = [UIColor blackColor];
    labelEventDescription.font = [UIFont boldSystemFontOfSize:15.0];
    labelEventDescription.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventDescription];
    
    UILabel *labelEventDescriptionText = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventDescription.frame.size.height+labelEventDescription.frame.origin.y+10,_scrollView.frame.size.width-20, 10)];
    //NSString *text = @"First take clear picture and then try to zoom in to fit the ";
    labelEventDescriptionText.text = strOfEventDescriptionForPreview;
    labelEventDescriptionText.textAlignment = NSTextAlignmentLeft;
    labelEventDescriptionText.lineBreakMode = NSLineBreakByWordWrapping;
    [labelEventDescriptionText setTextColor:[UIColor blackColor]];
    
     expectedLabelSize = [labelEventDescriptionText.text sizeWithFont:labelEventDescriptionText.font
                                constrainedToSize:labelEventDescriptionText.frame.size
                                    lineBreakMode:UILineBreakModeWordWrap];
    
     newFrame = labelEventDescriptionText.frame;
    newFrame.size.height = expectedLabelSize.height;
    labelEventDescriptionText.frame = newFrame;
    labelEventDescriptionText.numberOfLines = 0;
    [labelEventDescriptionText sizeToFit];
    [contentView addSubview:labelEventDescriptionText];
    
    
    //----------------------------------------------------------------------//
    
    
    //---------------------------Add Event Date And Time-----------------------//
    
    
    UILabel *labelEventDateAndTime = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventDescriptionText.frame.size.height+labelEventDescriptionText.frame.origin.y+10,300, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventDateAndTime.text = @"Event Date And Time :";
    labelEventDateAndTime.textColor = [UIColor blackColor];
    labelEventDateAndTime.font = [UIFont boldSystemFontOfSize:15.0];
    labelEventDateAndTime.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventDateAndTime];
    
    UILabel *labelEventDateAndTimeText = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventDateAndTime.frame.size.height+labelEventDateAndTime.frame.origin.y+10,300, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventDateAndTimeText.text = strOfEventDateAndTimeForPreview;
    labelEventDateAndTimeText.textColor = [UIColor blackColor];
    labelEventDateAndTimeText.font = [UIFont systemFontOfSize:15.0];
    labelEventDateAndTimeText.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventDateAndTimeText];
    
    
    //----------------------------------------------------------------------//
    
    
    //---------------------------Add Event Location-----------------------//
    
    
    UILabel *labelEventLocation = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventDateAndTimeText.frame.size.height+labelEventDateAndTimeText.frame.origin.y+10,300, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventLocation.text = @"Event Location :";
    labelEventLocation.textColor = [UIColor blackColor];
    labelEventLocation.font = [UIFont boldSystemFontOfSize:15.0];
    labelEventLocation.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventLocation];
    

    
    UILabel *labelEventLocationText = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventLocation.frame.size.height+labelEventLocation.frame.origin.y+10,_scrollView.frame.size.width-10, 10)];
    //NSString *text = @"First take clear picture and then try to zoom in to fit the ";
    labelEventLocationText.text = strOfEventPlaceForPreview;
    labelEventLocationText.textAlignment = NSTextAlignmentLeft;
    labelEventLocationText.lineBreakMode = NSLineBreakByWordWrapping;
    [labelEventLocationText setTextColor:[UIColor blackColor]];
    
    expectedLabelSize = [labelEventLocationText.text sizeWithFont:labelEventLocationText.font
                                                   constrainedToSize:labelEventLocationText.frame.size
                                                       lineBreakMode:UILineBreakModeWordWrap];
    
    newFrame = labelEventLocationText.frame;
    newFrame.size.height = expectedLabelSize.height;
    labelEventLocationText.frame = newFrame;
    labelEventLocationText.numberOfLines = 0;
    [labelEventLocationText sizeToFit];
    [contentView addSubview:labelEventLocationText];
    
    //----------------------------------------------------------------------//
    
    //---------------------------Add Event Type-----------------------//
    
    
    UILabel *labelEventType = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventLocationText.frame.size.height+labelEventLocationText.frame.origin.y+10,300, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventType.text = @"Event Type :";
    labelEventType.textColor = [UIColor blackColor];
    labelEventType.font = [UIFont boldSystemFontOfSize:15.0];
    labelEventType.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventType];
    
    
    
    UILabel *labelEventTypeText = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventType.frame.size.height+labelEventType.frame.origin.y+10,300, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventTypeText.text = strOfEventTypeForPreview;
    labelEventTypeText.textColor = [UIColor blackColor];
    labelEventTypeText.font = [UIFont systemFontOfSize:15.0];
    labelEventTypeText.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventTypeText];
    
    //----------------------------------------------------------------------//
    
    
    //---------------------------Add Event Picture-----------------------//
    
    
    UILabel *labelEventPicture = [[UILabel alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventTypeText.frame.size.height+labelEventTypeText.frame.origin.y+10,300, 30)];
    //labelEventName.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3];
    labelEventPicture.text = @"Event Picture :";
    labelEventPicture.textColor = [UIColor blackColor];
    labelEventPicture.font = [UIFont boldSystemFontOfSize:15.0];
    labelEventPicture.textAlignment = NSTextAlignmentLeft;
    [contentView addSubview:labelEventPicture];
    
    AsyncImageView * imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x+5, labelEventPicture.frame.size.height+labelEventPicture.frame.origin.y+10, _scrollView.frame.size.width-30, 150)];
    imageView.image = imageForPreview;;
    [contentView addSubview:imageView];

    
    //----------------------------------------------------------------------//

    
      contentView.frame = CGRectMake(0, 0, _scrollView.frame.size.width,imageView.frame.size.height+imageView.frame.origin.y+10);
    [_scrollView addSubview:contentView];
    
    // dataTableView.frame = CGRectMake(0, 0, 320, heightTableView);
    
    _scrollView.contentSize =  CGSizeMake(_scrollView.frame.size.width,imageView.frame.size.height+imageView.frame.origin.y+10);
    
}
- (IBAction)buttonBack:(id)sender {
    if ([strOfSceneName isEqualToString:@"MyScene"]) {
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"viewEvent"];
        [self.navigationController pushViewController:viewController animated:YES];

    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    }
@end
