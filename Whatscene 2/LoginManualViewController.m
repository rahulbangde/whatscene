//
//  LoginManualViewController.m
//  Whatscene
//
//  Created by Alet Viegas on 12/03/15.
//  Copyright (c) 2015 blackbean. All rights reserved.
//

#import "LoginManualViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "OAuthConsumer.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface LoginManualViewController ()<UITextFieldDelegate,UINavigationControllerDelegate,FBLoginViewDelegate,UIAlertViewDelegate>
{

    UIImage * image;
    NSString * Accesstoken;
    NSString * strOfName;
    NSString* strOfEmail;
    NSString*strOfGender;
    NSString*strOfLocation;
    NSMutableDictionary *json;
  


}




@property (strong, nonatomic) IBOutlet UIButton *buttonForFaceBook;


- (IBAction)LoginToFaceBook:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForUserName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForPassword;

@property (strong, nonatomic) IBOutlet UIButton *buttonForLogin;


- (IBAction)LoginManually:(id)sender;
- (IBAction)BackToHome:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *buttonForForgotPassword;

- (IBAction)ForgotPassword:(id)sender;




@property (weak, nonatomic) IBOutlet UIButton *buttonForSignUp;
@property (nonatomic,strong) OAToken* accessToken;

@end
NSDictionary *dictOfFBData;
@implementation LoginManualViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /// padding
    
    UIView *paddingCurrentPassword;
    
    UIView *paddingFirstName;

    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        paddingFirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
      
        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];

    }
    else
    {
        paddingFirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        

        paddingCurrentPassword = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        
    }
    
    _textFieldForUserName.leftView = paddingFirstName;
    _textFieldForUserName.leftViewMode = UITextFieldViewModeAlways;
    
    _textFieldForPassword.leftView = paddingCurrentPassword;
    _textFieldForPassword.leftViewMode = UITextFieldViewModeAlways;
    
   
    
    
    
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Futura-MediumItalic" size:17], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;

    _activityIndicator.hidden = YES;
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // create a fresh session object
        appDelegate.session = [[FBSession alloc] init];
        
        // if we don't have a cached token, a call to open here would cause UX for login to
        // occur; we don't want that to happen unless the user clicks the login button, and so
        // we check here to make sure we have a token before calling open
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                // we recurse here, in order to update buttons and labels
                // [self updateView];
            }];
        }
    }

//    
//    _LoginWithFacebook.delegate = self;
//    _LoginWithFacebook.readPermissions = @[@"public_profile", @"email"];
    
    
    
      self.navigationController.navigationBar.hidden = YES;
    
//Button Modification
    
    _buttonForLogin.layer.cornerRadius = 5.0; // this value vary as per your desire
    //_buttonForSubmitInfo.layer.masksToBounds = YES;
//    _buttonForLogin.layer.borderColor=[[UIColor lightGrayColor]CGColor];
//    _buttonForLogin.layer.borderWidth= 1.0f;

    _buttonForSignUp.layer.cornerRadius = 5.0; // this value vary as per your desire
    //_buttonForSubmitInfo.layer.masksToBounds = YES;
//    _buttonForSignUp.layer.borderColor=[[UIColor lightGrayColor]CGColor];
//    _buttonForSignUp.layer.borderWidth= 1.0f;

    
    
//TextField Placeholder & tint color
    UIColor *color =[UIColor  colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];
 
    [_textFieldForUserName setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldForPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];




    
    _textFieldForUserName.tintColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    _textFieldForPassword.tintColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];;
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:56.0/255.0 green:53.0/255.0 blue:57.0/255.0 alpha:1.0]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;

    self.navigationController.navigationBar.backgroundColor = [UIColor  colorWithRed:127.0 green:127.0 blue:127.0 alpha:1];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,navigationBar.frame.size.height-1,navigationBar.frame.size.width, 0.5)];
    
    // Change the frame size to suit yours //
    
    [navBorder setBackgroundColor:[UIColor colorWithWhite:200.0f/255.f alpha:0.8f]];
    [navBorder setOpaque:YES];
    [navigationBar addSubview:navBorder];
}

- (IBAction)LoginManually:(id)sender
{
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    
    
    
    if ([[_textFieldForUserName.text stringByTrimmingCharactersInSet:charSet] isEqualToString:@""] || [_textFieldForPassword.text isEqualToString:@""])
    {
    
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter valid username and password " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
    
    
    }

    else if(netStatus == NotReachable)
    {
        
        UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
        [networkAlert show];
        
        
    }

    else {
    
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
        });
        dispatch_async(kBgQueue, ^{
            
            [self loginMethod];
            
        });

    
    
    }
}





-(void) loginMethod
{
    
    
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/login_sub.php?email=%@&password=%@",_textFieldForUserName.text,_textFieldForPassword.text];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                NSLog(@"Login Successfully");
                
                NSLog(@"%@",[[json valueForKey:@"data"]valueForKey:@"image"]);
                [[NSUserDefaults standardUserDefaults] setObject:[[[json valueForKey:@"data"]valueForKey:@"notificatio_status"]objectAtIndex:0] forKey:@"notificatio_status"];
                [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"LoginStatus"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0] forKey:@"image"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"firstname"]objectAtIndex:0] forKey:@"firstname"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"lastname"]objectAtIndex:0] forKey:@"lastname"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0] forKey:@"LoginUserId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"mobno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"dob"]objectAtIndex:0] forKey:@"LoginUserDOB"];
                
                NSLog(@"Downloading...");
               
                image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[json valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0]]]];
                dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                   [self uploadImage];
                                   [self uploadContact];
                                   _activityIndicator.hidden = YES;
                                   [_activityIndicator stopAnimating];
                               });
                
                dispatch_async(dispatch_get_main_queue(), ^{

                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
                    [self.navigationController pushViewController:vc animated:YES];
                });
               


                
            }
            
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _activityIndicator.hidden = YES;
                    [_activityIndicator stopAnimating];
                    _textFieldForPassword.text = @"";
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please insert valid email and password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alertView.tag = 12;
                    [alertView show];
                    
                });
              
            }
  
        }
        
       
    }
    
    
   
}




-(void) uploadImage
{
  
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png",docDir];
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
    [data1 writeToFile:pngFilePath atomically:YES];
    
    
//    [self.navigationController popToRootViewControllerAnimated:YES];
   
}


-(void) uploadContact
{
    NSArray *arrContact = [[[NSUserDefaults standardUserDefaults]valueForKey:@"ContactList"]valueForKey:@"Phone"];
    
    NSString *strOfContactNumber = [arrContact componentsJoinedByString:@","];
    NSLog(@"%@",arrContact);
    NSLog(@"%@",strOfContactNumber);

    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/uploadcontact.php?mobileno=%@&userid=%@",strOfContactNumber,[[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserId"]];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    NSError *err;
    if (data !=nil )
    {
        json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                [[NSUserDefaults standardUserDefaults]setObject:[json valueForKey:@"data"] forKey:@"UploadedContacts"];
                
            }
        }
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    return YES;
}


- (IBAction)BackToHome:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[NSUserDefaults standardUserDefaults] valueForKey:@"StoryBoardId"]];
    [self.navigationController pushViewController:vc animated:NO];
    NSLog(@"done");

    
    
}
- (IBAction)ForgotPassword:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"forgot"];
    [self.navigationController pushViewController:vc animated:NO];
    NSLog(@"done");
    
}
- (IBAction)SignUp:(id)sender
{
    
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"signup"];
    [self.navigationController pushViewController:vc animated:NO];
    NSLog(@"done");
    
    
}



- (IBAction)LoginToFaceBook:(id)sender
{
    NSLog(@"fb");
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen) {
        // if a user logs out explicitly, we delete any cached token information, and next
        // time they run the applicaiton they will be presented with log in UX again; most
        // users will simply close the app or switch away, without logging out; this will
        // cause the implicit cached-token login to occur on next launch of the application
        [appDelegate.session closeAndClearTokenInformation];
        
    } else {
        if (appDelegate.session.state != FBSessionStateCreated) {
            // Create a new, logged out session.
            appDelegate.session = [[FBSession alloc] init];
        }
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState status,
                                                         NSError *error) {
            // and here we make sure to update our UX according to the new session state
            [self updateView];
        }];
        
        
    }
}

- (void)updateView
{
    // get the app delegate, so that we can reference the session property
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (appDelegate.session.isOpen)
    {
        // valid account UI is shown whenever the session is open
        //        [self.buttonLoginLogout setTitle:@"Log out" forState:UIControlStateNormal];
        //        [self.textNoteOrLink setText:[NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
        //appDelegate.session.accessTokenData.accessToken]];
        NSString *strData=[NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",
                           appDelegate.session.accessTokenData.accessToken];
        NSLog(@"%@",appDelegate.session.accessTokenData.accessToken);
        NSURL *strURl=[NSURL URLWithString:strData];
        NSError *err;
        NSData *data=[NSData dataWithContentsOfURL:strURl];
        dictOfFBData=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        NSLog(@"dictOfData>>>>>>>>>%@",dictOfFBData);
        NSString *strOfImage=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"username"]];
        
        dispatch_async(kBgQueue, ^{
            
            [self checkFbRegister];
//            dispatch_async(dispatch_get_main_queue(), ^{
//             
//                
//                
//            });
            
        });
    
    }
    else
    {
        // login-needed account UI is shown whenever the session is closed
        //        [self.buttonLoginLogout setTitle:@"Log in" forState:UIControlStateNormal];
        //        [self.textNoteOrLink setText:@"Login to create a link to fetch account data"];
    }
}



-(void) checkFbRegister
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebookloginstatus.php?fbid=%@",[dictOfFBData valueForKey:@"id"]];
    strOfUrl = [strOfUrl stringByReplacingOccurrencesOfString:@"'" withString:@""];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlOfStr standardizedURL]];
    NSURLResponse *response;
    NSError *err;
    [request setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    if (data !=nil ) {
        NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",jsondata);
            
            
            if ([[jsondata valueForKey:@"success"] isEqualToString:@"true"])
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                         [[NSUserDefaults standardUserDefaults] setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"notificatio_status"] objectAtIndex:0] forKey:@"notificatio_status"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"LoginStatus"];
                      [[NSUserDefaults standardUserDefaults] setObject:@"LoginFB" forKey:@"FBLoginStatus"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0] forKey:@"image"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"firstname"]objectAtIndex:0] forKey:@"firstname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"lastname"]objectAtIndex:0] forKey:@"lastname"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0] forKey:@"LoginUserId"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                    NSArray *key = [[[jsondata valueForKey:@"data"] objectAtIndex:0] allKeys];
                    if ([key containsObject:@"mobno"]) {
                        [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"mobno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                    }
                    if ([key containsObject:@"dob"]) {
                        [[NSUserDefaults standardUserDefaults]setObject:[[[jsondata valueForKey:@"data"]valueForKey:@"dob"]objectAtIndex:0] forKey:@"LoginUserDOB"];
                    }
                    
                    
                    
                    NSLog(@"Downloading...");
                    
                    image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[jsondata valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0]]]];
                    [self uploadImage];
//                    [self uploadContact];
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addFriend"];
                    [self.navigationController pushViewController:vc animated:NO];
                    
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VarifiedMobileNoView"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                });
            }
        }
    }
}
    
-(void) signUpWithfacebook
{
    NSString *strOfUrl = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/facebooklogin.php?id=%@&firstname=%@&lastname=%@",[dictOfFBData valueForKey:@"id"],[dictOfFBData valueForKey:@"first_name"],[dictOfFBData valueForKey:@"last_name"]];
    strOfUrl = [strOfUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlOfStr = [NSURL URLWithString:strOfUrl];
    NSData *data = [NSData dataWithContentsOfURL:urlOfStr];
    
    NSError *err ;
    if (data !=nil ) {
        json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        if (err) {
            NSLog(@"err---- %@",err.description);
        }
        else{
            NSLog(@"Result = %@",json);
           // strEventId = [[[json valueForKey:@"data"]valueForKey:@"userid"] objectAtIndex:0];
            
            if ([[json valueForKey:@"success"] isEqualToString:@"true"])
            {
     
                
                
                NSLog(@"Login Successfully");
                
                NSLog(@"%@",[[json valueForKey:@"data"]valueForKey:@"image"]);
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"LoginStatus"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0] forKey:@"image"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"firstname"]objectAtIndex:0] forKey:@"firstname"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"lastname"]objectAtIndex:0] forKey:@"lastname"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"userid"]objectAtIndex:0] forKey:@"LoginUserId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"email"]objectAtIndex:0] forKey:@"LoginUserEmail"];
                [[NSUserDefaults standardUserDefaults]setObject:[[[json valueForKey:@"data"]valueForKey:@"mobno"]objectAtIndex:0] forKey:@"LoginUserMobileNo"];
                
                
                NSLog(@"Downloading...");
                
                image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[json valueForKey:@"data"]valueForKey:@"image"]objectAtIndex:0]]]];
                
                
//                UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myscene"];
//                [self.navigationController pushViewController:vc animated:YES];
               
            }
            
            
        }
    }
    
}


//-(void)uploadFBImage
//{
//    ///home/listo8mw/public_html/IMAGEDEMO/uploadImage.php
//    NSString *strOfImage=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[dictOfFBData valueForKey:@"id"]];
//    
//    image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strOfImage]]];
//    
//    
//    NSData *imgData = UIImagePNGRepresentation(self.imageViewForUserSelFie.image);
//    NSString *urlString = [NSString stringWithFormat:@"http://whatscene.eventbuoy.com/dev/admin/admin/image/userimageupload.php?user_id=%@",strEventId];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:urlString]];
//    [request setHTTPMethod:@"POST"];
//    NSString *boundary = @"---------------------------14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//    NSMutableData *body = [NSMutableData data];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.png\"\r\n",[NSString stringWithFormat:@"user%@",strEventId]] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[NSData dataWithData:imgData]];
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [request setHTTPBody:body];
//    NSError *error = nil;
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
//    if (error) {
//        NSLog(@"Error:%@",error.localizedDescription);
//        return;
//    }
//    //NSString *str = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
//    NSMutableArray *arr=[NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
//    if (error) {
//        NSLog(@"Error:%@",error.localizedDescription);
//        return;
//    }
//    NSMutableDictionary *dict=[arr objectAtIndex:0];
//    NSLog(@"Data in Dixtionary %@",dict);
//}
//

    
    

@end
